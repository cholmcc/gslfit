/**
   @file      utils.hh
   @author    Christian Holm Christensen <cholmcc@gmail.com>
   @date      18 Aug 2021
   @copyright (c) 2021 Christian Holm Christensen. 
   @license   LGPL-3
   @brief     Various utilities
*/
#ifndef GSLFIT_UTILS_HH
#define GSLFIT_UTILS_HH
#include <iostream>
#include <valarray>
#include <random>
#include <numeric>
#include <functional>
#include <chrono>

/**  
    @defgroup gslfit_utils Various utilties used by examples 
     
    This module constains some utilites used in the examples. 

    @section gslfit_utils_wraps Wrapping function to fit 

    The class template utils::wrapper wraps a function @f$ f@f$, and
    possibly its gradient @f$\nabla f@f$ and Hessian @f$\nabla\nabla
    f@f$ to fit to data and returns functors to use in the fitting.
    This assumes a number of things

    - The data type `Data` has 
      - the data members `x`, `y`, and `e` and that we can call 
        functions on these. 
      - the member function `size` to get the number of points 
    - The parameter type `Parameters` 
      - is iterable 
      - objects of this class can be constructed with a size 
    - The function type `Function` has the prototype 

          Parameters Function(const Data&, const Parameters&)

      and returns the function @f$f@f$ evaluated at all points of the
      data.
    - The function type `Jacobian` has the prototype 

          Parameters Jacobian(const Data&, const Parameters&)
      
      and returns the gradient, with respect to the parameters, at
      each point of data.  Note, this means that the function must
      return a matrix (in row-major order) where each row is the
      gradient at the corresponding data point.

      If the `Jacobian` type is `void*`, then numerical evaluation of
      the Jacobian will be used instead.  -
    - The function type `Hessian` has the prototype 

          Parameters Hessian(const Data&, const Parameters&)
      
      and returns the second-order gradient, with respect to the
      parameters, at each point of data.  Note, this means that the
      function must return a tensor (in row-major order) where each
      row is the Hessian matrix at the corresponding data point.

      If the `Hessian` type is `void*`, then numerical evaluation of
      the Hessian will be used instead.  -

   This class template is really a helper.  Instead, one should use
   the function utils::make_wrapper which returns a tuple of wrapped
   functor calls which can be passed to gsl::fit.

   @section gslfit_utils_timer Timer 

   A simple timer guard (starts on construction, terminates at
   destruction).  One can use objects of this class to time execution.
   If one calls utils::timer::lap on each iteration, one will get the
   average time per iteration (and the uncertainty on this) printed
   out at the end.
   
*/
/** 
    Namespace for various utilities 

    @ingroup gslfit_utils
*/
namespace utils {
  //================================================================
  /** 
      @{ 
      @name Histogramming 
  */
  /** Histogram axis 
      
      Instances of this template defines a class for a one-dimensional
      axis of bins.  These bins can be equidistant (of same width) or
      of variable width.  The instances of this class template defines
      member functions to

      - Look up a bin index (utils::axis::find_bin) 

      - Get the widths (utils::axis::widths) and centers
        (utils::axis::centers) of all bins.

      - Get the number of bins (utils::axis::size). 

      Objects of instances of the class template utils::axis can be
      shared among different uses - once defined the objects are
      immutable.
      
      @ingroup gslfit_utils
  */
  template <typename Value=double>
  struct axis {
    /** Value type */
    using value_type=Value;
    
    /** Value array */
      using value_array=std::valarray<value_type>;
    
    /** 
	Constructor.  This will create equidistant bins
	
	@param nbins Number of bins 
	@param low   Low edge 
	@param high  High edge 
    */
    axis(size_t nbins, value_type low, value_type high)
      : _bounds(nbins+1) {
      value_type  dx = (high - low) / nbins;
      
      std::iota(std::begin(_bounds),std::end(_bounds),value_type());
      
      _bounds *= dx;
      _bounds += low;
    }
    /** 
	Constructor.  Initialize from an initializer list 
	
	@param bounds bin boundaries 
    */
    axis(std::initializer_list<value_type> bounds)
      : _bounds(bounds) {}
    /** 
	Find bin index corresponding to @a x 
	
	@param x  Value to find bin index for 
	
	@return bin index or -1 if out of bounds 
    */
    int find_bin(const value_type x) const {
      if (x == _bounds[size()])
	return size()-1;
      
      auto s = std::begin(_bounds);
      auto e = std::end(_bounds);
      auto i = std::upper_bound(s,e,x);
      if (i == e) return -1;
      
      return std::distance(s,i)-1;
    }
    
    /** Bin widts */
    value_array widths() const {
      size_t n = size();
      return _bounds[std::slice(1,n,1)]-_bounds[std::slice(0,n,1)]; }
    
    /** Bin centres */
    value_array centers() const {
      size_t n = size();
      return (_bounds[std::slice(1,n,1)]+_bounds[std::slice(0,n,1)])/2; }
    
    /** Number of bins */
    size_t size() const { return _bounds.size() - 1; }
  protected:
    value_array _bounds;
  };

  //----------------------------------------------------------------
  /** 
      Histogram
	
      Instances of this class template define a class of
      one-dimensional histogram.  Objects of instances of the class
      template uses the service of the utils::axis passed at the time
      of construction.  These objects will record the number of
      (possibly weighted) observations that fall within each of the
      bins defined by the utils::axis instance object.  The instances
      of the class template utils::histogram allows for

      - Filling in a single or multiple observations
        (utils::histogram::fill) with or without weights.

      - Get information from the utils::axis instance object
        used.  Note this object can be shared by different
        utils::histogram instance objects.

      - Get the density in all bins (utils::histogram::y) as
        well as the associated statistical uncertainty
        (utils::histogram::e).  Note that 

	@f{align*}{
	   y &= \frac{\sum_i w_i}{\Delta}
	   & e &= \frac{\sqrt{\sum_i w_i}}{\Delta}\quad,
	@f} 

	where @f$\sum_i w_i@f$ and @f$\Delta@f$ are the sum of weights
	and widths of each bin, respecively.

      - Get the total integral of histogram
        (utils::histogram::sumw).
      
      @ingroup gslfit_utils
  */
  template <typename Value=double>
  struct histogram {
    /** Value type */
    using value_type=Value;

    /** Axis type */
    using axis_type=axis<value_type>;

    /** Value array */
    using value_array=typename axis_type::value_array;

    /** Count array */
    using count_array=value_array;

    /** 
	Constructor 

	@param axis Axis to use (can be shared)
    */
    histogram(const axis_type& axis)
      : _axis(axis),
	_sumw(axis.size())
    {}

    /** 
	Fill in observation 
	  
	@param x Value to fill in 
	@param w weight to fill with 
	  
	@return true if x is in bounds 
    */
    bool fill(const value_type& x, const value_type& w=1) {
      int b = _axis.find_bin(x);
      if (b < 0) return false;

      _sumw[b] += w;

      return true;
    }

    /** 
	Fill in may observations 
	  
	@param x Values to fill in 
	@param w weights to fill with 
	  
	@returns number of successful fills
    */
    size_t fill(const value_array& x, const value_array& w) {
      size_t n  = 0;
      auto   ix = std::begin(x);
      auto   iw = std::begin(w);
      for (; ix != std::end(x); ++ix, ++iw) n+= fill(*ix,*iw);

      return n;
    }
      
    /** 
	Fill in may observations 
	  
	@param x Values to fill in 

	@returns number of successful fills
    */
    size_t fill(const value_array& x) {
      size_t n = 0;
      for (auto xx : x) n += fill(xx);

      return n;
    }

    /** Get bin mid-points */
    value_array x() const { return _axis.centers(); }

    /** Get bin widths */
    value_array w() const { return _axis.widths(); }

    /** Get bin heights (sum weights divided by widht) */
    value_array y() const { return _sumw / _axis.widths(); }

    /** Get bin uncertainties (square root of sum of weights,
	divided by widths) */
    value_array e() const { return std::sqrt(_sumw) / _axis.widths(); }

    /** Get the total sum of weights (integral) */
    value_type sumw() const { return _sumw.sum(); }

    /** Get the nubmer of bins */
    size_t size() const { return _sumw.size(); }
  protected:
    /** Axis used */
    const axis_type& _axis;
    /** Sum of weights */
    count_array _sumw;
  };

  //----------------------------------------------------------------
  /** 
      A histogram frozen in time.  Constructing an object of this type
      from a histogram will freeze the current values of the histogram
      in that object.

      Since obtaining the bin centers, widths, densities, and
      uncertainties requires (expensive) calculations, we define the
      class template utils::frozen_histogram.  When an object of an
      instance of this class template is constructed from an
      utils::histogram instance object, then the current values of
      these quantities are copied to the utils::frozen_histogram
      instance object.  Thus, we only need to do the potentially
      expensive calculations once.

      @note This class template (utils::frozen_histogram) together
      with the class templates utils::histogram and utils::axis really
      defines a very simple but efficient histogramming package.
      Coupled with the wrappers provided by gsl::fit we essentially
      have an analysis suit.
	
      
      @ingroup gslfit_utils
  */
  template <typename Value>
  struct frozen_histogram
  {
    /** Histogram type */
    using histogram_type=histogram<Value>;
      
    /** Value array type */
    using value_array=typename histogram_type::value_array;

    /** X values */
    const value_array x;

    /** Y values */
    const value_array y;

    /** Uncertainties on Y */
    const value_array e;

    /** 
	Constructor. 

	Copy current values in histogram to this object 
    */
    frozen_histogram(const histogram_type& h)
      : x(h.x()), y(h.y()), e(h.e())
    {}
  };
  /** @} */
  
  //================================================================
  /** 
      @{ 
      @name Wrappers around data and functions 
  */
  /** 
      Wrappers around function to fit calls.  

      @ingroup gslfit_utils
  */
  template <typename Data,
	    typename Parameters,
	    typename Function,
	    typename Jacobian,
	    typename Hessian>
  struct wrapper {
    /** Type of data */
    using data_type = Data;

    /** Type of parameters (container) */
    using parameters_type = Parameters;

    /** Type of function being fitted */
    using function_type = Function;

    /** Type of Jacobian of function being fitted */
    using jacobian_type = Jacobian;

    /** Type of Hessian of function being fitted */
    using hessian_type = Hessian;

    /** 
	Calculates the outer product of two vectors (assumed to be the
	same size).  

	We assume we can call parameters_type::size and that this is
	iterable.

	@param u  First vector 
	@param v  Second vector 
	
	@returns the outer product @f$ uv^T@f$ 
    */
    static parameters_type outer(const parameters_type& u,
				 const parameters_type& v) {
      auto   bu = std::begin(u);
      auto   bv = std::begin(v);
      size_t m  = std::distance(bu, std::end(u));
      size_t n  = std::distance(bv, std::end(v));

      parameters_type uv(m*n);
      auto            iuv = std::begin(uv);

      for (auto iu = bu; iu != std::end(u); ++iu)
	for (auto iv = bv; iv != std::end(v); ++iv, ++iuv)
	  *iuv = *iu * *iv;

      return uv;
    }

    /** 
	@{ 
	@name Data 
    */
    /** 
	Get the weights.  Assumes we can access uncertainties as
	data_type::e
    */
    static parameters_type weights(const data_type& data) {
      parameters_type w = 1. / std::pow(data.e,2);
      w[data.e < 1e-9] = 0;
      return w;
    }
    /** @} */
    
    /** 
	@{ 
	@name Residuals 
    */
    /** 
	Calculate residuals of data with respect to the function 

	Assumes we can call data_type::x and data_type::y, and that
	the function can evulate over and return arrays of values.

	@param data       The data 
	@param parameters The parameter values 
	@param function   Function to fit 
	
	@return Residuals 
    */
    static parameters_type residuals(const data_type&       data,
				     const parameters_type& parameters,
				     function_type          function) {
      return data.y - function(data.x, parameters);
    }
    /** 
	Get the call to residuals 

	@param function The function to fit 

	@return functor object to use when fitting 
    */
    static auto residuals_call(function_type function) ->
      decltype(std::bind(residuals,
			 std::placeholders::_1,
			 std::placeholders::_2,
			 function)) {
      using namespace std::placeholders;
      return std::bind(residuals,_1,_2,function);
    }
    /** @} */

    /** 
	@{
	@name Jacobian 
    */
    /** 
	Calculate the Jacobian of the residuals. 

	Assumes we can call data_type::x and that function returns a
	matrix encoded in row-major format of the jacobian of the
	function to fit.
      
	@param data       The data 
	@param parameters The parameter values 
	@param jacobian   Jacobian of function to fit 
	
	@return The Jacobian matrix 
    */
    static parameters_type jacobian(const data_type&       data,
				    const parameters_type& parameters,
				    jacobian_type&         jacobian) {
      return -jacobian(data.x, parameters);
    }
    /** 
	Get Jacobian function to call in fitting routines.  This is
	for the case that the user give null for this function.
    */
    template <typename J,
	      typename std::enable_if<std::is_same<J,void*>::value ||
				      std::is_same<J,nullptr_t>::value,
				      bool>::type = true>
    static void* jacobian_call(J) { return nullptr; }
    /** 
	Get the jacobian function to call in the fitting routines.
	This is for the case that the function was given.

	That is, if the user passed a Jacobian function, then this
	is called and we return a binding of the above wrapper with
	that function.
    */
    template <typename J,
	      typename std::enable_if<!std::is_same<J,void*>::value &&
				      !std::is_same<J,nullptr_t>::value,
				      bool>::type = true>
    static auto jacobian_call(J j) ->
      decltype(std::bind(jacobian,
			 std::placeholders::_1,
			 std::placeholders::_2,
			 j)) {
      using namespace std::placeholders;
      return std::bind(jacobian,_1,_2,j);
    }
    /** @} */
    

    /** 
	@{ 
	@name Hessian 
    */
    /** 
	Calculate the Hessian of the residuals. 

	Assumes we can call data_type::x and that function returns a
	matrix encoded in row-major format of the hessian of the
	function to fit.

	The hessian function is assumed to return a tensor where the
	first index indexes the data points, and gives the Hessian
	matrix at that point.
      
	@param data       The data 
	@param parameters The parameter values 
	@param velocities Current velocities
	@param hessian    Hessian of function to fit 
	
	@return The Hessian matrix 
    */
    static parameters_type hessian(const data_type&        data,
				    const parameters_type& parameters,
				    const parameters_type& velocities,
				    hessian_type&          hessian) {
      size_t          n   = parameters.size();
      parameters_type ret(data.size());
      auto            ir  = std::begin(ret);
      auto            hes = hessian(data.x, parameters);
      auto            ih  = std::begin(hes);
      auto            vv  = outer(velocities,velocities);

      for (; ir != std::end(ret); ++ir, ih += n*n)
	*ir = std::inner_product(std::begin(vv),
				 std::end(vv),
				 ih, 0);
      
      return -ret;
    }
    /** 
	Get the hessian function to call in the fitting routines. This
	is for the case when the user gave no such function. 
	
	That is, if the user passed null for the Hessian function,
	this is called which returns a null pointer.
    */
    template <typename H,
	      typename std::enable_if<std::is_same<H,void*>::value ||
				      std::is_same<H,nullptr_t>::value,
				      bool>::type = true>
    static void* hessian_call(H) { return nullptr; }
    /** 
	Get the hessian function to call in the fitting routines. This
	is for the case when such a function was given.
	
	That is, if the user passed a Hessian function, then this
	is called and we return a binding of the above wrapper with
	that function.
    */
    template <typename H,
	      typename std::enable_if<!std::is_same<H,void*>::value &&
				      !std::is_same<H,nullptr_t>::value,
				      bool>::type = true>
    static auto hessian_call(H h) ->
      decltype(std::bind(hessian,
			 std::placeholders::_1,
			 std::placeholders::_2,
			 std::placeholders::_3,
			 h)) {
      using namespace std::placeholders;
      return std::bind(hessian,_1,_2,_3,h);
    }
    /** @} */
  };
  //__________________________________________________________________
  /** 
      A function that returns the appropriate wrapper calls 

      @ingroup gslfit_utils
  */
  template <typename Data,
	    typename Parameters,
	    typename Function,
	    typename Jacobian=void*,
	    typename Hessian=void*>
  auto // Tuple of residuals, weights, jacobian, hessian
  make_wrappers(const Data& data,
		const Parameters& p0,
		Function          function,
		Jacobian          jacobian=nullptr,
		Hessian           hessian=nullptr)
#if 1
    -> std::tuple<decltype(wrapper<Data,Parameters,Function,Jacobian,Hessian>::
			   residuals_call(function)),
		  Parameters (*)(const Data&),
		  // decltype(wrapper<Data,Parameters,Function,Jacobian,Hessian>::
  // weights(data)),
		  decltype(wrapper<Data,Parameters,Function,Jacobian,Hessian>::
			   jacobian_call(jacobian)),
		  decltype(wrapper<Data,Parameters,Function,Jacobian,Hessian>::
			   hessian_call(hessian))>
#endif 
			   
  {
    using wrapped=wrapper<Data,Parameters,Function,Jacobian,Hessian>;
    return std::make_tuple(wrapped::residuals_call(function),
			   wrapped::weights,
			   wrapped::jacobian_call(jacobian),
			   wrapped::hessian_call(hessian));
  }
  /** @} */

  //================================================================
  /** 
      @{ 
      @name Timing
  */
  /** 
      Timer guard 

      @ingroup gslfit_utils
  */
  struct timer
  {
    /** clock type */
    using clock_type=std::chrono::high_resolution_clock;
    /** time type */
    using time_point=typename clock_type::time_point;
    /** Our measuring unit */
    using duration=std::chrono::duration<double,std::milli>;
    /** Start of timer */
    time_point _start;
    /** Last lap */
    time_point _prev;
    /** Sum of laps */
    double _sum;
    /** Sum of square laps */
    double _sum2;
    /** Number of laps */
    size_t _cnt;
      
    timer()
      : _start(clock_type::now()),
	_prev(_start),
	_sum(0),
	_sum2(0),
	_cnt(0)
    {}

    void lap() {
      auto now  =  clock_type::now();
      auto elap =  duration(now - _prev).count();
      _prev     =  now;
      _sum      += elap;
      _sum2     += elap * elap;
      _cnt++;
    }
    
    ~timer() {
      double mean  = _sum / _cnt;
      double mean2 = _sum2 / _cnt;
      double var   = mean2 - mean * mean;
      double sem   = std::sqrt(var / _cnt);

      std::cout << "Elapsed: " << _sum << " ms for " << _cnt << " laps"
		<< " average lap: (" << mean
		<< " +/- " << sem << ") ms" << std::endl;
    }
  };
  /** @} */
}
#endif
// Local Variables:
//   compile-command: "g++ -c utils.hh"
// End:

