# GSL fit

C++ wrapper around GSL fitting routines and fitting of YODA analysis
objects. 

## Introduction 

The [GNU Scientific Library](https://www.gnu.org/software/gsl/) is an
excellent C library with many utilities for scientific computing.  One
of the many features of the library is [nonlinear least-squares
fitting](https://en.wikipedia.org/wiki/Non-linear_least_squares) in
the sub-library `gsl_multifit_nlinear`.  

The interface of that library is in general well defined but is very
C-centric and is harder to use in a C++ application.  This library
(really the header [`gslfit.hh`](gslfit.hh)) provides a more C++
centric interface.  The interface consists of class and function
templates to ensure type safety and flexibility in use.

## Interface 

The core of the interface is the template function `gsl::fit`.

The function expects as input 

- A configuration for the fit (`gsl::fit_config`) which specifies
  tolerances, trust-region method, and so on.

- An initial guess of the parameter values to be fitted.  

  $`\displaystyle 
  p_0=\{p_1,\ldots,p_M\}`$
	
  These can be given as any _Iterable_ (not exactly the same as
  [_Container_](https://en.cppreference.com/w/cpp/named_req/Container)).
  The type of the parameters determine a number of other types,
  including some return value types.

- The data to fit to.  This can be of any type - all that is required
  is that the remaining passed functions operate on objects of that
  type.  Typically the data will consist of correlated pairs of an
  _independent_ variable $`x`$ and a _dependent_ variable $`y`$ (these
  can be in general be vectors).  That is, the data is somehow defined
  in terms of $`(x_i,y_i)`$ pairs.

- The number of elements in the data to fit.  This is needed
  internally by the GSL routines to properly allocate memory for
  working spaces and such. If the data is given by $`N`$ $`(x_i,y_i)`$
  pairs, then this should be $`N`$

- A _Callable_ (roughly
  [_invocable_](https://en.cppreference.com/w/cpp/concepts/invocable))
  which calculates the residuals of the data with respect to the
  function being fitted to the data evaluated at all independent
  variable values
    
  $`\displaystyle 
  r_i = y_i - f(x_i;p)\quad,`$
	
  where $`p`$ are the current parameter values.

- A _Callable_ that possibly computes the weights of each of the _N_
  data points.  If there is an uncertainty $`e_i`$ associated with the
  dependent variable values $`y_i`$, then the weight should be
  
  $`\displaystyle  
  w_i = \begin{cases} 1/e_i^2 & e_i > \epsilon\cr
  0\end{cases}\quad, 
  `$
  
  to perform a regular $`\chi^2`$ fit.  Note, if a point $`i`$ has
  small or zero uncertainty it is implicitly discarded by setting
  the associated weight to 0.
  
  If the data has no uncertainties(?) then all weights should be
  unity. 

- Optionally, a _Callable_ that computes the derivative of the
  residuals with respect to each of the $`M`$ parameters and thus
  returns the Jacobian
  
  $`\displaystyle 
  J = \begin{bmatrix} 
	  \frac{\partial r_1}{\partial p_1}
	  &\cdots
	  &\frac{\partial r_1}{\partial p_M}\cr
	  \vdots
	  &\ddots
	  &\vdots\cr
	  \frac{\partial r_N}{\partial p_1}
	  &\cdots
	  &\frac{\partial r_N}{\partial p_M}
	  \end{bmatrix}\quad.`$
	  
  Note that 
  
  $`\displaystyle 
  J_{ij} = \frac{\partial r_i}{\partial p_j} = 
  \frac{\partial}{\partial p_j}\left(y_i-f(x_i;p)\right)=
  -\frac{\partial}{\partial p_j}f(x_i;p)\quad,`$
  
  and thus the $`i`$ row of $`J`$ is the negative gradient, with
  respect to the parameters, of $`f`$ at the $`i`$ point
  
  $`\displaystyle 
  J_i = \nabla_p f(x_i;p)\quad.
  `$
  
  Note that if this function is not specified and `nullptr` is passed
  for this argument, then the GSL library will numerically evaluate
  $`J`$ in so far it is needed. 

- Optionally, a _Callable_ that calculates the second directional
  derivative of the residuals at each data point $`i`$
  
  $`\displaystyle 
  v^T H_i v = 
	  \begin{bmatrix} v_1&\cdots&v_M\end{bmatrix}
	\begin{bmatrix}
	  \frac{\partial^2 r_i}{\partial p_1^2}
	  &\cdots
	  &\frac{\partial^2 r_i}{\partial_1\partial p_M}\cr
	  \vdots
	  & \ddots
	  & \vdots
	  \cr
	  \frac{\partial^2r_i}{\partial p_1\partial_M}
	  &\cdots
	  &\frac{\partial^2 r_i}{\partial p_M^2}\cr
	  \end{bmatrix}
  	\begin{bmatrix} v_1\cr \vdots \cr v_M\end{bmatrix}
	= \sum_j\sum_k v_j
	\underbrace{\frac{\partial^2f_i}{\partial p_j\partial_k}}_{H_{i,jk}}
	v_k
	\quad.`$
		
  That is, the velocity $`v`$ (with respect to the parameters),
  transposed, times the Hessian (second derivative) matrix at the
  point $`i`$, times the velocity.  Note that
 
  $`\displaystyle 
    H_i = \begin{bmatrix}
	  \frac{\partial^2 r_i}{\partial p_1^2}
	  &\cdots
	  &\frac{\partial^2 r_i}{\partial_1\partial p_M}\cr
	  \vdots
	  &\ddots
	  &\vdots\cr
	  \frac{\partial^2r_i}{\partial p_1\partial_M}
	  &\cdots
	  &\frac{\partial^2 r_i}{\partial p_M^2}
	  \end{bmatrix}
	  = 
	  \begin{bmatrix}
	  \frac{\partial^2}{\partial p_1^2}\left(y_i - f(x_i;p)\right)
	  &\cdots
	  &\frac{\partial^2}{\partial_1\partial p_M}\left(y_i - f(x_i;p)\right)\cr
	  \vdots
	  &\ddots
	  &\vdots\cr
	  \frac{\partial^2}{\partial p_1\partial_M}\left(y_i - f(x_i;p)\right)
	  &\cdots
	  &\frac{\partial^2}{\partial p_M^2}\left(y_i - f(x_i;p)\right)
	  \end{bmatrix}
	  = 
	 -\begin{bmatrix}
	  \frac{\partial^2}{\partial p_1^2}f(x_i;p)
	  &\cdots
	  &\frac{\partial^2}{\partial_1\partial p_M}f(x_i;p)\cr
	  \vdots
	  &\ddots
	  &\vdots\cr
	  \frac{\partial^2}{\partial p_1\partial_M}f(x_i;p)
	  &\cdots
	  &\frac{\partial^2}{\partial p_M^2}f(x_i;p)
	  \end{bmatrix}
      =-\nabla_p\nabla_p f(x_i;p)\quad,
	  `$ 
	  
  if the negative second-order gradient, with respect to the
  parameters $`p`$ - of the function $`f`$ being fitted to the data. 

  Note that if this function is not specified and `nullptr` is passed
  for this argument, then the GSL library will numerically evaluate
  $`v^THv`$ in so far it is needed.

- Finally, and optionally, a verbosity level. 

The function will return an object of an appropriate instance of the
`gsl::fit_result`  class template.   This object contains  

- The over all status of the fit 
- The $`\chi^2`$ and number degrees of freedom ($`\nu`$) of the fit 
- The best-fit parameters 
- The best-fit parameter covariance (the uncertainties on the
  parameters are given by the square root of the diagonal of that
  matrix). 
- The number of iterations 
- The number of calls to the user supplied _Callables_

Thus, the user will need to at least 

- Define the data - for example 

  ~~~C++
  struct data { 
    std::valarray<double> x;
    std::valarray<double> y;
    std::valarray<double> e;
  };

  data d = ...;
  ~~~
	  
- A function to calculate the weights from the data - for example 

  ~~~C++
  valarray<double> weights(const data& data) { 
    auto w      = 1. / std::pow(data.e,2);
    w[e < 1e-9] = 0;
    return w;
  }
  ~~~
  
- Define a function that calculates the residuals at all points.
  Suppose the function $`f(x;p)`$ is calculated by the function `f`,
  then for example
  
  ~~~C++
  valarray<double> residuals(const data& data, 
                             const valarray<double>& parameters) {
    return data.y - f(data.x,parameters);
  }
  ~~~
  
  An example of `f` could be 
  
  ~~~C++
  valarray<double> f(const valarray<double>& x,
                     const valarray<double>& p) { 
    double a     = p[0];
    double mu    = p[1];
    double sigma = p[2];
  	 
  	return a / ((std::sqrt(2 * M_PI) * sigma)) 
       * std::exp(-std::pow((x - mu) / sigma, 2)/2)
  }
  ~~~ 
  
  for a scaled normal distribution. 
   
- A guess of the initial parameter values.  The top of these
  parameters will determine the second argument type of the functions
  `residuals` and `f` above, as well as the return type of the
  functions above.  So for example 
  
  ~~~C++
  std::valarray<double> p0{1,2,3};
  ~~~
	  
We can then perform our fit with 

~~~C++
auto result = gsl::fit(gsl::fit_config(),
                       p0,
                       d,
                       d.x.size(),
                       residuals,
                       weights);
~~~

See also the example [`simple.cc`](simple.cc).  Note that this example
used the service `utils::frozen_histogram` from [`utils.hh`](utils.hh)
to define the data.  See also the documentation of that class template
and the associate module documentation. 

## Even simpler interface

As we noted above, the residuals are 

$`\displaystyle 
r_i = y_i - f(x_i;p)\quad,
`$ 

the Jacobian 

$`\displaystyle 
J_i = -\nabla_p f(x_i;p)\quad,
`$

and the Hessian 

$`\displaystyle 
H_i = -\nabla_p\nabla_p f(x_i;p)\quad. 
`$

Thus, if we know the function $`f`$ and how to access $`(x_i,y_i)`$
from our data type, we can formulate even simpler wrappers.  This is
done in the class template `utils::wrapper` and creator function
template `utils::make_wrapper` in the file [`utils.hh`](utils.hh).

If the user has defined the functions 

- `f` that encodes $`f(x;p)`$, and optionally 
- `df` that encodes $`\nabla_p f(x;p)`$, and (equally optionally)
- `d2f` that encodes $`\nabla_p\nabla_p f(x;p)`$ 

and the data structure allows access to $`x`$, $`y`$, and $`e`$ (the
uncertainty on $`y`$) via data members `x`, `y`¸`e`, and the initial
parameter values are `p0`, then the call 

~~~C++
auto wrapped = utils::make_wrappers(data,p0,f,df,d2f);
~~~ 

returns a four-`std::tuple` with wrapper functions that 

1. calculates the residuals 
2. calculates the weights 
3. (optionally) calculates the Jacobian 
4. (optionally) calculates the second direction derivative $`v^THv`$ 

We can pass these on to `gsl::fit` like 

~~~C++
auto result = gsl::fit(gsl::fit_config(),
                       p0, data, data.x.size(), 
                       std:get<0>(wrapped),
                       std:get<1>(wrapped),
                       std:get<2>(wrapped),
                       std:get<3>(wrapped));
~~~ 

Note that we can write 

$`\displaystyle 
v^TH_iv = 
\sum_j\sum_k v_j
\underbrace{\frac{\partial^2f(x_i;p)}{\partial p_j\partial p_k}}_{H_{i,jk}}
v_k =
(vv^T)\cdot H_i\quad,
`$ 

where $`vv^T`$ is the [_outer product_](https://en.wikipedia.org/wiki/Outer_product) (a matrix) of the velocity
vectors, and $`\cdot`$ is the [_inner product_](https://en.wikipedia.org/wiki/Dot_product#Dyadics_and_matrices). By
using this, the class template utils::wrapper only needs the Hessian
(second order gradient) of the function $`f`$. 

This mechanism is exploited in the class template `example::example`
and function template `example::make_example` which forms the basis of
the examples 

- [`expo.cc`](expo.cc), 
- [`normal.cc`](normal.cc),
- [`unormal.cc`](unormal.cc), and 
- [`lorentz.cc`](lorentz.cc). 

### Histogramming 

The file [`histogram.hh`](histogram.hh) contains a simple histogram
class.  The file is copied verbatim form the
[`nd_histogram`](https://gitlab.com/cholmcc/nd_histogram) project. 
  
This is used in the example [`simple.cc`](simple.cc). 

## YODA 

The header [`YodaGslFit.hh`](YodaGslFit.hh) provides convenience
wrappers for fitting [YODA](https://yoda.hepforge.ort) objects. 

Similar to the `utils::wrapper` class template, this header contains
the class template `YODA::GSL::Trait` which allows transparent access
to data in the YODA data objects `YODA::Histogram1D`,
`YODA::Profile1D`, and `YODA::Scatter2D` (higher dimensional classes
will be taken care of later).  

The function `YODA::GSL::fit` then uses these traits to provide a
simple interface to fit a function to objects of these classes.
Suppose that the user has defined 

- `f` that encodes $`f(x;p)`$, and optionally 
- `df` that encodes $`\nabla_p f(x;p)`$, and (equally optionally)
- `d2f` that encodes $`\nabla_p\nabla_p f(x;p)`$ 

and has an object of one of the above classes in `data`, as well as
the initial parameter values in `p0`, then 

~~~C++
auto YODA::GSL::fit(YODA::GSL::FitConfig(),
                    p0, data, f, df, d2f);
~~~                 

will fit the function $`f`$ to that data an return the result in
`result`. Note, `YODA::GSL::FitConfig` is merely an alias for
`gsl::fit_config`. 

Since access to $`x`$ and $`y`$ of the YODA objects is one-by-one, the
functions must have the following prototypes 

~~~C++
Parameters f  (double x, const Parameters& p);
Parameters df (double x, const Parameters& p);
Parameters d2f(double x, const Parameters& p);
~~~ 

where `Parameters` is the container type of the initial parameter
values `p0` (e.g., `std::valarray<double>`).   The type
`Parameters`can be any _Iterable_ type has a constructor like 

~~~C++
Parameters::Parameters(size_t);
~~~

## Meaning of "Iterable"

By _Iterable_ we mean any type `T` for which the following is valid 

~~~C++
size_t n = 10;
T      x(n);

for (auto i = std::begin(x); i != std:end(x); ++i) 
  *i = 1;
  
assert n == std::distance(std::begin(x), std::end(x));
~~~

This include practically all _Standard Template Library_ containers
such as `std::valarray` (recommended), `std::vector`, `std::deque`,
and so on. 

## Build examples 

Build plain examples 

	c++ -I. expo.cc -lgsl -o expo
	c++ -I. normal.cc -lgsl -o normal
	c++ -I. unormal.cc -lgsl -o unormal	
	
Build YODA example 

	c++ yodatest.cc -lgsl -I. `yoda-config --cflags --libs` -o yodatest

## Plots 

Plots in this documentation was made in Python using the package
[`nbi_stat`](https://cholmcc.gitlab.io/nbi-python/statistics/nbi_stat/)
(in particular the function `nbi_stat.fit_plot`).  No C++ plotting
routines (which are sorely missing) are used.
