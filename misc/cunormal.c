#include "cgslfit.h"

/* model function: a * exp( -1/2 * [ (t - b) / c ]^2 ) */
double
f(const double x, const double a, const double mu, const double sigma)
{
  const double z = (x - mu) / sigma;
  return (a * exp(-0.5 * z * z));
}

int r(const gsl_vector* p, void *params, gsl_vector* r)
{
  data_t* d     = (data_t*) params;
  double  a     = gsl_vector_get(p, 0);
  double  mu    = gsl_vector_get(p, 1);
  double  sigma = gsl_vector_get(p, 2);

  for (size_t i = 0; i < d->n; ++i) {
    double xi = d->x[i];
    double yi = d->y[i];
    double y  = f(xi, a, mu, sigma);

    gsl_vector_set(r, i, yi - y);
  }

  return GSL_SUCCESS;
}

int dr(const gsl_vector* p, void* params, gsl_matrix* j)
{
  data_t* d     = (data_t*) params;
  double  a     = gsl_vector_get(p, 0);
  double  mu    = gsl_vector_get(p, 1);
  double  sigma = gsl_vector_get(p, 2);

  for (size_t i = 0; i < d->n; ++i) {
    double xi = d->x[i];
    double zi = (xi - mu) / sigma;
    double ei = f(xi,1,mu,sigma);

    gsl_matrix_set(j, i, 0, -ei);
    gsl_matrix_set(j, i, 1, -(a / sigma) * ei * zi);
    gsl_matrix_set(j, i, 2, -(a / sigma) * ei * zi * zi);
  }

  return GSL_SUCCESS;
}

int d2r(const gsl_vector* p, const gsl_vector* v,
	void* params, gsl_vector* h)
{
  data_t* d      = (data_t*) params;
  double  a      = gsl_vector_get(p, 0);
  double  mu     = gsl_vector_get(p, 1);
  double  sigma  = gsl_vector_get(p, 2);
  double  va     = gsl_vector_get(v, 0);
  double  vmu    = gsl_vector_get(v, 1);
  double  vsigma = gsl_vector_get(v, 2);

  for (size_t i = 0; i < d->n; ++i) {
    double xi = d->x[i];
    double zi = (xi - mu) / sigma;
    double ei = f(xi,1,mu,sigma);

    double damu        = -   zi *      ei / sigma;
    double dasigma     = -   zi * zi * ei / sigma;
    double dmumu       = a *           ei / (sigma * sigma) * (1 - zi*zi);
    double dmusigma    = a * zi *      ei / (sigma * sigma) * (2 - zi*zi);
    double dsigmasigma = a * zi * zi * ei / (sigma * sigma) * (3 - zi*zi);
    double sum = \
      2 * va  * vmu    * damu     +
      2 * va  * vsigma * dasigma  +
      2 * vmu * vsigma * dmusigma +
      vmu     * vmu    * dmumu    +
      vsigma  * vsigma * dsigmasigma;

    gsl_vector_set(h, i, sum);
  }
  
  return GSL_SUCCESS;
}

/* ---------------------------------------------------------------- */
data_t* make_data(size_t n, double a, double mu, double sigma)
{
  gsl_rng* r;
  data_t*  d;

  gsl_rng_env_setup();
  r = gsl_rng_alloc(gsl_rng_default);

  d = (data_t*)malloc(sizeof(data_t));
  d->n = n;
  d->x = (double*)malloc(n*sizeof(double));
  d->y = (double*)malloc(n*sizeof(double));
  d->e = (double*)malloc(n*sizeof(double));

  for (size_t i = 0; i < n; i++) {
    double xi = (double)i / (double) n;
    double yi = f(xi, a, mu, sigma);
    double si = yi / 10;
    double dy = gsl_ran_gaussian (r, si);

    d->x[i] = xi;
    d->y[i] = yi + dy;
    d->e[i] = si;
  }

  gsl_rng_free (r);

  return d;
}

/* ---------------------------------------------------------------- */
void usage(const char* progname)
{
  fprintf(stdout,
	  "Usage; %s [OPTIONS]\n\n"
	  "Options:\n"
	  "  -h         this help\n"
	  "  -v LEVEL   Set verbosity\n"
	  "  -f FUNCS   which functions to use\n\n"
	  "FUNCS is one of\n\n"
	  "- 0 numeric Jacobian and Hessian\n"
	  "- 1 analytic Jacobian, numeric Hessian\n"
	  "- 2 analytic Jacobian and Hessian\n");
}
  
/* ---------------------------------------------------------------- */
int
main(int argc, char** argv)
{
  const size_t n = 300;

  int            ifit    = 0;
  int            verbose = 0;
  int            repeat  = 1;
  int            cnt     = repeat - 1;
  gsl_vector*    p0      = gsl_vector_alloc(3);
  data_t*        data    = make_data(n, 5, 0.4, 0.15);
  
  /* Set initial guess */
  gsl_vector_set(p0,0,1);
  gsl_vector_set(p0,1,0);
  gsl_vector_set(p0,2,1);

  for (size_t i = 1; i < argc; i++) {
    if (argv[i][0] != '-') {
      fprintf(stderr, "%s: Unknown option: %s\n", argv[0], argv[i]);
      return 1;
    }

    switch(argv[i][1]) {
    case 'v': verbose = atoi(argv[++i]); break;
    case 'f': ifit    = atoi(argv[++i]); break;
    case 'r': repeat  = atoi(argv[++i]); break;
    case 'h': usage(argv[0]); return 0;
    default:
      fprintf(stderr, "%s: Unknown option: %s\n", argv[0], argv[i]);
      return 1;
    }
  }      

  driver(ifit, verbose, repeat, p0, data, r, dr, NULL);
  
  free(data);
  
  return 0;
}
// Local Variables:
//   compile-command: "gcc cunormal.c -O2 -lgsl -lm -o cunormal"
// End:

