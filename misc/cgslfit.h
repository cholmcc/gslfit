#ifndef CGSLFIT_H
#define CGSLFIT_H
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_multifit_nlinear.h>

/* ---------------------------------------------------------------- */
typedef struct data {
  size_t  n;
  double* x;
  double* y;
  double* e;
} data_t;

/* ================================================================ */
typedef struct context {
  int   verbose;
  FILE* out;
} context_t;

/* ---------------------------------------------------------------- */
void callback(const size_t                          iter,
	      void*                                 params,
	      const gsl_multifit_nlinear_workspace* work)
{
  context_t*  ctx = (context_t*)params;
  gsl_vector* r   = 0;
  gsl_vector* p   = 0;
  gsl_matrix* jac = 0;
  double      rcond;
  double      chisq;
  
  if (ctx->verbose <= 0) return;

  if (iter == 0)
    fprintf(ctx->out, "Solver: %s, method: %s\n",
	    gsl_multifit_nlinear_name(work),
	    gsl_multifit_nlinear_trs_name(work));

  if (ctx->verbose <= 1) return;

  r    = gsl_multifit_nlinear_residual(work);
  p    = gsl_multifit_nlinear_position(work);
  gsl_blas_ddot(r, r, &chisq);
        
  /* compute reciprocal condition number of J(x) */
  gsl_multifit_nlinear_rcond(&rcond, work);

  fprintf(ctx->out, "%3zu:", iter);
  for (size_t i = 0; i < p->size; i++)
    fprintf(ctx->out, "%10.4f, ", gsl_vector_get(p,i));
  fprintf(ctx->out, " chi^2=%10.4f", chisq);
  
  if (ctx->verbose > 2) {
    gsl_multifit_nlinear_rcond(&rcond, work);
    fprintf(ctx->out, " condition=%10f",1/rcond);
  }
  if (ctx->verbose > 3) 
    fprintf(ctx->out, " |a/v|=%10f",
	    gsl_multifit_nlinear_avratio(work));

  fprintf(ctx->out, "\n");
  
  if (ctx->verbose > 4) {
    jac = gsl_multifit_nlinear_jac(work);

    for (size_t i = 0; i < jac->size1; i++) {
      fprintf(ctx->out, "%12f | ", gsl_vector_get(r,i));

      for (size_t j = 0; j < jac->size2; j++)
	fprintf(ctx->out,"%12f", gsl_matrix_get(jac,i,j));
      fprintf(ctx->out,"\n");
    }
  }
}


/* ================================================================ */
typedef struct lap_timer {
  clock_t start;
  clock_t prev;
  double sum;
  double sum2;
  size_t cnt;
} lap_timer_t;

/* ---------------------------------------------------------------- */
void start_timer(lap_timer_t* t) {
  clock_t now = clock();
  t->start = now;
  t->prev  = now;
  t->sum   = 0;
  t->sum2  = 0;
  t->cnt   = 0;
};

/* ---------------------------------------------------------------- */
void lap_timer(lap_timer_t* t) {
  clock_t now  = clock();
  double  diff = (now - t->prev) * 1000. / CLOCKS_PER_SEC;
  t->prev      = now;
  t->sum       += diff;
  t->sum2      += diff * diff;
  t->cnt++;
}

/* ----------------------------------------------------------------- */
void stop_timer(lap_timer_t* t) {
  double mean  = t->sum  / t->cnt;
  double mean2 = t->sum2 / t->cnt;
  double var   = mean2 - mean * mean;
  double sem   = sqrt(var / t->cnt);

  printf("Elapsed: %f ms for %d laps, average lap: (%f +/- %f) ms\n",
	 t->sum, t->cnt, mean, sem);
}

/* ================================================================ */
typedef struct fit_config {
  gsl_multifit_nlinear_parameters params;
  double xtol;
  double gtol;
  double ftol;
  int    nmax;
} fit_config_t;

/* ================================================================ */
typedef struct fit_result {
  int         info;
  double      chisq;
  size_t      nu;
  gsl_vector* p;
  gsl_matrix* covar;
  size_t      niter;
  size_t      nr;
  size_t      ndr;
  size_t      nd2r;
} fit_result_t;


/* ---------------------------------------------------------------- */
void print_fit_result(fit_result_t* r)
{
#define BAR    "------------"
  
  printf("chi^2/nu: %f/%d=%f (%s)\n",
	 r->chisq, r->nu, r->chisq/r->nu,
	 (r->info == 0 ? "failed" :
	  r->info == 1 ? "small step size" : "small gradient"));
  
  printf("  # | %12.8s | %12.8s |", "Value", "Error");
  for (size_t i = 0; i < r->p->size; i++) printf("%12d", i);

  printf("\n----+-%12s-+-%12s-+",BAR,BAR);
  for (size_t i = 0; i < r->p->size; i++) printf("%12s", BAR);

  printf("\n");

  for (size_t i = 0; i < r->p->size; i++) {
    printf ("%3d | %12f | %12f |",i,
	    gsl_vector_get(r->p,i),
	    sqrt(gsl_matrix_get(r->covar, i, i)));
    for (size_t j = 0; j < r->p->size; j++) 
      printf("%12.8f",gsl_matrix_get(r->covar,i,j));
    printf("\n");
  }

  printf("# iterations: %d # calls %zu(residuals) %zu(jacobian) %zu(hessian)\n",
	 r->niter, r->nr, r->ndr, r->nd2r);
}

/* ================================================================ */
typedef int (*residuals_t)(const gsl_vector*,void*,gsl_vector*);
typedef int (*jacobian_t)(const gsl_vector*,void*,gsl_matrix*);
typedef int (*hessian_t)(const gsl_vector*,const gsl_vector*,void*,gsl_vector*);

/* ---------------------------------------------------------------- */
int fit(fit_config_t* cfg,
	context_t*    ctx,
	fit_result_t* res,
	data_t*       data,
	gsl_vector*   p0,
	residuals_t   residuals,
	jacobian_t    jacobian,
	hessian_t     hessian)
{
  size_t                           n       = data->n;
  size_t                           p       = p0->size;
  gsl_multifit_nlinear_workspace*  work    = 0;
  gsl_multifit_nlinear_fdf         fdf;	  
  gsl_vector*                      r       = 0;
  gsl_vector*                      weights = gsl_vector_alloc(n);
  gsl_matrix*                      jac     = 0;
  double                           chisq0  = 0;
  int                              status  = 0;
  
  /* define the function to be minimized */
  fdf.f      = residuals;
  fdf.df     = jacobian;
  fdf.fvv    = hessian;
  fdf.n      = n;
  fdf.p      = p;
  fdf.params = data;

  /* Calculate weights, taking care to zero where uncerainty is small */
  for (size_t i = 0; i < data->n; i++) {
    double w = data->e[i] < 1e-9 ? 0 : 1/pow(data->e[i],2);
    gsl_vector_set(weights,i,w);
  }

  /* allocate workspace */
  work = gsl_multifit_nlinear_alloc(gsl_multifit_nlinear_trust,
				    &cfg->params, n, p);

  /* initialize solver with starting point and weights */
  gsl_multifit_nlinear_winit (p0, weights, &fdf, work);

  /* solve the system with a maximum of 100 iterations */
  status = gsl_multifit_nlinear_driver(cfg->nmax,
				       cfg->xtol,
				       cfg->gtol,
				       cfg->ftol,
                                       callback, ctx,
				       &res->info,
				       work);

  
  /* compute covariance of best fit parameters */
  jac = gsl_multifit_nlinear_jac(work);
  gsl_multifit_nlinear_covar(jac, 0.0, res->covar);

  /* compute final cost */
  r = gsl_multifit_nlinear_residual(work);
  gsl_blas_ddot(r, r, &res->chisq);

  /* set the result */
  for (size_t i = 0; i < p; i++)
    gsl_vector_set(res->p, i, gsl_vector_get(work->x,i));

  res->niter = gsl_multifit_nlinear_niter(work);
  res->nr    = fdf.nevalf;
  res->ndr   = fdf.nevaldf;
  res->nd2r  = fdf.nevalfvv;

  gsl_multifit_nlinear_free(work);

  return status;
}

/* ================================================================ */
void driver(int          ifit,
	    int          verbose,
	    int          repeat,
	    gsl_vector*  p0,
	    data_t*      data,
	    residuals_t  residuals,
	    jacobian_t   jacobian,
	    hessian_t    hessian)
{
  int            cnt    = repeat - 1;
  fit_config_t   cfg;
  fit_result_t   res;
  context_t      ctx;
  lap_timer_t    tmr;
  
  /* Allocate room for covariance and parameters */
  res.nu    = data->n - p0->size;
  res.p     = gsl_vector_alloc(p0->size);
  res.covar = gsl_matrix_alloc(p0->size,p0->size);
  
  /* Set fit configuration */
  cfg.params = gsl_multifit_nlinear_default_parameters();
  cfg.xtol   = 1e-8;
  cfg.gtol   = 1e-8;
  cfg.ftol   = 0;
  cfg.nmax   = 100;
  
  /* set call-back context */
  ctx.verbose = verbose;
  ctx.out     = stdout;

  /* setup timer */
  cnt = repeat - 1;
  start_timer(&tmr);

  do {
    int status = fit(&cfg,
		     &ctx,
		     &res,
		     data,
		     p0,
		     residuals,
		     ifit > 0 ? jacobian : NULL,
		     ifit > 0 ? hessian : NULL);
    lap_timer(&tmr);

    if (cnt == 0) 
      print_fit_result(&res);
    else if (cnt % (repeat / 10) == 0)
      printf("Repition %d\n", cnt);
  } while (--cnt >= 0);

  stop_timer(&tmr);
}
#endif
