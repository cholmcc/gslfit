''' Make some deductions for a un-normalised normal distribution 

    Author   : Christian Holm Christensen <cholmcc@gmail.com>
    Date     : 18 Aug 2021
    Copyright: (c) 2021 Christian Holm Christensen. 
    License  : LGPL-3
'''
from sympy import symbols, exp, pi, sqrt, Eq, Derivative, init_printing, simplify, lambdify

init_printing()

x, mu = symbols('x mu',real=True)
a, sigma = symbols('a sigma',real=True,positive=True)

z = (x - mu) / sigma
s = exp(-z**2/2)
f = a * s

fn, zn, sn = symbols('f z s')

display(Eq(fn,f))

dfda     = f.diff(a)
dfdmu    = f.diff(mu)
dfdsigma = f.diff(sigma)

def nd(f,x):
    return Derivative(f,x)

def nd2(f,x,y):
    return Derivative(f,x,y)

display(Eq(nd(fn,a),     dfda))
display(Eq(nd(fn,mu),    dfdmu))
display(Eq(nd(fn,sigma), dfdsigma))

display(Eq(nd(fn,a),       sn))
assert simplify(dfda     - s) == 0
display(Eq(nd(fn,mu),      fn * zn / sigma))
assert simplify(dfdmu    - f  * z  / sigma) == 0
display(Eq(nd(fn,sigma),   fn * zn**2 / sigma))
assert simplify(dfdsigma - f  * z **2 / sigma) == 0

 
d2fdada         = f.diff(a,    a)
d2fdadmu        = f.diff(a,    mu)
d2fdadsigma     = f.diff(a,    sigma)
d2fdmudmu       = f.diff(mu,   mu)
d2fdmudsigma    = f.diff(mu,   sigma)
d2fdsigmadsigma = f.diff(sigma,sigma)



display(Eq(nd2(fn,a,    a),       d2fdada))
display(Eq(nd2(fn,a,    mu),      d2fdadmu))
display(Eq(nd2(fn,a,    sigma),   d2fdadsigma))
display(Eq(nd2(fn,mu,   mu),      d2fdmudmu))
display(Eq(nd2(fn,mu,   sigma),   d2fdmudsigma))
display(Eq(nd2(fn,sigma,sigma),   d2fdsigmadsigma))

display(Eq(nd2(fn,a,    a),       0))
assert simplify(d2fdada	        - 0) == 0
display(Eq(nd2(fn,a,    mu),      sn * zn / sigma))
assert simplify(d2fdadmu	- s  * z  / sigma) == 0
display(Eq(nd2(fn,a,    sigma),   sn * zn**2  / sigma))
assert simplify(d2fdadsigma	- s  * z **2  / sigma) == 0
display(Eq(nd2(fn,mu,   mu),      fn * (zn**2 - 1) / sigma**2))
assert simplify(d2fdmudmu	- f  * (z **2 - 1) / sigma**2) == 0
display(Eq(nd2(fn,mu,   sigma),   fn * zn * (zn**2 - 2) / sigma**2))
assert simplify(d2fdmudsigma	- f  * z  * (z **2 - 2) / sigma**2) == 0
display(Eq(nd2(fn,sigma,sigma),   fn * zn**2 * (zn**2 - 3) / sigma**2))
assert simplify(d2fdsigmadsigma - f  * z **2 * (z **2 - 3) / sigma**2) == 0


