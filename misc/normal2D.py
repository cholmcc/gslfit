''' Make some deductions for a normal distribution 

    Author   : Christian Holm Christensen <cholmcc@gmail.com>
    Date     : 18 Aug 2021
    Copyright: (c) 2021 Christian Holm Christensen. 
    License  : LGPL-3
'''
from sympy import symbols, exp, pi, sqrt, Eq, \
    Derivative, init_printing, simplify, lambdify

init_printing()

x, y, mux, muy = symbols('x y mu_x mu_y',real=True)
a, sigmax, sigmay = symbols('a sigma_x sigma_y',real=True,positive=True)

zx = (x - mux) / sigmax
zy = (y - muy) / sigmay
z  = sqrt(zx**2 + zy**2)
s  = 1 / ((2 * pi) * sigmax * sigmay) * exp(-(zx**2+zy**2)/2)
f  = a * s

fn, zn, znx, zny, sn = symbols('f z z_x z_y s')

display(Eq(fn,f))

dfda      = f.diff(a)
dfdmux    = f.diff(mux)
dfdsigmax = f.diff(sigmax)
dfdmuy    = f.diff(muy)
dfdsigmay = f.diff(sigmay)

def nd(f,x):
    return Derivative(f,x)

def nd2(f,x,y):
    return Derivative(f,x,y)

display(Eq(nd(fn,a),      dfda))
display(Eq(nd(fn,mux),    dfdmux))
display(Eq(nd(fn,muy),    dfdmuy))
display(Eq(nd(fn,sigmax), dfdsigmax))
display(Eq(nd(fn,sigmay), dfdsigmay))

display(Eq(nd(fn,a),       sn))
assert simplify(dfda     - s) == 0

display(Eq(nd(fn,mux),       fn * znx / sigmax))
assert simplify(dfdmux     - f  * zx  / sigmax) == 0

display(Eq(nd(fn,muy),       fn * zny / sigmay))
assert simplify(dfdmuy     - f  * zy  / sigmay) == 0

display(Eq(nd(fn,sigmax),   fn * (znx**2 - 1) / sigmax))
assert simplify(dfdsigmax - f  * (zx **2 - 1) / sigmax) == 0

display(Eq(nd(fn,sigmay),   fn * (zny**2 - 1) / sigmay))
assert simplify(dfdsigmay - f  * (zy **2 - 1) / sigmay) == 0


d2fdada         = f.diff(a,    a)
d2fdadmux       = f.diff(a,    mux)
# d2fdadsigma     = f.diff(a,    sigma)
# d2fdmudmu       = f.diff(mu,   mu)
# d2fdmudsigma    = f.diff(mu,   sigma)
# d2fdsigmadsigma = f.diff(sigma,sigma)
# 
# 
# 
display(Eq(nd2(fn,a,    a),       d2fdada))
display(Eq(nd2(fn,a,    mux),     d2fdadmux))
# display(Eq(nd2(fn,a,    sigma),   d2fdadsigma))
# display(Eq(nd2(fn,mu,   mu),      d2fdmudmu))
# display(Eq(nd2(fn,mu,   sigma),   d2fdmudsigma))
# display(Eq(nd2(fn,sigma,sigma),   d2fdsigmadsigma))
#

display(Eq(nd2(fn,a,    a),       0))
assert simplify(d2fdada	        - 0) == 0
display(Eq(nd2(fn,a,    mux),     sn / sigmax    * znx)) 
assert simplify(d2fdadmux	- s  / sigmax    * zx ) == 0
# display(Eq(nd2(fn,a,    sigma),   sn / sigma    * (zn**2 - 1)))
# assert simplify(d2fdadsigma	- s  / sigma    * (z **2 - 1)) == 0
# display(Eq(nd2(fn,mu,   mu),      fn / sigma**2 * (zn**2 - 1)))
# assert simplify(d2fdmudmu	- f  / sigma**2 * (z **2 - 1)) == 0
# display(Eq(nd2(fn,mu,   sigma),   fn / sigma**2 * zn * (zn**2 - 3)))
# assert simplify(d2fdmudsigma	- f  / sigma**2 * z  * (z **2 - 3)) == 0
# display(Eq(nd2(fn,sigma,sigma),   fn / sigma**2 * (2 + zn**2 * (zn**2 - 5))))
# assert simplify(d2fdsigmadsigma - f  / sigma**2 * (2 + z **2 * (z **2 - 5))) == 0


