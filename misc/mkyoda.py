''' Create YODA data objects. 

    Author   : Christian Holm Christensen <cholmcc@gmail.com>
    Date     : 18 Aug 2021
    Copyright: (c) 2021 Christian Holm Christensen. 
    License  : LGPL-3
'''

# ====================================================================
class Data2D:
    def __init__(self):
        self.obj = None
        pass

    def plot(self,ax=None,**kwargs):
        from matplotlib.pyplot import gca
        
        ax = ax if ax is not None else gca()

        ax.errorbar(self.data[:,0],
                    self.data[:,1],
                    self.data[:,3],
                    self.data[:,2],
                    fmt='.',
                    label=self.obj.title(),
                    **kwargs)

    @property
    def data(self):
        raise NotImplemented()


# --------------------------------------------------------------------
class H1D(Data2D):
    def __init__(self, random, n=100, nev=10):
        from yoda import Histo1D;
    
        self.obj = Histo1D(30,-3,3,'histo1D','Histo1D')
        
        for x in random.normal(size=n*nev):
            self.obj.fill(x)

        self.obj.scaleW(1/nev)

    @property
    def data(self):
        from numpy import asarray

        return asarray([[b.xMid(),b.height(),b.xWidth()/2,b.heightErr()]
                        for b in self.obj.bins()])
        

# --------------------------------------------------------------------
class P1D(Data2D):
    def __init__(self,random, n=100, nev=10):
        from yoda import Histo1D, Profile1D

        tmp = Histo1D(30,-3,3,'tmp','tmp')
        self.obj = Profile1D(30,-3,3,'profile1D','Profile1D')
    
        for _ in range(nev):
            tmp.reset()
        
            for x in random.normal(size=n):
                tmp.fill(x)

            for b in tmp.bins():
                self.obj.fill(b.xFocus(),b.height());

    @property
    def data(self):
        from numpy import asarray

        return asarray([[b.xMid(),b.mean(),b.xWidth()/2,b.stdErr()]
                        for b in self.obj.bins()])
        

# --------------------------------------------------------------------
class S2D(Data2D):
    def __init__(self,histo1D):
        from yoda import Scatter2D

        self.obj = Scatter2D('scatter2D','Scatter2D')

        for b in histo1D.bins():
            self.obj.addPoint(b.xMid(),b.height(),
                              b.xWidth()/2,b.heightErr())

    @property
    def data(self):
        from numpy import asarray

        return asarray([[p.x(),p.y(),p.xErrAvg(),p.yErrAvg()]
                        for p in self.obj.points()])
        
# ====================================================================
class Data3D:
    def __init__(self):
        self.obj = None
        pass

    def plot(self,ax=None,**kwargs):
        from matplotlib.pyplot import gca
    
        ax = ax if ax is not None else gca()

        ax.scatter(self.data[:,0],
                   self.data[:,1],
                   self.data[:,2]/100,
                   self.data[:,5],
                   label=self.obj.title(),
                   **kwargs)

    @property
    def data(self):
        raise NotImplemented()
    
# --------------------------------------------------------------------
class H2D(Data3D):
    def __init__(self,random, n=10000, nev=10):
        from yoda import Histo2D;
    
        self.obj = Histo2D(30,-3,3,30,-3,3,'histo2D','Histo2D')

        for x in random.normal(size=(n*nev,2)):
            self.obj.fill(*x)

        self.obj.scaleW(1/nev)

    @property
    def data(self):
        from numpy import asarray

        return asarray([[b.xMid(),b.yMid(),b.height(),
                         b.xWidth()/2,b.yWidth()/2,b.heightErr()]
                        for b in self.obj.bins()])

# --------------------------------------------------------------------
class P2D(Data3D):
    def __init__(self,random,n=10000, nev=10):
        from yoda import Histo2D, Profile2D

        tmp = Histo2D(30,-3,3,30,-3,3,'tmp','tmp')
        self.obj = Profile2D(30,-3,3,30,-3,3,'profile2D','Profile2D')
    
        for _ in range(nev):
            tmp.reset()
        
            for x in random.normal(size=(n,2)):
                tmp.fill(*x)

            for b in tmp.bins():
                self.obj.fill(b.xFocus(),b.yFocus(),b.height());
                
    @property
    def data(self):
        from numpy import asarray

        return asarray([[b.xMid(),b.yMid(),b.mean(),
                         b.xWidth()/2,b.yWidth()/2,b.stdErr()]
                        for b in self.obj.bins()])

# --------------------------------------------------------------------
class S3D(Data3D):
    def __init__(self,histo2D):
        from yoda import Scatter3D

        self.obj = Scatter3D('scatter3D','Scatter3D')

        for b in histo2D.bins():
            self.obj.addPoint(b.xMid(),b.yMid(),b.height(),
                              b.xWidth()/2,b.yWidth()/2,b.heightErr())

    @property 
    def data(self):
        from numpy import asarray

        return asarray([[p.x(),p.y(),p.z(),p.xErrAvg(),p.yErrAvg(),p.zErrAvg()]
                        for p in self.obj.points()])


# ====================================================================
from numpy.random import default_rng
from matplotlib.pyplot import ion, gca, subplots

ion()

random    = default_rng(123456)
histo1D   = H1D(random)
profile1D = P1D(random)
scatter2D = S2D(histo1D.obj)

histo2D   = H2D(random)
profile2D = P2D(random)
scatter3D = S3D(histo2D.obj)

objs      = [histo1D, profile1D, scatter2D,
             histo2D, profile2D, scatter3D]


fig, ax = subplots(ncols=3,
                   nrows=2,
                   sharey='row',
                   figsize=(10,10),
                   gridspec_kw={'wspace':0});


for o,a in zip(objs,ax.ravel()):
    o.plot(ax=a)
    a.legend();
    
fig.tight_layout();
fig.savefig('data.png')

from yoda import write

write([o.obj for o in objs],'data.yoda')

# ====================================================================
def f1(x,a,mu,sigma):
    from numpy import pi, sqrt, exp
    return a / (sqrt(2*pi)*sigma) * exp(-((x-mu)/sigma)**2/2)

def f2(xy,a,mux,muy,sigmax,sigmay):
    from numpy import pi, sqrt, exp
    x, y = xy.T
    return a / (2*pi*sigmax*sigmay) * \
        exp(-(((x-mux)/sigmax)**2+((y-muy)/sigmay)**2)/2)

from scipy.optimize import curve_fit

def fit1(data,ax):
    from nbi_stat import fit, fit_plot
    from numpy import sqrt

    d      = data.data
    e      = d[:,3]
    x      = d[:,0]
    y      = d[:,1]
    p, cov = fit(f1,x,y,[1,2,3],e)

    print(data.obj.title());
    for pv,pe in zip(p,sqrt(cov.diagonal())):
        print(f'{pv:10.4f} +/- {pe:10.4f}')

    fit_plot(x,y,e,f1,p,cov,ax=ax)

def fit2(data,ax,proj=False):
    from scipy.optimize import curve_fit
    from nbi_stat import plot_fit_table, chi2nu
    from numpy import meshgrid, sqrt, newaxis
    
    d      = data.data
    e      = d[:,5]
    x      = d[:,:2]
    y      = d[:,2]
    xe     = x[e>0]
    ye     = y[e>0]
    ee     = e[e>0]
    p, cov = curve_fit(f2,xe,ye,[1,2,2,3,3],ee,absolute_sigma=True)

    print(data.obj.title());
    for pv,pe in zip(p,sqrt(cov.diagonal())):
        print(f'{pv:10.4f} +/- {pe:10.4f}')
    
    ff    = f2(x,*p)
    xm = x[:,0].reshape((30,30))
    ym = x[:,1].reshape((30,30))
    em = e.reshape(30,30);
    zm = y.reshape(30,30);
    fm = ff.reshape((30,30))

    nu     = x.shape[0] - len(p)
    chi2,_ = chi2nu(x,y,f2,p,e)

    if not proj:
        data.plot(ax=ax);
        ax.contour(xm,ym,fm,10);
        plot_fit_table(p,sqrt(cov.diagonal()),1,(chi2,nu),ax=a)
    else:
        mn = None
        mx = None
        for x1,y1,e1,f1 in zip(ym[mn:mx],zm[mn:mx],em[mn:mx],fm[mn:mx]):
            # print(e1)
            l = a.errorbar(x1,y1,e1,fmt='.')
            a.plot(x1,f1,color=l[0].get_color())

fig, ax = subplots(ncols=3,
                   nrows=2,
                   sharey='row',
                   figsize=(10,10),
                   gridspec_kw={'wspace':0});

for a,d in zip(ax[0],objs[:3]):
    fit1(d,a)

for a,d in zip(ax[1],objs[3:]):
    fit2(d,a)
    
fig.tight_layout();
fig.savefig('fits.png')


from numpy import allclose

print(allclose(histo1D.data,scatter2D.data))
print(allclose(histo2D.data,scatter3D.data))
