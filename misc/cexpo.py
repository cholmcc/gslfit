from csv import reader 

with open('cexpo.dat','r') as file:
    r = reader(file,delimiter='\t')
    
    npar = -1
    ndat = -1
    p    = None
    cov  = []
    dat  = []
    for row in r:
        if len(row) < 1: continue
        
        if npar < 0 and len(row) == 1: 
            npar = int(row[0])
            continue

        if not p:
            p = [float(c) for c in row if c != '']
            continue 
            
        if len(cov) < npar:
            cov.append([float(c) for c in row if c != ''])
            continue 
            
        if p and cov and ndat < 0 and len(row) == 1:
            ndat = int(row[0]);
            continue 
            
        dat.append([float(c) for c in row[1:] if c != ''])

from numpy import asarray, exp

p   = asarray(p)
cov = asarray(cov)
dat = asarray(dat)

f   = lambda x,*p: p[0]*exp(-p[1]*x)+p[2]

from matplotlib.pyplot import gca, ion

ion()
ax = gca()

from nbi_stat import fit_plot

fit_plot(*dat.T,f,p,cov,ax=ax)
