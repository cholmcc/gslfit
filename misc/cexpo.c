#include "cgslfit.h"

/* ---------------------------------------------------------------- */
double f(double x, double a, double lambda, double b)
{
  return a * exp(-lambda * x) + b;
}

/* ---------------------------------------------------------------- */
int r(const gsl_vector* p,
      void*             data,
      gsl_vector*       r)
{
  data_t* d      = (data_t*)data;
  size_t  n      = d->n;
  double* x      = d->x;
  double* y      = d->y;
  double* e      = d->e;
  double  A      = gsl_vector_get(p, 0);
  double  lambda = gsl_vector_get(p, 1);
  double  b      = gsl_vector_get(p, 2);
  // double sq     = 0;
    
  for (size_t i = 0; i < n; i++) {
    /* Model fi = A * exp(-lambda * t_i) + b */
    double fi = f(x[i],A,lambda,b);
    // sq += pow((fi - y[i])/d->e[i],2);
    gsl_vector_set (r, i, fi - y[i]);
  }
  // fprintf(stdout,"%f\n",sq);
  return GSL_SUCCESS;
}

/* ---------------------------------------------------------------- */
int dr(const gsl_vector* p,
       void*             data,
       gsl_matrix*       j)
{
  data_t* d      = (data_t*)data;
  size_t  n      = d->n;
  double* x      = d->x;
  double  A      = gsl_vector_get (p, 0);
  double  lambda = gsl_vector_get (p, 1);

  size_t i;

  for (size_t i = 0; i < n; i++) {
    /* Jacobian matrix J(i,j) = dfi / dxj, */
    /* where fi  = (yi - ydi)/sigma[i],      */
    /*       yfi = A * exp(-lambda * t_i) + b  */
    /* and the pj are the parameters (A,lambda,b) */
    double e = f(x[i],1,lambda,0);
    gsl_matrix_set(j, i, 0, e);
    gsl_matrix_set(j, i, 1, -x[i] * A * e);
    gsl_matrix_set(j, i, 2, 1.0);
  }

  return GSL_SUCCESS;
}

/* ---------------------------------------------------------------- */
data_t* make_data(size_t n, double xmax)
{
  gsl_rng* r;
  data_t*  d;

  gsl_rng_env_setup();
  r = gsl_rng_alloc(gsl_rng_default);

  d = (data_t*)malloc(sizeof(data_t));
  d->n = n;
  d->x = (double*)malloc(n*sizeof(double));
  d->y = (double*)malloc(n*sizeof(double));
  d->e = (double*)malloc(n*sizeof(double));

  for (size_t i = 0; i < n; i++) {
    double xi = i * xmax / (n - 1);
    double yi = f(xi,5,1.5,1.0);
    double si = 0.1 * yi;
    double dy = gsl_ran_gaussian(r, si);

    d->x[i] = xi;
    d->y[i] = yi + dy;
    d->e[i] = si;
  }

  gsl_rng_free (r);

  return d;
}

/* ---------------------------------------------------------------- */
void usage(const char* progname)
{
  fprintf(stdout,
	  "Usage; %s [OPTIONS]\n\n"
	  "Options:\n"
	  "  -h         this help\n"
	  "  -v LEVEL   Set verbosity\n"
	  "  -f FUNCS   which functions to use\n\n"
	  "FUNCS is one of\n\n"
	  "- 0 numeric Jacobian and Hessian\n"
	  "- 1 analytic Jacobian, numeric Hessian\n"
	  "- 2 analytic Jacobian and Hessian\n");
}
  
/* ---------------------------------------------------------------- */
int
main(int argc, char** argv)
{
  const size_t n = 100;
  const size_t p = 3;

  int            ifit    = 0;
  int            verbose = 0;
  int            repeat  = 1;
  int            cnt     = repeat - 1;
  gsl_vector*    p0      = gsl_vector_alloc(3);
  data_t*        data    = make_data(n,3);
  
  /* Set initial guess */
  gsl_vector_set(p0,0,1);
  gsl_vector_set(p0,1,0);
  gsl_vector_set(p0,2,0);

  for (size_t i = 1; i < argc; i++) {
    if (argv[i][0] != '-') {
      fprintf(stderr, "%s: Unknown option: %s\n", argv[0], argv[i]);
      return 1;
    }

    switch(argv[i][1]) {
    case 'v': verbose = atoi(argv[++i]); break;
    case 'f': ifit    = atoi(argv[++i]); break;
    case 'r': repeat  = atoi(argv[++i]); break;
    case 'h': usage(argv[0]); return 0;
    default:
      fprintf(stderr, "%s: Unknown option: %s\n", argv[0], argv[i]);
      return 1;
    }
  }      

  driver(ifit, verbose, repeat, p0, data, r, dr, NULL);
  
  free(data);
  
  return 0;
}
// Local Variables:
//   compile-command: "gcc cexpo.c -O2 -lgsl -lm -o cexpo"
// End:

