from numpy import log, asarray, geomspace, loadtxt, sqrt
from nbi_stat import fit, fit_plot, plot_fit_func, chi2nu
from matplotlib.pyplot import subplots, ion

t0 = loadtxt('complexity_0.dat',delimiter=',')
t1 = loadtxt('complexity_1.dat',delimiter=',')
t2 = loadtxt('complexity_2.dat',delimiter=',')
ts = [t0,t1,t2]
ms = ['Numeric Jacobian and Hessian',
      'Analytic Jacobian, numeric Hessian',
      'Analytic Jacobian and Hessian']

f = [lambda n,a,b : b+a * n,
     lambda n,a,b : b+a * log(n),
     lambda n,a,b : b+a * n * log(n),
     lambda n,a,b : b+a * n**2,
     lambda n,a,b : b+a * n**3]
n = [r"$O(n)$",
     r"$O(\log n)$",
     r"$O(n\log n)$",
     r"$O(n^2)$",
     r"$O(n^3)$"]

ion()

def try_fits():
    fig, ax = subplots(nrows=len(ts),figsize=(10,8),
                       sharex=True,
                       gridspec_kw={'hspace':0})
    
    for mm,tt,aa in zip(ms,ts,ax):
        aa.set_yscale('log')
        aa.set_xscale('log')

        tx = tt[:,0];
        ty = tt[:,1]
        te = tt[:,2]
        aa.errorbar(tx,ty,te,fmt='.')
        
        for ii,(ff,nn) in enumerate(zip(f,n)):
            
            p, cov   = fit(ff,tx,ty,[1,0],te)
            tt       = geomspace(tx[0],tx[-1],100)
            chi2, nu = chi2nu(tx,ty,ff,p,te)
            plot_fit_func(tt,ff,p,cov,ax=aa,
                          label=fr'{nn}, $\chi^2/\nu={chi2/nu}$',
                          band={'color':f'C{ii+1}'});

            print(nn)
            for pv,pe in zip(p,sqrt(cov.diagonal())):
                print(f'  {pv:10.4f} +/- {pe:10.4f}')

        aa.legend(title=mm)
            
    ax[1].set_ylabel(r'Average time $\bar{t}$ (ms)')
    ax[-1].set_xlabel(r'Data size $n$')
    fig.suptitle(r'Fitting $\mathcal{N}[0,1]$')
    fig.tight_layout()
    fig.savefig('try.png')


def final():
    
    fig, ax = subplots(nrows=len(ts),figsize=(10,8),
                       sharex=True,
                       gridspec_kw={'hspace':0})

    ff = f[2];
    nn = n[2];
    
    for mm,tt,aa in zip(ms,ts,ax):
        aa.set_yscale('log')
        aa.set_xscale('log')

        tx = tt[:,0];
        ty = tt[:,1]
        te = tt[:,2]

    
        p, cov   = fit(ff,tx,ty,[1,0],te)
        tt       = geomspace(tx[0],tx[-1],100)

        fit_plot(tx,ty,te,ff,p,cov,ax=aa,
                 data={'label':r'$\bar{t}$'},
                 fit={'label':f'{nn}'},
                 table={'loc':'lower right'},
                 legend={'title':mm})


    ax[1].set_ylabel(r'Average time $\bar{t}$ (ms)')
    ax[-1].set_xlabel(r'Data size $n$')
    fig.suptitle(r'Fitting $\mathcal{N}[0,1]$')
    fig.tight_layout()
    fig.savefig('complexity.png')



final()
