#
# Author    Christian Holm Christensen <cholmcc@gmail.com>
# Date      18 Aug 2021
# Copyright (c) 2021 Christian Holm Christensen. 
# License   LGPL-3
# Brief     Build instructions
#

GSL_CPPFLAGS	:= $(shell gsl-config --cflags)
GSL_LDFLAGS	:= $(filter-out -l%, $(shell gsl-config --libs))
GSL_LIBS	:= $(filter     -l%, $(shell gsl-config --libs))

YODA_CPPFLAGS	:= $(shell yoda-config --cflags)
YODA_LDFLAGS	:= $(filter-out -l%, $(shell yoda-config --libs))
YODA_LIBS	:= $(filter     -l%, $(shell yoda-config --libs))

GPROF_LIBS	:= -Wl,--no-as-needed -lprofiler -Wl,--as-needed
GPROF_FLAGS	:= -fno-omit-frame-pointer -g 

GMON_FLAGS	:= -pg -fno-omit-frame-pointer -g

PERF_FLAGS	:= -fno-omit-frame-pointer -g 

REPEAT		:= -r 1000

CXX		:= g++
CXXFLAGS	:= -O2 
CPPFLAGS	:= -I. $(GSL_CPPFLAGS)
LDFLAGS		:= $(GSL_LDFLAGS)
LIBS		:= $(GSL_LIBS)

ifdef PAR
CXXFLAGS	+= -std=c++17
LIBS		+= -ltbb
endif
ifdef TRANS
CPPFLAGS	+= -DTRANS=1
endif

HEADERS		:= gslfit.hh YodaGslFit.hh utils.hh example.hh
SOURCES		:= normal.cc 		\
		   unormal.cc 		\
		   expo.cc  		\
		   yodatest.cc  	\
		   simple.cc  		\
		   lorentz.cc 		\
		   mapdata.cc
PROGRAMS	:= $(SOURCES:%.cc=%)
DOC		:= doc/Doxyfile doc/DoxygenLayout.xml README.md
TIMES		:= $(foreach f, 0 1 2, \
			$(addsuffix -$(f).time, normal unormal yodatest)) \
		   $(foreach f, 0 1, \
			   $(addsuffix -$(f).time, expo))
PROFILES	:= $(TIMES:%.time=%.gprof.png)

%-gprof:%.cc
	$(CXX) $(CXXFLAGS) $(GPROF_FLAGS) $(CPPFLAGS) $< $(LDFLAGS) \
		$(LIBS) $(GPROF_LIBS) -o $@

%-gmon:%.cc
	$(CXX) $(CXXFLAGS) $(GMON_FLAGS) $(CPPFLAGS) $< $(LDFLAGS) $(LIBS) -o $@

%-perf:%.cc
	$(CXX) $(CXXFLAGS) $(PERF_FLAGS) $(CPPFLAGS) $< $(LDFLAGS) $(LIBS) -o $@

%:%.cc
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) $< $(LDFLAGS) $(LIBS) $(TC) -o $@



%.gprof:
	p=`echo $* | sed 's/-[012]//'`-gprof && \
	f=`echo $* | sed 's/.*-//'` && \
	$(MAKE) $$p && \
	env CPUPROFILE=$@ ./$$p -f $$f $(REPEAT)

%.gmon:
	p=`echo $* | sed 's/-[012]//'`-gmon && \
	f=`echo $* | sed 's/.*-//'` && \
	$(MAKE) $$p && \
	./$$p -f $$f $(REPEAT)
	mv gmon.out $@

# echo 2 > /proc/sys/kernel/perf_event_paranoid 
%.perf:
	p=`echo $* | sed 's/-[012]//'`-perf && \
	f=`echo $* | sed 's/.*-//'` && \
	$(MAKE) $$p && \
	perf record -g --user-callchains -o $@ -- ./$$p -f $$f $(REPEAT)


%.gprof.png:%.gprof
	google-pprof --cum --dot ./`echo $* | sed 's/-[012]/-gprof/'` $< | \
		dot -Tpng -o $@

%.perf.png:%.perf
	perf script -i $< | \
		./misc/gprof2dot.py -w -s -f perf | \
		dot -Tpng -o $@

%.gmon.png:%.gmon
	gprof `echo $* | sed 's/-[012]//'` $< | \
		./misc/gprof2dot.py -w -s | \
		dot -Tpng -o $@

%.time:
	p=`echo $* | sed 's/-[012]//'` && $(MAKE) $$p &&  \
	f=`echo $* | sed 's/.*-//'` &&  \
		./$$p -f $$f $(REPEAT) | \
		sed -n -e "s/.*average lap: (\(.*\)) ms/$* $$f: \1/p" \
		> $@

%.pmd:	%.md
	$(MUTE)sed 							   \
	    -e 's/\$$`/@f$$/g'						   \
	    -e 's/`\$$/@f$$/g'	 					   \
	    -e 's/~~~[Cc]++/~~~{.cc}/'					   \
	    -e 's,\[\([^]]*\)\](\([^)]*\)\(.cc\|.hh)\)),[\1](@ref \2\3),g' \
	    < $< > $@

all:	$(PROGRAMS)

doc:	html/index.html

timing:$(TIMES)
	cat $(sort $^) > $@

profiles:$(PROFILES)

clean:
	rm -f $(PROGRAMS)
	rm -f plot_*.py
	rm -f *.gch  gmon.out
	rm -f *.gprof *.gmon *.perf
	rm -f *-gprof *-gmon *-perf
	rm -f *.gprof.png *.gmon.png *.perf.png *.time 
	rm -rf html timing

data.yoda:misc/mkyoda.py
	python $< 

html/index.html:$(DOC) $(HEADERS) $(SOURCES) README.pmd 
	doxygen $<
	rm -f README.pmd

complexity_%.dat:normal
	for n in 10 20 50 100 200 500 1000 2000 5000 10000 20000 50000 100000; \
	do \
	  echo $$n > /dev/stderr ; \
	  ./$< -r 100 -N $$n | \
	  sed -n -e "s@.* lap: (\([0-9.]*\) +/- \([0-9.]*\)).*@$$n,\1,\2@p";\
	done >> $@

yodatest%:	CPPFLAGS:=$(CPPFLAGS) $(YODA_CPPFLAGS)
yodatest%:	LDFLAGS:=$(LDFLAGS) $(YODA_LDFLAGS)
yodatest%:	LIBS:=$(LIBS) $(YODA_LIBS)

expo:		expo.cc gslfit.hh utils.hh example.hh
normal:		normal.cc gslfit.hh utils.hh example.hh
unormal:	unormal.cc gslfit.hh utils.hh example.hh
lorentz:	lorentz.cc gslfit.hh utils.hh example.hh
yodatest:	yodatest.cc gslfit.hh utils.hh YodaGslFit.hh
yodatest:	CPPFLAGS:=$(CPPFLAGS) $(YODA_CPPFLAGS)
yodatest:	LDFLAGS:=$(LDFLAGS) $(YODA_LDFLAGS)
yodatest:	LIBS:=$(LIBS) $(YODA_LIBS)

