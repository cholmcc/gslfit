/**
   @file      normal.cc
   @author    Christian Holm Christensen <cholmcc@gmail.com>
   @date      18 Aug 2021
   @copyright (c) 2021 Christian Holm Christensen. 
   @license   LGPL-3
   @brief     Fit normal distribution to data
*/
#include <random>
#include <valarray>
#include <iostream>
#include <gslfit.hh>
#include <utils.hh>
#include <example.hh>

/** 
    @defgroup gslfit_examples Examples 

    Here are various examples of using the gsl::fit interface to the
    GNU Scientific Library fitting routines.

    * @ref gslfit_example_simple is an example of a simple program 
      that fits a function to data 

    * @ref gslfit_example_normal is an example of fitting a scaled
      (normalised) normal distribution to data.  
     
    * @ref gslfit_example_unormal is an example of fitting a scaled
      (unnormalised) normal distribution to data.  This corresponds to
      the [second geodesic
      example](https://www.gnu.org/software/gsl/doc/html/nls.html#geodesic-acceleration-example-2)
      in the GSL documentation.

    * @ref gslfit_example_expo is an example of fitting an exponential
      function to exponentional data.  This corresponds to the [first
      example](https://www.gnu.org/software/gsl/doc/html/nls.html#exponential-fitting-example)
      in the GSL documentation.

    * @ref gslfit_example_yoda is an example of reading in YODA
    * analysis objects and fitting a normal distribution to that.  

    Note that all example program provide help on various options
    through the command line option `-h`.  For example

         Usage: ./normal [OPTIONS]
         
         Options:
         
          -m METHOD   Choice of Trust region step method
          -f TYPE     Choice of fitting functions
          -v LEVEL    Verbosity level
          -h          This help
         
         TYPE is one of
         
         - 0 Numerical Jacobian and Hessian
         - 1 Analytic Jacobian, and numerical Hessian
         - 2 Analytic Jacobian and Hessian
         
         METHOD is one of
         
         - 0 Levenberg-Marquardt
         - 1 Levenberg-Marquardt w/acceleration
         - 2 Dogleg
         - 3 Double dogleg
         - 4 2D subspace
         - 5 Steihaug-Toint    

    The examples can be compiled with, f.ex., 

        c++ `gsl-config --cflags` -I. normal.cc `gsl-config --libs`  -o normal

    For the YODA example do 

        c++ `yoda-config --cflags` `gsl-config --cflags` -I. yodatest.cc `yoda-config --libs` `gsl-config --libs` -o yodatest


    @b Timing

    Average duration in miliseconds of performing fit on a 

        Intel(R) Core(TM) i7-4550U CPU @ 1.50GHz
      
    with 1,000 samples   

    | J | H | normal            | unormal           | expo              |
    |---|---|-------------------|-------------------|-------------------|
    | n | n | 0.228  +/- 0.001  | 1.570 +/- 0.006   | 0.0904 +/- 0.0007 |
    | a | n | 0.1841 +/- 0.0007 | 2.068 +/- 0.009   | 0.0884 +/- 0.0006 |
    | a | a | 0.237  +/- 0.001  | 2.696 +/- 0.008   |        n/a        |

    where <b>J</b>acobian or <b>H</b>essian are either <i>n</i>umerical or
    <i>a</i>nalytical determined.

    Same for the YODA interface 

    | J | H | yodatest          |
    |---|---|-------------------|
    | n | n | 0.2063 +/- 0.0009 |
    | a | n | 0.1590 +/- 0.0009 |
    | a | a | 0.2044 +/- 0.001  |
*/
/** 
    Namespace for examples 

    This namespace contains various examples 

    @ingroup gslfit_examples
*/
namespace example
{
  /** 
      @defgroup gslfit_example_normal Fitting normal distributed data 

      @ingroup gslfit_examples

      We fit the function 

      @f[ f(x;a,\mu,\sigma) = a\frac{1}{\sqrt{2\pi}\sigma}
      e^{-\frac12\left(\frac{x-\mu}{\sigma}\right)^2}
      \quad,
      @f] 
      
      to data.  

      The data is has @f$ N=31@f$ points and have 

      @f{align*}{
      x &= \left\{x_i=\left(i-\left\lfloor\frac{N}{2}\right\rfloor\right)
      \frac{6\sigma_0}{N-1}\middle|i=0,\ldots,N-1\right\}\\
      \delta &= 
      \left\{\delta_i 
      \sim\mathcal{N}\left[0,\frac1{10}f(x_i;a_0,\mu_0,\sigma_0)\right]
      \middle|i=1,\ldots,N-1\right\}\\
      y &= \left\{y_i=f(x_i;a_0,\mu_0,\sigma_0)+\delta_i\middle|
                  i=1,\ldots,N-1\right\}\\
      e &= \left\{e_i=\delta_i\middle|i=1,\ldots,N-1\right\}\quad,
      @f}

      where 
      
      @f{align*}{
      a_0      &= 100\\
      \mu_0    &= 0\\
      \sigma_0 &= 1 
      @f}      

      @image html normal.png

      The class example::normal::fit encodes the fitting function @f$
      f@f$, the gradient @f$J=\nabla f@f$, and the directional second
      derivative @f$ v^THv=v^T(\nabla\nabla f)v@f$. 

      The class example::unormal::data contains our data.  
      
      The class example::unormal::functions encodes the residual function 

      @f[r(x_i,a,\mu,\sigma) = y_i - f(x_i;a,\mu,\sigma)\quad,@f] 

      the gradient of that 

      @f[J=\nabla r=\begin{pmatrix}
      \frac{\partial r}{\partial a}\\
      \frac{\partial r}{\partial \mu}\\
      \frac{\partial r}{\partial \sigma}\\
      \end{pmatrix}\quad,
      @f] 
      
      as well as the second directional derivative 

      @f[v^THv = 
      \begin{pmatrix} v_a & v_\mu & v_\sigma \end{pmatrix} 
      \begin{pmatrix}
      \frac{\partial r}{\partial a\partial a} 
      & \frac{\partial r}{\partial a\partial \mu} 
      & \frac{\partial r}{\partial a\partial \sigma}\\
      \frac{\partial r}{\partial \mu\partial a}
      &\frac{\partial r}{\partial \mu\partial \mu}
      &\frac{\partial r}{\partial \mu\partial \sigma}\\
      \frac{\partial r}{\partial \sigma\partial a}
      &\frac{\partial r}{\partial \sigma\partial \mu}
      &\frac{\partial r}{\partial \sigma\partial\sigma}\\
      \end{pmatrix}
      \begin{pmatrix} v_a\\ v_\mu\\ v_\sigma \end{pmatrix} 
      \quad.
      @f]

      @b Timings

      The test was performed on a 

          Intel(R) Core(TM) i7-4550U CPU @ 1.50GHz

      with 1,000 runs of each fit kind

      | Jacobian | Hessian  | Average time (ms) |
      |----------|----------|-------------------|
      | numeric  | numeric  | 0.228  +/- 0.001  |
      | analytic | numeric  | 0.1841 +/- 0.0007 |
      | analytic | analytic | 0.237  +/- 0.001  |

   */
  /** 
      Name space for this example 

      @ingroup gslfit_example_normal 
  */
  namespace normal
  {
      
    /** 
	Array of values.  We use std::valarray as this containter (as
	opposed to f.ex. std::vector) allows for optimised computations
	in parallel.

	@ingroup gslfit_example_normal
    */
    using values=std::valarray<double>;

    /** 
	Normal distribution functions 

	@ingroup gslfit_example_normal
    */
    struct fit
    {
      /** The constant @f$\sqrt{2\pi}@f$ */
      constexpr static double sqrt_2pi = std::sqrt(2*M_PI);

      /**
	 Calculates
     
	 @f[ f = a\frac{1}{\sqrt{2\pi}\sigma}
	 e^{-\frac12\left(\frac{x-\mu}{\sigma}\right)^2}\quad. @f]
      */ 
      static values f(const values& x, const values& p)
      {
	double a     = p[0];
	double mu    = p[1];
	double sigma = p[2];
	auto   z     = (x - mu) / sigma;
	auto   s     = 1. / (sqrt_2pi * sigma) * std::exp(-std::pow(z,2)/2);
	return a * s;
      }

      /**
	 Calculates
       
	 @f[
         \nabla f=\begin{bmatrix}
	 \frac{\partial f}{\partial a}\\
	 \frac{\partial f}{\partial\mu}\\
	 \frac{\partial f}{\partial\sigma}\\
	 \end{bmatrix} = 
	 \begin{bmatrix}
	 f\frac{1}{a}\\
	 f\frac{z}{\sigma}\\
	 f\frac{z^2-1}{\sigma}
	 \end{bmatrix}
	 @f]
      */
      static values
      df(const values& x, const values& p)
      {
	using i=std::slice;
	size_t n     = x.size();
	size_t q     = p.size();
	double a     = p[0];
	double mu    = p[1];
	double sigma = p[2];
	auto   z     = (x - mu) / sigma;
	auto   s     = 1. / (sqrt_2pi * sigma) * std::exp(-std::pow(z,2)/2);

	values g(n*q);
	g[i(0,n,q)] = s; 
	g[i(1,n,q)] = s * a / sigma * z;
	g[i(2,n,q)] = s * a / sigma * (std::pow(z,2)-1);

	return g;
      }

      /** 
	  Return the second partial derivatives of the function 

	  @f[ 
	  \nabla\nabla f = 
	  \begin{pmatrix} 
	  \frac{\partial^2f}{\partial a\partial a}  
	  & \frac{\partial^2f}{\partial a     \partial \mu}  
	  & \frac{\partial^2f}{\partial a     \partial \sigma} \\
	  & \frac{\partial^2f}{\partial \mu   \partial \mu}  
	  & \frac{\partial^2f}{\partial \mu   \partial \sigma} \\
	  &
	  & \frac{\partial^2f}{\partial \sigma\partial \sigma}  
	  \end{pmatrix} = 
	  \begin{pmatrix} 
	  0 
	  & f\frac{z}{a\sigma}
	  & f\frac{z^2-1}{a\sigma}\\
	  & f\frac{z^2-1}{\sigma^2}
	  & f\frac{z(z^2-3)}{\sigma^2}\\
	  &
	  & f\frac{2+z^2(z^2-5)}{\sigma^2}
	  \end{pmatrix}\quad.
	  @f]

	  Note matrix is symmetric, and thus we only show the upper triangle. 
      */
      static values
      d2f(const values& x, const values& p)
      {
	using  i      = std::slice;
	size_t n      = x.size();
	size_t q      = p.size();
	size_t q2     = q * q;
	double a      = p[0];
	double mu     = p[1];
	double sigma  = p[2];
	double sigma2 = sigma * sigma;
	auto   z      = (x - mu) / sigma;
	auto   z2     = std::pow(z, 2);
	auto   s      = 1. / (sqrt_2pi * sigma) * std::exp(-std::pow(z,2)/2);

	values g(n * q2); // Tensor!
	g[i(0,n,q2)] = 0;                                    // d2f/da2
	g[i(1,n,q2)] =     s / sigma * z;                    // d2f/(da dmu)
	g[i(2,n,q2)] =     s / sigma * (z2 - 1);             // d2f/(da dsigma)
	g[i(3,n,q2)] = g[i(1,n,q2)];                         // d2f/(dmu da)
	g[i(4,n,q2)] = a * s / sigma2 * (z2 - 1);            // d2f/dmu2
	g[i(5,n,q2)] = a * s / sigma2 * (z2 - 3) * z;        // d2f/(dmu dsigma)
	g[i(6,n,q2)] = g[i(2,n,q2)];                         // d2f/(dsigma da)
	g[i(7,n,q2)] = g[i(5,n,q2)];                         // d2f/(dsigma dmu)
	g[i(8,n,q2)] = a * s / sigma2 * (2 + z2 * (z2 - 5)); // d2f/dsigma2

	return g;
      };
    };

    /**
       Data structure

       Thist contains 

       * @f$ x @f$ - the independent variable 
       * @f$ y @f$ - the dependent variable 
       * @f$ e @f$ - the uncertainty on @f$ y @f$ 

       @ingroup gslfit_example_normal
    */
    struct data
    {
      /** 
	  Constructor.   Sets 

	  * @f$x\in[\mu-3\sigma,\mu+3\sigma]@f$ equidistantly spaced 
	  * @f$y = f(x;a,\mu,\sigma) @f$ 
	  * @f$e = \frac{y/10}@f$ 
	  * Adds noise @f$n\sim\mathcal{N}\left[0,\frac{y}{10}\right]@f$ 
	  to @f$y@f$ distribution
	
	
	  @param n     Number of points 
	  @param seed  Random number seed
	  @param a     @f$a@f$ - scale  
	  @param mu    @f$\mu@f$ - mean of normal distribution 
	  @param sigma @f$\sigma@f$ - Standard deviation 
      */
      data(size_t   n     = 31,
	   unsigned seed  = 123456,
	   double   a     = 100,
	   double   mu    = 0,
	   double   sigma = 1)
	: x(n), y(), e()
      {
	std::random_device         dev;
	std::default_random_engine eng(seed == 0 ? dev() : seed);
	std::normal_distribution<> nrm(0,1);

	values i(n);
	std::iota(std::begin(i), std::end(i), 0);
	i -= n / 2;

	values d(n);
	std::generate(std::begin(d),std::end(d),
		      [&eng,&nrm](){ return nrm(eng); });

	x =  mu + i * 6 * sigma / (n-1);
	y =  fit::f(x,{a,mu,sigma});
	e =  y / 10;
	y += e * d;
      }
      size_t size() const { return x.size(); }
      /** Independent variable */
      values x;
      /** Dependent varialbe */
      values y;
      /** Uncertainty on @f$y@f$ */
      values e;
    };
  }
}

/** 
    Show usage 
    
    @ingroup gslfit_example_normal
*/
void usage(std::ostream& o, const std::string& progname)
{
  o << "Usage: " << progname << " [OPTIONS]\n\n"
    << "Options:\n\n"
    << " -m METHOD   Choice of Trust region step method\n"
    << " -f TYPE     Choice of fitting functions\n"
    << " -v LEVEL    Verbosity level\n"
    << " -h          This help\n\n"
    << "TYPE is one of\n\n"
    << "- 0 Numerical Jacobian and Hessian\n"
    << "- 1 Analytic Jacobian, and numerical Hessian\n"
    << "- 2 Analytic Jacobian and Hessian\n\n"
    << "METHOD is one of\n\n"
    << "- 0 Levenberg-Marquardt\n"
    << "- 1 Levenberg-Marquardt w/acceleration\n"
    << "- 2 Dogleg\n"
    << "- 3 Double dogleg\n"
    << "- 4 2D subspace\n"
    << "- 5 Steihaug-Toint\n"
    << std::endl;
}

/** 
    Program  

    @ingroup gslfit_example_normal
*/
int main(int argc, char** argv)
{
  size_t n = 31;
  for (int i = 1; i < argc; i++) 
    if (argv[i][0] == '-' and argv[i][1] == 'N') n = std::stoi(argv[++i]);
    
  example::normal::values p0{1,2,3};
  example::normal::data   data(n);

  auto exa = example::make_example("normal", p0, data,
				   example::normal::fit::f,
				   example::normal::fit::df,
				   example::normal::fit::d2f);

  if (!exa.parse_args(argc, argv)) return 1;
  
  exa.run("p[0]/(sqrt(2*pi)*p[2])*exp(-((x-p[1])/p[2])**2/2)");

  return 0;
}

// Local Variables:
//   compile-command: "g++ -O2 -I. `gsl-config --cflags` normal.cc `gsl-config --libs`  -o normal"
// End:

    
    
  
    
    
    
