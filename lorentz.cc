/**
   @file      lorentz.cc
   @author    Christian Holm Christensen <cholmcc@gmail.com>
   @date      18 Aug 2021
   @copyright (c) 2021 Christian Holm Christensen. 
   @license   LGPL-3
   @brief     Fit to a resonance peak
*/
#include <random>
#include <valarray>
#include <gslfit.hh>
#include <utils.hh>
#include <example.hh>

namespace example
{
  /** 
      @defgroup gslfit_example_lorentz Lorentz fit to resonance 

      @ingroup gslfit_examples

      We fit the function 

      @f[f(x;a,b,c,d,\Gamma,\mu) = b(x;a,b,c)+d\,s(x;\Gamma,\mu) @f] 
      
      to a resonance spectrum.  Here 

      @f[b(x;a,b,c) = a + bx + cx^2\quad,@f]

      models the background, and 

      @f[s(x;\Gamma,\mu) = 
      \frac{\Gamma / (\pi / 2)}{(x-\mu)^2+(\Gamma/2)^2}\quad,@f] 

      models the signal (resonance) peak. 


      The initial parameters are 
      
      @f{align*}{
        a              &= 1\\
	b              &= 1\\
	c              &= 1\\
	d              &= 1\\
	\Gamma         &= .1\\
	\mu            &= 1\quad.
      @f} 

      @image html lorentz.png 

      The class example::lorentz::fit encodes the fitting function @f$f@f$.

      The class example::lorentz::data contains our data.  
      
      @b Timings

      The test was performed on a 

          Intel(R) Core(TM) i7-4550U CPU @ 1.50GHz

      with 1,000 runs of each fit kind

      | Jacobian | Hessian  | Average time (ms) |
      |----------|----------|-------------------|
      | numeric  | numeric  |   1.18 +/- 0.01   |
  */
  /** 
      Namespace for this example 
    
      @ingroup gslfit_example_lorentz
  */
  namespace lorentz
  {
    /** 
	Array of values.  We use std::valarray as this containter (as
	opposed to f.ex. std::vector) allows for optimised computations
	in parallel.

	@ingroup gslfit_example_lorentz
    */
    using values = std::valarray<double>;

    /** 
	The function to fit to data 

	@ingroup gslfit_example_lorentz
    */
    struct fit
    {
      constexpr static double pi_half = M_PI / 2;
      
      static values f(const values& x, const values& p) {
	double a = p[0];
	double b = p[1];
	double c = p[2];
	double d = p[3];
	double gamma = p[4];
	double mean  = p[5];

	return a + b * x + c * std::pow(x, 2) + \
	  d * (gamma / pi_half) / (std::pow(x - mean,2)+std::pow(gamma/2,2));
      }
   };
      
    /** 
	Our data structure 

	@ingroup gslfit_example_lorentz
    */
    struct data
    {
      /**
	 The resonance peak over background data.
       
      */
      data()
	: x(60),
	  y(60),
	  e(60)
      {
	x = {
	  0.0000, 0.0500, 0.1000, 0.1500, 0.2000, 0.2500,
	  0.3000, 0.3500, 0.4000, 0.4500, 0.5000, 0.5500,
	  0.6000, 0.6500, 0.7000, 0.7500, 0.8000, 0.8500,
	  0.9000, 0.9500, 1.0000, 1.0500, 1.1000, 1.1500,
	  1.2000, 1.2500, 1.3000, 1.3500, 1.4000, 1.4500,
	  1.5000, 1.5500, 1.6000, 1.6500, 1.7000, 1.7500,
	  1.8000, 1.8500, 1.9000, 1.9500, 2.0000, 2.0500,
	  2.1000, 2.1500, 2.2000, 2.2500, 2.3000, 2.3500,
	  2.4000, 2.4500, 2.5000, 2.5500, 2.6000, 2.6500,
	  2.7000, 2.7500, 2.8000, 2.8500, 2.9000, 2.9500 };

	y = {
	  7     , 2     , 6     , 12    , 15    , 18    ,
	  31    , 29    , 27    , 27    , 41    , 35    ,
	  37    , 37    , 63    , 71    , 102   , 95    ,
	  115   , 202   , 190   , 113   , 86    , 68    ,
	  74    , 79    , 75    , 79    , 68    , 62    ,
	  69    , 81    , 79    , 85    , 87    , 68    ,
	  70    , 89    , 77    , 70    , 71    , 62    ,
	  85    , 62    , 73    , 70    , 59    , 61    ,
	  77    , 61    , 62    , 73    , 67    , 71    ,
	  75    , 66    , 73    , 71    , 71    , 49    };

	e = std::sqrt(y);
      }
      size_t size() const { return x.size(); }
      values x;
      values y;
      values e;
    };
  }
}

// -------------------------------------------------------------------
/** 
    Program  

    @ingroup gslfit_example_lorentz
*/
int main(int argc, char** argv)
{
  example::lorentz::values p0{1,1,1,1,1,0.1};
  example::lorentz::data   data;

  auto exa = example::make_example("lorentz", p0, data,
				   example::lorentz::fit::f, nullptr, nullptr);

  if (!exa.parse_args(argc, argv)) return 1;
  
  exa.run("p[0]+p[1]*x+p[2]*x**2+"
	  "p[3]*(p[4]/(pi/2))/((x-p[5])**2+(p[4]/2)**2)");

  return 0;
}
  
// Local Variables:
//   compile-command: "g++ -O2 -I. `gsl-config --cflags` lorentz.cc `gsl-config --libs`  -o lorentz"
// End:
