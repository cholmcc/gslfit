/**
   @file      mapdata.cc
   @author    Christian Holm Christensen <cholmcc@gmail.com>
   @date      18 Aug 2021
   @copyright (c) 2021 Christian Holm Christensen. 
   @license   LGPL-3
   @brief     Fit to a resonance peak
*/
#include <random>
#include <valarray>
#include <gslfit.hh>
#include <utils.hh>
#include <example.hh>
#if __cplusplus >= 201703L
# include <execution>
# define  POLICY std::execution::par_unseq,
#else
# define POLICY
#endif 

namespace example
{
  /** 
      @defgroup gslfit_example_mapdata Lorentz fit to resonance 

      @ingroup gslfit_examples

      This is the same fit as @ref gslfit_example_lorentz, but here
      our data structure is a simple std::map from double (@f$x@f$) to
      a std::pair (@f$(y,e)@f$).  Also, we specify the residual and
      weight functions (instead of using example::example).
      
      Note, our residual and weight functions example::mapdata:r and
      example::mapdata:w, respectively, uses std::transform to map our
      input data to residuals and weights, respectively.  This allows
      us to use C++17 [execution
      policies](https://en.cppreference.com/w/cpp/header/execution)
      such as `std::execution::par_unseq` for parallel, un-sequenced,
      evaluation of the fitted function.

  */
  /** 
      Namespace for this example 
    
      @ingroup gslfit_example_mapdata
  */
  namespace mapdata
  {
    /** 
	Array of values.  We use std::valarray as this containter (as
	opposed to f.ex. std::vector) allows for optimised computations
	in parallel.

	@ingroup gslfit_example_mapdata
    */
    using values = std::valarray<double>;

    /** 
	The function to fit to data 

	@ingroup gslfit_example_mapdata
    */
    double f(const double& x, const values& p) {
      constexpr double pi_half = M_PI / 2;
      
      double a     = p[0];
      double b     = p[1];
      double c     = p[2];
      double d     = p[3];
      double gamma = p[4];
      double mean  = p[5];
      
      return a + b * x + c * std::pow(x, 2) +				\
	d * (gamma / pi_half) / (std::pow(x - mean,2)+std::pow(gamma/2,2));
    }
    /** 
	Our data structure 

	@ingroup gslfit_example_mapdata
    */
    using data=std::map<double,std::pair<double,double>>;
    
    /** 
	Our residual function 

	@ingroup gslfit_example_mapdata
    */
    values r(const data& data, const values& p) {
      values ret(data.size());

      std::transform(POLICY
		     data.begin(), data.end(), std::begin(ret),
		     [&p](const std::pair<double,std::pair<double,double>>& in)
		     {
      		       return in.second.first - f(in.first,p); });

      return ret;
    }
    
    /** 
	Our weights function 

	@ingroup gslfit_example_mapdata
    */
    values w(const data& data) {
      values ret(data.size());

      std::transform(POLICY
		     data.begin(), data.end(), std::begin(ret),
		     [](const std::pair<double,std::pair<double,double>>& in) {
		       return in.second.second > 1e-8 ?
			 1. / std::sqrt(in.second.second) : 0; });
      return ret;
    }
  }
}

// -------------------------------------------------------------------
/** 
    Program  

    @ingroup gslfit_example_mapdata
*/
int main(int argc, char** argv)
{
  example::mapdata::values p0{1,1,1,1,1,0.1};
  example::mapdata::data   data;
  unsigned                 verb = 1;
  
  data[0.0000] =     {   7,   2.65}; 
  data[0.0500] =     {   2,   1.41};   
  data[0.1000] =     {   6,   2.45};   
  data[0.1500] =     {  12,   3.46};  
  data[0.2000] =     {  15,   3.87};   
  data[0.2500] =     {  18,   4.24};   
  data[0.3000] =     {  31,   5.57};  
  data[0.3500] =     {  29,   5.39}; 
  data[0.4000] =     {  27,   5.20}; 
  data[0.4500] =     {  27,   5.20}; 
  data[0.5000] =     {  41,   6.40}; 
  data[0.5500] =     {  35,   5.92};  
  data[0.6000] =     {  37,   6.08}; 
  data[0.6500] =     {  37,   6.08}; 
  data[0.7000] =     {  63,   7.94}; 
  data[0.7500] =     {  71,   8.43}; 
  data[0.8000] =     { 102,  10.10}; 
  data[0.8500] =     {  95,   9.75};  
  data[0.9000] =     { 115,  10.72};  
  data[0.9500] =     { 202,  14.21};
  data[1.0000] =     { 190,  13.78};
  data[1.0500] =     { 113,  10.63}; 
  data[1.1000] =     {  86,   9.27}; 
  data[1.1500] =     {  68,   8.25}; 
  data[1.2000] =     {  74,   8.60};
  data[1.2500] =     {  79,   8.89};
  data[1.3000] =     {  75,   8.66};
  data[1.3500] =     {  79,   8.89}; 
  data[1.4000] =     {  68,   8.25};
  data[1.4500] =     {  62,   7.87};
  data[1.5000] =     {  69,   8.31};
  data[1.5500] =     {  81,   9.00};
  data[1.6000] =     {  79,   8.89}; 
  data[1.6500] =     {  85,   9.22}; 
  data[1.7000] =     {  87,   9.33}; 
  data[1.7500] =     {  68,   8.25};
  data[1.8000] =     {  70,   8.37}; 
  data[1.8500] =     {  89,   9.43}; 
  data[1.9000] =     {  77,   8.77}; 
  data[1.9500] =     {  70,   8.37};  
  data[2.0000] =     {  71,   8.43}; 
  data[2.0500] =     {  62,   7.87};
  data[2.1000] =     {  85,   9.22}; 
  data[2.1500] =     {  62,   7.87}; 
  data[2.2000] =     {  73,   8.54}; 
  data[2.2500] =     {  70,   8.37}; 
  data[2.3000] =     {  59,   7.68}; 
  data[2.3500] =     {  61,   7.81}; 
  data[2.4000] =     {  77,   8.77}; 
  data[2.4500] =     {  61,   7.81}; 
  data[2.5000] =     {  62,   7.87}; 
  data[2.5500] =     {  73,   8.54}; 
  data[2.6000] =     {  67,   8.19}; 
  data[2.6500] =     {  71,   8.43}; 
  data[2.7000] =     {  75,   8.66}; 
  data[2.7500] =     {  66,   8.12}; 
  data[2.8000] =     {  73,   8.54}; 
  data[2.8500] =     {  71,   8.43}; 
  data[2.9000] =     {  71,   8.43}; 
  data[2.9500] =     {  49,   7.00}; 

  auto fit = gsl::fit(gsl::fit_config(),
		      p0,data,data.size(),
		      example::mapdata::r,
		      example::mapdata::w,verb);

  std::cout << fit << std::endl;

  return 0;
}
  
// Local Variables:
//   compile-command: "g++ -O2 -I. `gsl-config --cflags` mapdata.cc `gsl-config --libs`  -o mapdata"
// End:
