/**
   @file      YodaGslFit.hh
   @author    Christian Holm Christensen <cholmcc@gmail.com>
   @date      18 Aug 2021
   @copyright (c) 2021 Christian Holm Christensen. 
   @license   LGPL-3
   @brief     Fit functions to YODA objects using GSL
*/
#ifndef YODA_GSL_FIT_HH
#define YODA_GSL_FIT_HH
#include <YODA/Histo1D.h>
#include <YODA/Profile1D.h>
#include <YODA/Scatter2D.h>
#include <YODA/Histo2D.h>
#include <YODA/Profile2D.h>
#include <YODA/Scatter3D.h>
#include <gslfit.hh>

namespace YODA
{
  namespace GSL
  {
    /** 
	@defgroup yoda_gslfit Fitting YODA objects using GSL 

	Code in this module allows us to fit YODA objects (histograms,
	profiles, and scatters) using the GNU Scientific Library. 

	The main function is the YODA::GSL::fit function.  

	The user must supply a function @f$ f@f$ with signature 

	    double Function(double x, const Parameters& p)
	
        for the one-dimensional case (Histo1D,Profile1D,Scatter2D),
        and
	
	    double Function(std::pair<double,double> x, const Parameters& p) 
	
        for the two-dimensional case (Histo2D,Profile2D,Scatter3D).
        This function must return the value of the function being
        fitted to the data at the point `x` with parameters `p`

	@f[f(x;p) = \ldots\quad.@f]

	Note, that for fitting the function @f$ f@f$ is considered a
	function of the _parameters_ (i.e., @f$ x@f$ is considered
	constants) 

	@f[ f:p\in\mathbb{P}^n \mapsto f(x;p)\in\mathbb{R}\quad. @f]

	In addition, the user may supply a function with signature 

	    double Jacobian(double x, const Parameters& p)

        (for the one-dimensional case, and similarly for the two
        dimensional case). which calculates the vector 

	@f[
	J = 
	\begin{pmatrix}\frac{\partial f}{\partial p_1}\\
	\vdots\\
	\frac{\partial f}{\partial p_n}
	\end{pmatrix}\quad,
	@f] 
        
	evaluted at the point `x`.  That is, the function must
	calculate the gradient with respect to the parameters at point
	`x`.  Supplying this function will cause GSL to use this to
	determining the next step size.  This can improve the fit in
	some cases.  Note, if this function is not supplied, then the
	gradient will be evaluated numerically.

	Again, note that for fitting the function @f$ f@f$ to data, we
	consider @f$ f@f$ to be a function of the _parameters_ with
	@f$ x@f$ being constants.  Thus the Jacobian vector @f$ J@f$,
	in case @f$ f(x,p)\in\mathbb{R}@f$ is really the gradient of
	@f$ @f$ over the domain @f$ P^n@f$ 

	@f[J: p\in P^n\mapsto\nabla f\in\mathbb{R}^n\quad.@f] 

	@note You may have heard the term _Jacobian matrix_ and be
	confused why we call it a _Jacobian vector_ here.  In general,
	if the function @f$ g@f$ takes on values in a
	multi-dimensional set
	
	@note 
	@f[g:x\in X^n\mapsto y\in Y^m\quad,@f] 
	then the Jacobian is the matrix
	
	@note
	@f[ 
	J_g = \begin{pmatrix} 
	\frac{\partial f_1}{\partial x_1} 
	& \cdots 
	& \frac{\partial f_1}{\partial x_n}\\
	\vdots 
	& \ddots 
	& \vdots \\
	\frac{\partial f_m}{\partial x_1} 
	& \cdots 
	& \frac{\partial f_m}{\partial x_n}\\
	\end{pmatrix}\quad.
	@f]
	
	@note
	However, if @f$ Y=\mathbb{R}@f$, then this becomes a
	vector.

	Furthermore, the user may also supply a function with
	signature

	    double 
	    Hessian(double x, const Parameters& p)

	which calculates the second order derivative matrix 

	@f[
	H = 
	  \begin{pmatrix}
	    \frac{\partial^2 f}{\partial p_1\partial_1}
	  & \cdots 
	  & \frac{\partial^2 f}{\partial p_1\partial_n}\\
	  \vdots & & \vdots\\
	    \frac{\partial^2 f}{\partial p_1\partial p_n}
	  & \cdots 
	  & \frac{\partial^2 f}{\partial p_n\partial_n}\\
	  \end{pmatrix}\quad.
	@f] 

	If the user supplies this function, then GSL will use that to
	better estimate the next step.  If not supplied, then GSL will
	calculate it numerically if needed by the chosen method (see
	gsl::fit_config). 

	Since @f$ f@f$ is considered a function of the _parameters_,
	this means that @f$ H@f$ is the mapping

	@f[H:p\in P^n\mapsto 
	\nabla\nabla f(x;p)\in\mathbb{R}^n\times\mathbb{R}^n\quad. @f]

    */
    //================================================================
    /** 
	Import fit_config  into this namespace
	
	@ingroup yoda_gslfit
    */
    using FitConfig =::gsl::fit_config ;

    /**
       Import fit_result  into this namespace

	@ingroup yoda_gslfit
    */
    template <typename Parameters>
    using FitResult =::gsl::fit_result <Parameters>;

    //================================================================
    /** 
	Traits of object to fit to - unimplemented base template 
	
	@ingroup yoda_gslfit
     */
    template <typename T> struct FitTrait;
    
    //----------------------------------------------------------------
    /** 
	Fit data trait - Histo1D specialisation
	
	@ingroup yoda_gslfit
    */
    template <>
    struct FitTrait<Histo1D>
    {
      using DataType=Histo1D;
      using PointType=DataType::Bin;
      using PointsType=DataType::Bins;
      using XType=double;
      using YType=double;

      /** Get number of points to evaluate at */
      static size_t n(const DataType& data) { return data.numBins(); }

      /** Get indepedent value at point @a i */
      static const XType x(const DataType& data, size_t i) {
	return data.bin(i).xFocus(); }

      /** Get indepedent value at point @a i */
      static const YType y(const DataType& data, size_t i) {
	return data.bin(i).height(); }

      /** Get uncertainty on indepedent value at point @a i */
      static const YType e(const DataType& data, size_t i) { 
	return data.bin(i).heightErr(); }
    };
    
    //----------------------------------------------------------------
    /** 
	Fit data trait - Profile1D specialisation
	
	@ingroup yoda_gslfit
    */
    template <>
    struct FitTrait<Profile1D>
    {
      using DataType=Profile1D;
      using PointType=DataType::Bin;
      using PointsType=DataType::Bins;
      using XType=double;
      using YType=double;

      /** Get number of points to evaluate at */
      static size_t n(const DataType& data) { return data.numBins(); }

      /** Get indepedent value at point @a i */
      static const XType x(const DataType& data, size_t i) {
	return data.bin(i).xFocus(); }

      /** Get indepedent value at point @a i */
      static const YType y(const DataType& data, size_t i) {
	return data.bin(i).mean(); }

      /** Get uncertainty on indepedent value at point @a i */
      static const YType e(const DataType& data, size_t i) { 
	return data.bin(i).stdErr(); }
    };
    
    //----------------------------------------------------------------
    /** 
	Fit data trait - Scatter2D specialisation
	
	@ingroup yoda_gslfit
    */
    template <>
    struct FitTrait<Scatter2D>
    {
      using DataType=Scatter2D;
      using PointType=Scatter2D::Point;
      using PointsType=DataType::Points;
      using XType=double;
      using YType=double;

      /** Get all data points */
      static const PointsType& xye(const DataType& data) {
	return data.points(); }

      /** Get number of points to evaluate at */
      static size_t n(const DataType& data) { return data.numPoints(); }

      /** Get indepedent value at point @a i */
      static const XType x(const DataType& data, size_t i) {
	return data.point(i).x(); }

      /** Get indepedent value at point @a i */
      static const YType y(const DataType& data, size_t i) {
	return data.point(i).y(); }

      /** Get uncertainty on indepedent value at point @a i */
      static const YType e(const DataType& data, size_t i) { 
	return data.point(i).yErrAvg(); }
    };
    
    //----------------------------------------------------------------
    /** 
	Fit data trait - Histo2D specialisation
	
	@ingroup yoda_gslfit
    */
    template <>
    struct FitTrait<Histo2D>
    {
      using DataType=Histo2D;
      using PointType=DataType::Bin;
      using PointsType=DataType::Bins;
      using XType=std::pair<double,double>;
      using YType=double;

      /** Get number of points to evaluate at */
      static size_t n(const DataType& data) { return data.numBins(); }

      /** Get indepedent value at point @a i */
      static const XType x(const DataType& data, size_t i) {
	return std::make_pair(data.bin(i).xFocus(),data.bin(i).yFocus()); }

      /** Get indepedent value at point @a i */
      static const YType y(const DataType& data, size_t i) {
	return data.bin(i).height(); }

      /** Get uncertainty on indepedent value at point @a i */
      static const YType e(const DataType& data, size_t i) { 
	return data.bin(i).heightErr(); }
    };
    
    //----------------------------------------------------------------
    /** 
	Fit data trait - Profile2D specialisation
	
	@ingroup yoda_gslfit
    */
    template <>
    struct FitTrait<Profile2D>
    {
      using DataType=Profile2D;
      using PointType=DataType::Bin;
      using PointsType=DataType::Bins;
      using XType=std::pair<double,double>;
      using YType=double;

      /** Get number of points to evaluate at */
      static size_t n(const DataType& data) { return data.numBins(); }


      /** Get indepedent value at point @a i */
      static const XType x(const DataType& data, size_t i) {
	return std::make_pair(data.bin(i).xFocus(),data.bin(i).yFocus()); }

      /** Get indepedent value at point @a i */
      static const YType y(const DataType& data, size_t i) {
	return data.bin(i).mean(); }

      /** Get uncertainty on indepedent value at point @a i */
      static const YType e(const DataType& data, size_t i) { 
	return data.bin(i).stdErr(); }
    };
    
    //----------------------------------------------------------------
    /** 
	Fit data trait - Scatter3D specialisation
	
	@ingroup yoda_gslfit
    */
    template <>
    struct FitTrait<Scatter3D>
    {
      using DataType=Scatter3D;
      using PointType=DataType::Point;
      using PointsType=DataType::Points;
      using XType=std::pair<double,double>;
      using YType=double;

      /** Get number of points to evaluate at */
      static size_t n(const DataType& data) { return data.numPoints(); }

      /** Get all data points */
      static const PointsType& xye(const DataType& data) {
	return data.points(); }

      /** Get indepedent value at point @a i */
      static const XType x(const DataType& data, size_t i) {
	return std::make_pair(data.point(i).x(),data.point(i).y()); }

      /** Get indepedent value at point @a i */
      static const YType y(const DataType& data, size_t i) {
	return data.point(i).z(); }

      /** Get uncertainty on indepedent value at point @a i */
      static const YType e(const DataType& data, size_t i) { 
	return data.point(i).zErrAvg(); }
    };
    //================================================================
    /**
       Wrapper around passed functions and data.  Member functions
       used the FitTrait template to get access to different parts of
       the data, and the trampoline approriate arguments to the passed
       functions
       
       @tparam Parameters _Iterable_ of parameter and function values
       @tparam Data       Data container
       @tparam Function   _Callable_ to fit to the data
       @tparam Jacobian   _Callable_ to calculate Jacobian at each point
       @tparam Hessian    _Callable_ to calculate Hessian at each point
	
       @ingroup yoda_gslfit
    */
    template <typename Parameters,
	      typename Data,
	      typename Function,
	      typename Jacobian,
	      typename Hessian>
    struct Wrappers
    {
      /// Our traits 
      using TraitType=FitTrait<Data>;

      /** 
	  @{ 
	  @name Information about the data 
      */
      /**
         Get the size of data
         
         @param data      The data to fit to 
         
         @returns Number of data bins       
      */
      static size_t size(const Data& data) {
	return TraitType::n(data);
      }

      /**
         Calculate the weigths from the data 
         
         @param data      The data to fit to 
         
         @returns one over the square uncertainty at every bin of
         the data
      */
      static Parameters weights(const Data& data) {
	size_t     n = TraitType::n(data);
	Parameters ret(n);
	auto       cur    = std::begin(ret);

	for (size_t i = 0; i < n; i++, ++cur) {
	  auto e = TraitType::e(data,i);
	  *cur   = e < 1e-9 ? 0 : 1. / std::pow(e,2);
	}
	return ret;
      }
      /** @} */
      
      /** 
	  @{ 
	  @name Residuals 
      */
      /**
         Calculate the residuals from the data given the current
         parameter values
         
         @param data       The data to fit to 
         @param parameters Current parameter values
         @param function   _Callable_ to fit to the data
         
         @returns Residuals evalaute at every bin of the data 
      */
      static Parameters residuals(const Data&        data,
				  const Parameters&  parameters,
				  Function           function) {
	size_t     n = TraitType::n(data);
	Parameters ret(n);
	auto       cur = std::begin(ret);

	for (size_t i = 0; i < n; i++, ++cur)
	  *cur = TraitType::y(data,i) - function(TraitType::x(data,i),
						 parameters);
	
	return ret;
      }

      /** 
	  Get the above residuals function with function argument
	  bound to parameter
      */
      static auto residuals_func(Function function) ->
	decltype(std::bind(residuals,
			   std::placeholders::_1,
			   std::placeholders::_2,
			   function))
			   
      {
	using namespace std::placeholders;
	return std::bind(residuals,_1,_2,function);
      }
      /** @} */
      
      /** 
	  @{ 
	  @name Jacobian 
      */
      /**
         Calculate the Jacobian from the data and current parameter values
         
         @param data       The data to fit to 
         @param parameters Current parameter values
         @param jacobian   _Callable_ to calculate Jacobian
         
         @returns Gradient at each bin
      */
      static Parameters jacobian(const Data&       data,
				 const Parameters& parameters,
				 Jacobian          jacobian)
      {
	size_t     n    = TraitType::n(data);
	size_t     npar = std::distance(std::begin(parameters),
					std::end(parameters));
	Parameters ret(npar * n);
	auto       cur  = std::begin(ret);
	
	for (size_t i = 0; i < n; i++) {
	  Parameters grad = jacobian(TraitType::x(data,i), parameters);

	  for (auto ig = std::begin(grad); ig != std::end(grad); ++ig, ++cur)
	    *cur = - *ig; // Negative since diff or residuals
	}

	return ret;
      }

      /** 
	  Get the jacobian function to use - in case it wasn't given. 

	  That is, if the user passed null for the Jacobian function,
	  this is called which returns a null pointer.
      */
      template <typename J,
		typename std::enable_if<std::is_same<J,void*>::value ||
					std::is_same<J,nullptr_t>::value,
					bool>::type = true>
      static void* jacobian_func(J) {
	return nullptr; }

      /** 
	  Get the jacobian function to use - in case it was given. 

	  That is, if the user passed a Jacobian function, then this
	  is called and we return a binding of the above wrapper with
	  that function.
      */
      template <typename J,
		typename std::enable_if<!std::is_same<J,void*>::value &&
					!std::is_same<J,nullptr_t>::value,
					bool>::type = true>
      static auto jacobian_func(J j) ->
	decltype(std::bind(jacobian,
			   std::placeholders::_1,
			   std::placeholders::_2,
			   j))
      {
	using namespace std::placeholders;
	return std::bind(jacobian,_1,_2,j);
      }
      /** @} */

      /** 
	  @{ 
	  @name Hessian 
      */
      /**
         Calculate the Hessian from the data and current parameter values
         
         @param data       The data to fit to 
         @param parameters Current parameter values
         @param velocity   Current velocity
         @param hessian    _Callable_ to calculate direction acceleration
         
         @returns Acceleration at each bin
      */
      static Parameters hessian(const Data&       data,
				const Parameters& parameters,
				const Parameters& velocity,
				Hessian           hessian) {
	size_t     n = TraitType::n(data);
	size_t     m = std::distance(std::begin(parameters),
				     std::end(parameters));

	// Calculate outer product of velocity 
	Parameters vv(m*m);
	auto       vc = std::begin(vv);
	for (auto vi = std::begin(velocity); vi != std::end(velocity); ++vi) 
	  for (auto vj = std::begin(velocity); vj != std::end(velocity);
	       ++vj, ++vc)
	    *vc = - *vi * *vj; // Negative since diff or residuals

	Parameters ret(n);
	auto       cur  = std::begin(ret);
	for (size_t i = 0; i < n; i++, ++cur) {
	  auto   d2f = hessian(TraitType::x(data,i), parameters);
	  *cur = std::inner_product(std::begin(vv),std::end(vv),
				    std::begin(d2f), 0);
	}
	return ret;
      }

      /** 
	  Get the hessian function to use - in case it wasn't given. 

	  That is, if the user passed null for the Hessian function,
	  this is called which returns a null pointer.
      */
      template <typename H,
		typename std::enable_if<std::is_same<H,void*>::value ||
					std::is_same<H,nullptr_t>::value,
					bool>::type = true>
      static void* hessian_func(H) {
	return nullptr; }

      /** 
	  Get the hessian function to use - in case it was given. 

	  That is, if the user passed a Hessian function, then this
	  is called and we return a binding of the above wrapper with
	  that function.
      */
      template <typename H,
		typename std::enable_if<!std::is_same<H,void*>::value &&
					!std::is_same<H,nullptr_t>::value,
					bool>::type = true>
      static auto hessian_func(H h) ->
	decltype(std::bind(hessian,
			   std::placeholders::_1,
			   std::placeholders::_2,
			   std::placeholders::_3,
			   h))
      {
	using namespace std::placeholders;
	return std::bind(hessian,_1,_2,_3,h);
      }
      /** @} */
    };
    
    
    /**
       Function to fit an analysis object.
       
       @param config   Fit configuration
       @param p0       Initial paraemter values
       @param data     The data to fit to
       @param function The function to fit to data
       @param jacobian Function to calculate Jacobian
       @param hessian  Function to calculate hessian
       @param verbose  Verbosity level
       
       @return Result of fit 
	
       @ingroup yoda_gslfit
    */
    template <typename Parameters,
	      typename Data,
	      typename Function,
	      typename Jacobian=void*,
	      typename Hessian=void*>
    FitResult<Parameters> fit(const FitConfig&  config,
			      const Parameters& p0,
			      const Data&       data,
			      Function          function,
			      Jacobian          jacobian=nullptr,
			      Hessian           hessian=nullptr,
			      unsigned          verbose=0)
    {
      using Wrapped=Wrappers<Parameters,Data,
			     Function,Jacobian,Hessian>;

      return ::gsl::fit(config,
			p0,
			data,
			Wrapped::size(data),
			Wrapped::residuals_func(function),
			Wrapped::weights,
			Wrapped::jacobian_func(jacobian),
			Wrapped::hessian_func(hessian),
			verbose);
    }


    /** 
	@example yodatest.cc Fitting to YODA objects

	This example shows how to fit a function to various types of
	YODA objects.

	See also @ref gslfit_example_yoda
    */

  }
}
#endif
// Local Variables:
//  compile-command: "g++ -c YodaGslFit.hh `yoda-config --cflags` -I."
// End:

    
	
      
