/**
   @file      simple.cc
   @author    Christian Holm Christensen <cholmcc@gmail.com>
   @date      18 Aug 2021
   @copyright (c) 2021 Christian Holm Christensen. 
   @license   LGPL-3
   @brief     A "simple" example of using C++ GSL fitting interface
*/
#include <iostream>
#include <valarray>
#include <random>
#include <numeric>
#include <gslfit.hh>
#include <utils.hh>
#include <histogram.hh>

namespace example
{
  /** 
      @defgroup gslfit_example_simple A "simple" example 

      @ingroup gslfit_examples

      In this example we same create a sample from a random variable 

      @f[X\sim\mathcal{N}[\mu,\sigma]\quad,@f] 

      of size @f$ N@f$ and histogram that simple into
      @f$N_{\mathrm{bins}}@f$ equidistant in the interval 
      @f$[\mu-3\sigma,\mu+3\sigma]@f$. 

      We then fit the function 

      @f[f(x;a,\mu,\sigma)=a\frac{1}{\sqrt{2\pi}\sigma}
         e^{-\frac12\left(\frac{x-\mu}{\sigma}\right)^2}\quad,@f] 
	 
      to that data to extract the best-fit parameters
      @f$\{a,\mu,\sigma\}@f$.

      The code is pretty straight forward.  The function
      example::simple::make_data generates a sample of @f$ X @f$ and
      histograms it using the class template utils::histogram above.
      The instances of class templates are defined by a template
      parameter.

      The function example::simple::f encodes the fitted function.
      Again, it is templated on the value type.  

      The function example::simple::r calculates the residuals of the
      data with respect to the fitted function, and the function
      example::simple::w calculates the observation weights as

      @f[w = \frac{1}{e^2} = \frac{1}{\sum_i w_i/\Delta}\quad. @f] 
      
      Note, if any bin has @f$\sum_i w_i \leq 0 @f$ then the
      corresponding weight is set to zero.

      The last function example::simple::fit is where we 

      - Generate our fit configuration 
      - Generate our data 
      - Perform our fit of @f$f@f$ to the data 
      - Print out the result. 

      @section gslfit_example_simple_valarray std::valarray 

      Througout we use the class template std::valarray to hold
      data. This is because instances of this class template allow for
      vectorized numerical calculations straight forwardly and
      simplifies out code a lot.  We do not need to loop over data to
      calculate @f$f(x;p)@f$ ,for example.

      The down-side is that we do need to copy some of this data
      around, but we ave minimized that as best we could in the code.
  */
  namespace simple
  {
    //================================================================
    template <typename T>
    using data_type=nd_histogram::frozen_histogram<T,1>;
    
    //================================================================
    /** 
	Function to generate our data 
	
	@ingroup gslfit_example_simple
    */
    template <typename Value>
    data_type<Value>
    make_data(size_t       nsamples,
	      size_t       nbins,
	      const Value& mu,
	      const Value& sigma) {
      using return_type    = data_type<Value>;
      using histogram_type = typename return_type::histogram_type;
      using axis_type      = typename histogram_type::axis_type;
      using value_array    = typename axis_type::x_array;
      using value_type     = typename axis_type::x_type;
      
      axis_type      a(nbins, mu - 3 * sigma, mu + 3 * sigma);
      histogram_type h(a);
      
      std::random_device              rnd;
      std::default_random_engine      eng(rnd());
      std::normal_distribution<Value> norm(mu,sigma);
      
      value_array r(nsamples);
      std::generate(std::begin(r), std::end(r),
		    [&eng,&norm](){ return norm(eng); });

      for (auto rr : r) h.fill(rr);

      return return_type(h);
    }
      
      
    //----------------------------------------------------------------
    /** 
	Function to fit to the data 

	@f[f(x;a,\mu,\sigma)=a\frac{1}{\sqrt{2\pi}\sigma}
	e^{-\frac12\left(\frac{x-\mu}{\sigma}\right)^2}\quad.@f]

	@param x where to evaluate 
	@param p paramters @f$\{a,\mu,\sigma\}@f$ 
	
	@returns @f$ f(x;p)@f$ 
	
	@ingroup gslfit_example_simple
    */
    template <typename Value>
    std::valarray<Value>
    f(const std::valarray<Value>& x, const std::valarray<Value>& p) {
      return p[0] / (std::sqrt(2 * M_PI) * p[2])
	*std::exp(- std::pow((x - p[1]) / p[2], 2) / 2);
    }

    //----------------------------------------------------------------
    /** 
	Function that calculates the residuals 
	
	@f[r=y-f(x;p)\quad.@f]

	@param data The data 
	@param parameters paramters @f$\{a,\mu,\sigma\}@f$ 
	
	@returns @f$r@f$ 
	
	@ingroup gslfit_example_simple
    */
    template <typename Value>
    std::valarray<Value>
    r(const data_type<Value>&     data,
      const std::valarray<Value>& parameters) {
      return data.y - f(data.x, parameters);
    }
    
    //----------------------------------------------------------------
    /** 
	Function that calculates the weights
	
	@f[ w = \begin{cases}\frac{1}{e^2} & e>0\\ 0\end{cases}\quad.@f]

	@param data The data 
	@ingroup gslfit_example_simple
    */
    template <typename Value>
    std::valarray<Value>
    w(const data_type<Value>& data) {
      std::valarray<Value> t = 1. / std::pow(data.e,2);
      t[data.e<1e-9] = 0;
      return t;
    }
    
    //================================================================
    /** 
	Short hand to do our fit 
 
	
	@ingroup gslfit_example_simple
   */
    template <typename Value=double>
    void fit(unsigned verbose=1) {
      gsl::fit_config cfg;

      auto                 data = make_data<Value>(1000, 30, 0, 1);
      std::valarray<Value> p0{1,2,3};

      auto result = gsl::fit(cfg,
			     p0,
			     data,
			     data.x.size(),
			     r<Value>,
			     w<Value>,
			     verbose);

      std::cout << result << std::endl;
    }
  }
}

int main() {
  example::simple::fit();

  return 0;
}
// Local Variables:
//   compile-command: "g++ -O2 -I. `gsl-config --cflags` simple.cc `gsl-config --libs`  -o simple"
// End:

    
    
  
    
    
    

      
