/**
   @file      example.hh
   @author    Christian Holm Christensen <cholmcc@gmail.com>
   @date      18 Aug 2021
   @copyright (c) 2021 Christian Holm Christensen. 
   @license   LGPL-3
   @brief     More utilities
*/
#ifndef GSLFIT_MORE_HH
#define GSLFIT_MORE_HH
#include <gslfit.hh>
#include <fstream>

namespace example {
  /** 
      Base template class for examples 

      @ingroup gslfit_examples 
  */
  template <typename Parameters,
	    typename Data,
	    typename Function,
	    typename Jacobian,
	    typename Hessian>
  struct example {
    /** Type of parameters */
    using parameters_type = Parameters;

    /** Type of data */
    using data_type = Data;

    /** Type of function to fit to data */
    using function_call = Function;

    /** Type of Jacobian of function */
    using jacobian_call = Jacobian;

    /** Type of Hessian of function */
    using hessian_call = Hessian;

    /** Fit result type */
    using fit_result = gsl::fit_result<parameters_type>;

    /** Fit configuration */
    using fit_config = gsl::fit_config;

    example(const std::string&     name,
	    const parameters_type& p0,
	    const data_type&       data,
	    function_call          f,
	    jacobian_call          df,
	    hessian_call           d2f)
      : _name(name),
	_p0(p0),
	_data(data),
	_f(f),
	_df(df),
	_d2f(d2f),
	_method(1),
	_funcs(0),
	_verbose(0),
	_repeat(1),
	_plot(false),
	_result(_p0.size(), _data.size()),
	_cfg()
    {}

    /** 
	@{ 
	@name Command line 
    */
    /** 
	Show summary of command line options 
	
	@param o Output stream 
	@param prog Invocation name 
    */
    void usage(std::ostream&      o,
	       const std::string& prog) {
      o << "Usage: " << prog << " [OPTIONS]\n\n"
	<< "Options:\n\n"
	<< " -m METHOD   Choice of Trust region step method ("<<_method <<")\n"
	<< " -f TYPE     Choice of fitting functions        ("<<_funcs  <<")\n"
	<< " -v LEVEL    Verbosity level                    ("<<_verbose<<")\n"
	<< " -h          This help\n\n"
	<< "TYPE is one of\n\n"
	<< "- 0 Numerical Jacobian and Hessian\n"
	<< "- 1 Analytic Jacobian, and numerical Hessian\n"
	<< "- 1 Analytic Jacobian and Hessian\n\n"
	<< "METHOD is one of\n\n"
	<< "- 0 Levenberg-Marquardt\n"
	<< "- 1 Levenberg-Marquardt w/acceleration\n"
	<< "- 2 Dogleg\n"
	<< "- 3 Double dogleg\n"
	<< "- 4 2D subspace\n"
	<< "- 5 Steihaug-Toint\n"
	<< std::endl;
    }
    /** 
	Parse command line arguments
	
	@param argc  Argument count 
	@param argv  Argument vector 
	
	@return true on success
    */
    virtual bool parse_args(int argc, char** argv) {
      for (int i = 1; i < argc; i++) {
	if (argv[i][0] != '-') {
	  std::cerr << argv[0] << ": Unknown option " << +argv[i] << std::endl;
	  return false;
	}
	
	switch (argv[i][1]) {
	case 'h': usage(std::cout, argv[0]); return 0;
	case 'm': _method  = std::stoi(argv[++i]); break;
	case 'f': _funcs   = std::stoi(argv[++i]); break;
	case 'v': _verbose = std::stoi(argv[++i]); break;
	case 'r': _repeat  = std::stoi(argv[++i]); break; // For timing
	case 'N': ++i;                             break; // Ignored!
	case 'p': _plot    = !_plot;               break;
	default:
	  std::cerr << argv[0] << ": Unknown option " << +argv[i] << std::endl;
	  return false;
	}
      }
      return true;
    }
    /* @} */
    
    /** 
	@{ 
	@name Plot preparation 
    */
    /** 
	Write out an array 
	
	@param name  Name of variable 
	@param v     Values 
	@param o     Output stream 

	@ingroup gslfit_utils
    */
    template <typename Values>
    void make_array(std::ostream&      o,
		    const std::string& name,
		    const Values&      v) {
      o << name << " = asarray([";
      std::copy(std::begin(v), std::end(v),
		std::ostream_iterator<double>(o, ", "));
      o << "])" << std::endl;
    }
    /** 
	Create a plot of the fit 

	@param o      Output stream 
	@param expr   Function expression 

	@ingroup gslfit_utils
    */
    void fit_plot(std::ostream&                      o,
		  const std::string&                 expr) {
      size_t p = _result.parameters.size();

      // Import stuff from numpy 
      o << "from numpy import asarray,vstack,pi,sqrt,exp,sin,cos,tan\n\n";

      // Make data arrays
      make_array(o,"x",_data.x);
      make_array(o,"y",_data.y);
      make_array(o,"e",_data.e);

      // Collect into an array 
      o << "\n"
	<< "data = vstack((x,y,e)).T\n\n";

      // Make fit arrays 
      make_array(o,"p",_result.parameters);
      make_array(o,"c",_result.covariance);

      // Reshape to matrix 
      o << "c = c.reshape((" << p << "," << p << "))\n\n";

      // Write out chi^2 and nu
      o << "chi2 = " << _result.chi_square << "\n"
	<< "nu   = " << _result.nu         << "\n\n";

      // Write out fit expression;
      o << "f = lambda x,*p: " << expr << "\n\n";

      // Import from matplot lib and set-up
      o << "from matplotlib.pyplot import gca, ion\n\n"
	<< "ion()\n"
	<< "ax = gca()\n\n";
      
      // Now import from nbi_stat and matplotlib 
      o << "from nbi_stat import fit_plot\n\n";

      // Create the plot and write out PNG
      o << "fit_plot(x,y,e,f,p,c,ax=ax,data={'fmt':'o'})\n\n"
	<< "ax.figure.tight_layout()\n"
	<< "ax.figure.savefig('plot_" << _name << ".png')" << std::endl;
    }
    
    /** 
	Create a plot of the fit 

	@param expr   Function expression 

	@ingroup gslfit_utils
    */
    void fit_plot(const std::string& expr) {
      std::ofstream f(("plot_"+_name+".py").c_str());
      fit_plot(f, expr);
    }
    /** @} */

    /** 
	@{ 
	@name Main methods 
    */
    /** 
	Perform the fit
    */
    fit_result fit() {
      if (_funcs == 1) {
	auto wrapped = utils::make_wrappers(_data,_p0,_f,_df);
	return gsl::fit(_cfg,_p0,_data, _data.x.size(),
			std::get<0>(wrapped),
			std::get<1>(wrapped),
			std::get<2>(wrapped),
			std::get<3>(wrapped),
			_verbose);
      }
      if (_funcs == 2) {
	auto wrapped = utils::make_wrappers(_data,_p0,_f,_df,_d2f);
	return gsl::fit(_cfg,_p0,_data, _data.x.size(),
			std::get<0>(wrapped),
			std::get<1>(wrapped),
			std::get<2>(wrapped),
			std::get<3>(wrapped),
			_verbose);
      }
      auto wrapped = utils::make_wrappers(_data,_p0, _f);
      return gsl::fit(_cfg,_p0,_data, _data.x.size(),
		      std::get<0>(wrapped),
		      std::get<1>(wrapped),
		      std::get<2>(wrapped),
		      std::get<3>(wrapped),
		      _verbose);
    }
      
    /** 
	Convert from method number to enum 
	
	@param imeth  Method selection 
	
	@return Method enumeration 
	
	@ingroup gslfit_utils
    */
    gsl::fit_config::method_type method() {
      switch (_method) {
      case 0:
	return gsl::fit_config::method_type::levenberg_marquardt;
      case 1:
	return gsl::fit_config::method_type::accelerated_levenberg_marquardt;
      case 2:
	return gsl::fit_config::method_type::dogleg;
      case 3:
	return gsl::fit_config::method_type::double_dogleg;
      case 4:
	return gsl::fit_config::method_type::subspace;
      case 5:
	return gsl::fit_config::method_type::steihaug_toint;
      }
      return gsl::fit_config::method_type::accelerated_levenberg_marquardt;
    }
    /** 
	Run this example 
    */
    void run(const std::string& expr) {
      // Set method 
      _cfg.method = method();

      utils::timer t;      
      unsigned cnt = _repeat;
      while (cnt--) {
	_result = fit();
	t.lap();

	if (cnt == 0) {
	  if (_repeat >= 10) std::cout << "\n";

	  std::cout << _result << std::endl;

	  if (_plot)
	    fit_plot(expr);
	}
	else if (_repeat >= 10 && cnt % (_repeat / 10) == 0)
	  std::cout << "." << std::flush;
      }
    }
    /** @} */
  protected:
    std::string            _name;
    const parameters_type& _p0;
    const data_type&       _data;
    function_call          _f;
    jacobian_call          _df;
    hessian_call           _d2f;
    unsigned               _method;
    unsigned               _funcs;
    unsigned               _verbose;
    unsigned               _repeat;
    bool                   _plot;
    fit_result             _result;
    fit_config             _cfg;
  };
  //==================================================================
  template <typename Parameters,
	    typename Data,
	    typename Function,
	    typename Jacobian=void*,
	    typename Hessian=void*>
  auto make_example(const std::string&  name,
		    const Parameters&   p0,
		    const Data&         data,
		    Function            f,
		    Jacobian            df=nullptr,
		    Hessian             d2f=nullptr) ->
    decltype(example<Parameters,
	     Data,
	     Function,
	     Jacobian,
	     Hessian>(name,p0,data,f,df,d2f)) {
    return example<Parameters,
		   Data,
		   Function,
		   Jacobian,
		   Hessian>(name,p0,data,f,df,d2f);
  }
		       
}
#endif 
    
    
  
