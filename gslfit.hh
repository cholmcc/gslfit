/**
   @file      gslfit.hh
   @author    Christian Holm Christensen <cholmcc@gmail.com>
   @date      18 Aug 2021
   @copyright (c) 2021 Christian Holm Christensen. 
   @license   LGPL-3
   @brief     C++ interface to GSL fitting routines
*/
#ifndef GSL_FIT_HH
#define GSL_FIT_HH
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_multifit_nlinear.h>
#include <iterator>
#include <map>
#include <iostream>
#include <iomanip>

/** 
    @defgroup gslfit C++ interface to GNU Scientitic Library fitting routines. 

    Code in this module provides C++ wrappers around the GNU
    Scientitic Library fitting routines which simplifies using that
    library in C++-centric way.

    @section gslfit_api Interface 

    The main interface is the function gsl::fit.  The class
    gsl::fitter is a helper class.

    @subsection gslfit_api_p Parameters type 

    The template parameter Parameters is a _Iterable_ container of
    parameter values and values of function evaluations. Typically
    this is one of
     
        std::valarray<double>
        std::vector<double>
     
    but can be any iterable type. 

    @subsection gslfit_api_Data Data type 

    The Data template parameter can be any type.  The only requirement
    is that that the _Callable_ Residuals and (optionally) Jacobian
    and Hessian accet a const reference to an object of that type.

    @subsection gslfit_api_r Residual function 

    The user must supply a _Callable_ (function) @f$ f@f$ with
    signature
    
    @code
    Parameters Residuals(const Data& data, const Parameters& p)
    @endcode

    This function must return the residuals of the data with respect
    to the function @f$ f@f$ being fitted to the data

    @f[ r: x_i,p \mapsto y_i - f(x_i,p)\quad, @f]

    for each point @f$(x_i,y_i)@f$ in `data`.  Note that @f$x_i@f$ can
    of any dimension.

    Note, that for fitting the function @f$ r@f$ is considered a
    function of the _parameters_ (i.e., @f$ x@f$ is considered
    constants)

    @f[ r:p\in\mathbb{P}^n \mapsto y-f(x;p)\in\mathbb{R}\quad. @f]

    @subsection gslfit_api_W Weight function 

    The template parameter Weights must be a _Callable_ with
    signature
     
         Parameters Weights(const Data& data)
     
    If the uncertainty on the dependent variable @f$y@f$ in _data_ is
    @f$\delta@f$ then this function can return
     
    @f[ w_i = \frac{1}{\delta_i^2}\quad,@f]
     
    for each data point @f$(x_i,y_i,\delta_i)@f$ in _data_.  The fit
    will then correspond to a regualar least-squares (@f$\chi^2@f$ )
    fit.

    @subsection gslfit_api_J Jacobian function

    In addition, the user may supply a _Callable_ with signature 

    @code
    Parameters Jacobian(const Data& data, const Parameters& p)
    @endcode 

    which calculates the vector 

    @f[
    J = 
    \begin{pmatrix}\frac{\partial r}{\partial p_1}\\
    \vdots\\
    \frac{\partial r}{\partial p_n}
    \end{pmatrix}
    = 
    -\begin{pmatrix}\frac{\partial f}{\partial p_1}\\
    \vdots\\
    \frac{\partial f}{\partial p_n}
    \end{pmatrix}
    \quad,
    @f] 
    
    evaluted at each data point @f$ x_i@f$ in `data`.  That is,
    the function must calculate the negative gradient with respect
    to the parameters.  Supplying this function will cause GSL to
    use this to determining the next step size.  This can improve
    the fit in some cases.  Note, if this function is not
    supplied, then the gradient will be evaluated numerically.

    Again, note that for fitting the function @f$ f@f$ to data, we
    consider @f$ f@f$ to be a function of the _parameters_ with
    @f$ x@f$ being constants.  Thus the Jacobian vector @f$ J@f$,
    in case @f$ f(x,p)\in\mathbb{R}@f$ is really the negative
    gradient of @f$ @f$ over the domain @f$ P^n@f$

    @f[J: p\in P^n\mapsto-\nabla f\in\mathbb{R}^n\quad.@f] 

    @note You may have heard the term _Jacobian matrix_ and be
    confused why we call it a _Jacobian vector_ here.  In general,
    if the function @f$ g@f$ takes on values in a
    multi-dimensional set
    
    @note 
    @f[g:x\in X^n\mapsto y\in Y^m\quad,@f] 
    then the Jacobian is the matrix
    
    @note
    @f[ 
    J_g = \begin{pmatrix} 
    \frac{\partial f_1}{\partial x_1} 
    & \cdots 
    & \frac{\partial f_1}{\partial x_n}\\
    \vdots 
    & \ddots 
    & \vdots \\
    \frac{\partial f_m}{\partial x_1} 
    & \cdots 
    & \frac{\partial f_m}{\partial x_n}\\
    \end{pmatrix}\quad.
    @f]
    
    @note
    However, if @f$ Y^m=\mathbb{R}^1@f$, then this becomes a
    vector.

    @subsection gslfit_api_H Hessian function 

    Furthermore, the user may also supply a _Callable_ with signature

    @code
    Parameters Hessian(const Data& data, 
                        const Parameters& p,
   	                const Parameters& v)
    @endcode

    which calculates the second directional derivative of @f$ r@f$ 

    @f{align*}{
    v^THv &= 
      \begin{pmatrix} 
        v_1 & \cdots & v_n
      \end{pmatrix}
      \begin{pmatrix}
        \frac{\partial^2 r}{\partial p_1\partial_1}
      & \cdots 
      & \frac{\partial^2 r}{\partial p_1\partial_n}\\
      \vdots & \ddots & \vdots\\
        \frac{\partial^2 r}{\partial p_1\partial p_n}
      & \cdots 
      & \frac{\partial^2 r}{\partial p_n\partial p_n}\\
      \end{pmatrix}
      \begin{pmatrix} 
        v_1\\ \vdots\\ v_n
      \end{pmatrix}\\
      &= 
      -
      \begin{pmatrix} 
        v_1 & \cdots & v_n
      \end{pmatrix}
      \begin{pmatrix}
        \frac{\partial^2 f}{\partial p_1\partial_1}
      & \cdots 
      & \frac{\partial^2 f}{\partial p_1\partial_n}\\
      \vdots & \ddots & \vdots\\
        \frac{\partial^2 f}{\partial p_1\partial p_n}
      & \cdots 
      & \frac{\partial^2 f}{\partial p_n\partial p_n}\\
      \end{pmatrix}
      \begin{pmatrix} 
        v_1\\ \vdots\\ v_n
      \end{pmatrix}\\
      &= 
      - 
      \sum_i\sum_j v_i v_j 
      \frac{\partial^2 f}{\partial p_i\partial p_j}
    @f} 

    If the user supplies this function, then GSL will use that to
    better estimate the next step.  If not supplied, then GSL will
    calculate it numerically if needed by the chosen method (see
    gsl::fit_config). 

    Since @f$ f@f$ is considered a function of the _parameters_,
    this means that @f$ H@f$ is the mapping

    @f[H:p\in P^n\mapsto 
    \nabla\nabla f(x;p)\in\mathbb{R}^n\times\mathbb{R}^n\quad. @f]

    @note Strictly speaking the function `Hessian` does not return the
    Hessian matrix, but the middle factor above

    @note
    @f[
    H = 
      \begin{pmatrix}
        \frac{\partial^2 r}{\partial p_1\partial_1}
      & \cdots 
      & \frac{\partial^2 r}{\partial p_1\partial_n}\\
      \vdots & \ddots & \vdots\\
        \frac{\partial^2 r}{\partial p_1\partial p_n}
      & \cdots 
      & \frac{\partial^2 r}{\partial p_n\partial p_n}\\
      \end{pmatrix}\quad,
    @f]

    @note _is_ the Hessian matrix of @f$ r@f$ with respect to the
    parameters @f$ p@f$. 

    @section gslfit_complex Complexity 

    The complexity of fitting is 

    @f[O(n\log n)\quad,@f] 

    where @f$ n@f$ is the number of data points.  This is actually
    pretty good for a sophisticated algorithm.

    @image html complexity.png 

    The plot above shows the average time @f$\overline{t}@f$ in
    milliseconds as a function of the number of data points @f$n@f$ to
    fit to.  Also shown are fits

    @f[f(n;p) = p_1 n \log n + p_2\quad,@f]

    to the timing data. The data fitted to was normal distributed and
    the fitting function was likewise a (scaled) normal distribution
    (see @ref gslfit_example_normal).  Three cases are shown

    - Numeric determination of the Jacobian and Hessian (passing two
      `nullptr` arguments to gsl::fit).

    - Analytic expression for the Jacobian, and numeric determination
      of the Hessian (passing example::normal::fit::df and a single
      `nullptr` argument to gsl::fit).

    - Analytic expression of both the Jacobian and Hessian (passing
      exmaple::normal::fit::df and example::normal::fit::d2f to
      gsl::fit).

    From the plot it is evindent that the complexity is the same
    (@f$ O(n\log n)@f$) in all cases and the prefactors @f$p_1@f$ are
    roughly equal.  Thus, one does not necessarily acheive much
    speed-up by specifying analytic expression for the Jacobian or
    Hessian of the fitted function in so far as the fitted function is
    similar to a normal distribution.
*/

/** 
    Namespace for GNU Scientitic Library C++ interface 
    
    @ingroup gslfit
*/
namespace gsl
{
  namespace {
    /**
        Wrap double for fixed precision and width when streaming

	@ingroup gslfit 
    */
    template <size_t width,
	      size_t precision,
	      std::ios_base::fmtflags fmt=std::ios_base::fixed>
    struct wdouble {
      /** 
	  Construct from a value 

	  @param x Value 
      */
      wdouble(double x) : _x(x) {};
      /** The value */
      double _x;
    };
    /**
        Stream out wrapped double

	@param o  Output stream 
	@param x  Value 

	@return @a o 
	
	@ingroup gslfit 
    */
    template <size_t width,
	      size_t precision,
	      std::ios_base::fmtflags fmt>
    std::ostream& operator<<(std::ostream& o,
			     const wdouble<width,precision,fmt>& x) {
      auto op = o.precision(precision);
      auto of = o.setf(fmt,std::ios_base::floatfield);
      o << std::setw(width) << x._x;
      o.precision(op);
      o.setf(of,std::ios_base::floatfield);
      return o;
    }
  }
    
  //==================================================================
  /**
     The return structure from a fit
  
     This contains
  
     * the status which is one of the enumerated values
     * the chi_square (objective function value)
     * number degrees of freedom
     * best-fit parameter values
     * best-fit covariance of parameters
     * the number of calls to 
       * residual function 
       * Jacobian function 
       * Hessian function 
  
     The covariance is encode in standard C fashion - that is as
     row-major (elements of a row are continuous).  To access row i,
     column j element do
  
         fit_result<Parameters> res = ...
         cov_ij = res.covariance[i * res.parameters.size() + j]
  
     The uncertainties on the best-fit parameter values are given by
     the square root of the diagonal elements
  
         uncer_i = std::sqrt(res.covariance[i * res.parameters.size() + j])

     If Parameters is for example std::valarray<double>, one can
     extract the uncertainties with

         size_t npar = res.parameters.size();
         uncer = std::sqrt(res.covariance()[std::slice(0,n,n+1)]);

     @ingroup gslfit 
  */ 
  template <typename Parameters>
  struct fit_result
  {
    /** Type of parameters */
    using parameters_type=Parameters;
    
    /**
        Status of fit 
    */
    enum {
      no_progress,
      max_iterations,
      failed,
      small_step,
      small_gradient,
      small_residuals } status;
    /** chi-square (objective function) value  */
    double      chi_square;
    /** number of degrees of freedom  */
    size_t      nu;
    /** best-fit parameter values  */
    Parameters  parameters;
    /** Covariance of best-fit parameter values */
    Parameters  covariance;
    /** Number of iterations */
    size_t iterations;
    /** Number of calls to residual function */
    size_t residual_calls;
    /** Number of calls to jacobian function */
    size_t jacobian_calls;
    /** Number of calls to hessian function */
    size_t hessian_calls;

    /**
        Constructor 
    */
    fit_result(size_t npar, size_t ndata)
      : status(failed),
	chi_square(0),
	nu(ndata-npar),
	parameters(npar),
	covariance(npar*npar),
	iterations(0),
	residual_calls(0),
	jacobian_calls(0),
	hessian_calls(0)
    {}

    /** 
	Evalate status of the fit as  

	@return true if fit succeeded. 
    */
    operator bool() const { return status > failed; }
  };

  //------------------------------------------------------------------
  /**
     Stream out a fit_result

     @param o   Output stream 
     @param res fit result 

     @return @a o 

     @ingroup gslfit 
  */
  template <typename Parameters>
  std::ostream& operator<<(std::ostream& o, const fit_result<Parameters>& res)
  {
    using FR=fit_result<Parameters>;
    size_t n = res.parameters.size();
    size_t iw = 3;
    size_t vw = 12;

    
    std::map<unsigned,std::string> sttext =
      { { FR::no_progress,    "no progress"},
	{ FR::max_iterations, "ran out"},
	{ FR::failed,         "failed"},
	{ FR::small_step,     "small step"},
	{ FR::small_gradient, "small gradient"},
	{ FR::small_residuals,"small residuals" } };
	
    o << "chi^2/nu: " << res.chi_square << "/" << res.nu << "="
      << res.chi_square / res.nu << " (" << sttext[res.status] << ")\n"
      << std::setw(iw) << "#" << " | "
      << std::setw(vw) << "Value" << " | "
      << std::setw(vw) << "Error" << " |";
    for (size_t i = 0; i < n; i++)
      o << " " << std::setw(vw) << i;

    o << '\n'
      << std::string(iw,'-') << "-+-"
      << std::string(vw,'-') << "-+-"
      << std::string(vw,'-') << "-+";
    for (size_t i = 0; i < n; i++)
      o << std::string(vw+1,'-');

    for (size_t i = 0; i < n; i++) {
      o << "\n"
	<< std::setw(iw) << i << " | "
	<< std::setw(vw) << res.parameters[i] << " | "
	<< std::setw(vw) << std::sqrt(res.covariance[i*n+i]) << " |";
      for (size_t j = 0; j < n; j++)
	o << " " << std::setw(vw) << res.covariance[i*n+j];
    }
    o << "\n"
      << "# iterations: " << std::setw(iw) << res.iterations
      << " # calls: "
      << std::setw(iw) << res.residual_calls << "(residuals) "
      << std::setw(iw) << res.jacobian_calls << "(jacobian) "
      << std::setw(iw) << res.hessian_calls << "(hessian)";
    return o;
  }

  //==================================================================
  /**
      Configuration of fit
     
      Objects of this kind holds the configuration parameters for
      fits.  The parameters are
     
      * `method`     The kind of trust region search method
      * `scale.      The kind of scaling of trust region 
      * `inversion`  The kind of matrix inversion algorithm
      * `difference` The kind of finite difference for numerical
         derivatives
      *	`up_factor`, 'down_factor` Up and down scaling factors of
        trust regtion
      * `max_relative_acceleration` Maximum acceleration to velocity
         factor
      * `difference_step` the step size for numerical derivatives 
      * `relative_difference_step` the relative step size when
        calculating numerical Hessian
      * `max_iterations` The maximum number of iterations 
      * `parameter_tolerance` lower bound on change in parameters
      * `gradient_tolerance` lower bound on change in gradient 
      * `residual_tolerance` lower bound on change in residuals 

      @ingroup gslfit
  */
  struct fit_config
  {
    /**
       The various methods for searching the trust region
    
       The default is levenberg-marquardt with geodesic accelaration.
    */
    enum class method_type {
      levenberg_marquardt,
      accelerated_levenberg_marquardt, // Use 2nd diff
      dogleg,
      double_dogleg,
      subspace,
      steihaug_toint
    } method = method_type::accelerated_levenberg_marquardt;

    /** 
        Scale method for step matrix.
       
        Default is more 
    */
    enum class scale_type {
      more,
      levenberg,
      marquardt } scale = scale_type::more;

    /**
        Matrix inversion solver method
    */
    enum class inversion_type {
      qr,
      cholesky,
      modified_cholesky,
      svd } inversion = inversion_type::qr;

    /** 
        Difference type for numerical derivatives. Default
        is forward difference.
    */
    enum class difference_type {
      forward,
      centered } difference = difference_type::forward;

    /**
        Upscaling factor of trust region 
    */
    double up_factor = 3.;

    /**
        Downscaling factor of trust region
    */
    double down_factor = 2;

    /**
        Maximum relative (wrt. velocity) acceleration when using
        geodesic acceleration.  Steps that has a ratio of acceleration
        to velocity larger than this parameter are discared.
    */
    double max_relative_acceleration = 0.75;

    /**
        Step size when evaluating finite differences for numerical
        derivatives.
    */
    double difference_step = GSL_SQRT_DBL_EPSILON;

    /**
        Relative (wrt. veloctiy) step size when evaluating finite
        difference for second order derivatives with geodesic
        acceleration.  This parameter is the fraction of the current
        velocity taken as a step.
    */
    double relative_difference_step = 0.02;

    /**
        Maximum number of iterations to perform
    */
    size_t max_iterations = 100;

    /**
        Relative tolerance @f$ r@f$ on parameter change.  If the
        change in parameter value @f$\delta_i@f$ meets the criteria
       
        @f[|\delta_i| \leq r(|p_i|+r)\quad,@f]
       
        then iteration stops.  This is indicated by the status code 
	`small_step`.
    */
    double parameter_tolerance = 1e-8;

    /**
        Relative tolerance @f$ r@f$ on gradient change.  If the
        gradient @f$g=\nabla\Phi(p)@f$ of the objective function
        @f$f=\Phi(x)@f$ meetsthe condition
       
        @f[\mathrm{max}_i|g_i \mathrm{max}(x_i,1)|\leq 
           r\mathrm{max}(f,1)\quad,@f]
       
        then iteration stop.  This is indicated by the status code
        `small_gradient`
    */
    double gradient_tolerance = 1e-8;

    /**
        Relative tolerance @f$ r@f$ for residuals.  If the change in
        residuals @f$\epsilon=||f(x+\delta)-f(x)||@f$ meets the
        condition
       
        @f[\epsilon \leq r\mathrm{max}(||f(x)||,1)\quad,@f]
       
        then the iteration is stopped.  This is indicated by the
        status code `small_residuals`
    */
    double residuals_tolerance = 0;
    
    /**
        Our parameter object we will fill 
    */
    gsl_multifit_nlinear_parameters create() const
    {
      gsl_multifit_nlinear_parameters p =
	gsl_multifit_nlinear_default_parameters();

      std::map<method_type,const gsl_multifit_nlinear_trs*> m = {
	{method_type::levenberg_marquardt, gsl_multifit_nlinear_trs_lm},
	{method_type::accelerated_levenberg_marquardt,
	 gsl_multifit_nlinear_trs_lmaccel},
	{method_type::dogleg,         gsl_multifit_nlinear_trs_dogleg},
	{method_type::double_dogleg,  gsl_multifit_nlinear_trs_ddogleg},	
	{method_type::subspace,       gsl_multifit_nlinear_trs_subspace2D},
	{method_type::steihaug_toint, gsl_multifit_nlinear_trs_lmaccel}};

      std::map<scale_type,const gsl_multifit_nlinear_scale*> s = {
	{scale_type::more,      gsl_multifit_nlinear_scale_more},
	{scale_type::levenberg, gsl_multifit_nlinear_scale_levenberg},
	{scale_type::marquardt, gsl_multifit_nlinear_scale_marquardt}};

      std::map<inversion_type,const gsl_multifit_nlinear_solver*> i = {
	{inversion_type::qr,        gsl_multifit_nlinear_solver_qr},
	{inversion_type::cholesky,  gsl_multifit_nlinear_solver_cholesky},
	{inversion_type::modified_cholesky,
	 gsl_multifit_nlinear_solver_mcholesky},
	{inversion_type::svd,       gsl_multifit_nlinear_solver_svd}};
	
      std::map<difference_type,gsl_multifit_nlinear_fdtype> d = {
	{difference_type::forward,  GSL_MULTIFIT_NLINEAR_FWDIFF},
	{difference_type::centered, GSL_MULTIFIT_NLINEAR_CTRDIFF}};
      
    
      p.trs         = m[method];
      p.scale       = s[scale];
      p.solver      = i[inversion];
      p.fdtype      = d[difference];
      p.factor_up   = up_factor;
      p.factor_down = down_factor;
      p.avmax       = max_relative_acceleration;
      p.h_df        = difference_step;
      p.h_fvv       = relative_difference_step;
      
      return p;
    }      
  };

  //==================================================================
  /**
     Helper class to implement the gsl fitting
     
     @tparam Parameters type of parameter container (_Iterable_)
     @tparam Data       type of data container
     @tparam Residuals  type of residual function
     @tparam Weights    type of weight function
     @tparam Jacobian   type of Jacobian function
     @tparam Hessian   type of hessian function
     
     See also @ref gslfit_api 

     @ingroup gslfit
  */
  template <typename Parameters,
	    typename Data,
	    typename Residuals,
	    typename Weights,
	    typename Jacobian,
	    typename Hessian>
  class fitter {
  public:
    /** Type of parameters */
    using parameters_type=Parameters;
    /** Type of data */
    using data_type=Data;
    /** Type of residuals callable */
    using residuals_call=Residuals;
    /** Type of weights callable */
    using weights_call=Weights;
    /** Type of Jacobian callable */
    using jacobian_call=Jacobian;
    /** Type of Hessian callable */
    using hessian_call=Hessian;
    /** Type of result */
    using result_type=fit_result<parameters_type>;

  protected:
    /**
        Structure that holds the user supplied functions and other
        information needed for calculating the residuals and jacobian
    */
    struct user_type {
      /** The residuals function  */
      residuals_call residuals;
      /** The Jacobian functoin  */
      jacobian_call jacobian;
      /** The Hessian functoin */
      hessian_call hessian;
      /** The data to fit to  */
      const data_type& data;
      /** The number of parameters  */
      size_t npar;
    };
    /**
        Structure that holds the user supplied callback functoin and
        other information needed for callbacks.
    */
    struct context_type {
      unsigned      verbose;
      size_t        npar;
      std::ostream& out;
    };
  public:

    /**
        Constructor
       
        @param config    Fit configuration 
        @param p0        Intial guess at parameter values
        @param data      Data to fit to
        @param ndata     Number of data points in data
        @param residuals Callable that evaluates the residuals
        @param weights   Callable that evaluates the weights
        @param jacobian  Callable that evaluates the Jacobian at each point
        @param hessian   Callable called on each iteration    
	@param out       Output stream for logging messages
    */
    fitter(const fit_config&      config,
	   const parameters_type& p0,
	   const data_type&       data,
	   size_t                 ndata,
	   residuals_call         residuals,
	   weights_call           weights,
	   jacobian_call          jacobian = nullptr,
	   hessian_call           hessian = nullptr,
	   std::ostream&          out = std::cout)
      : _npar(std::distance(std::begin(p0),std::end(p0))),
	_config(config),
	_type(gsl_multifit_nlinear_trust),
	_parameters(config.create()),
	_work(0),
	_wvector(gsl_vector_alloc(ndata)),
	_guess(gsl_vector_alloc(_npar)),
	_jacobian(0),
	_covar(0),
	_user{residuals,jacobian,hessian,data,_npar},
	_context{0,_npar, out},
	_result(_npar,ndata)  {
      // Set initial guess of parameters 
      to_vector(p0,_guess);

      // Allocate workspace
      _work = gsl_multifit_nlinear_alloc(_type,
					 &_parameters,
					 ndata,
					 _npar);
      // Calculate weights on data points (typicall 1/error_squared)
      auto        wdata   = weights(data);
      to_vector(weights(data), _wvector);

      // Set function wrappers 
      _functions.f      = fitter::residuals;
      _functions.df     = NULL;
      _functions.fvv    = NULL;
      _functions.n      = ndata;
      _functions.p      = _npar;
      _functions.params = &_user;
    }

    /**
        Destructor 
    */
    ~fitter() {
      gsl_multifit_nlinear_free(_work);
      gsl_matrix_free(_covar);
      gsl_vector_free(_guess);
      gsl_vector_free(_wvector);
    }

    /**
        Perform the fit and return result
       
        @param verbose Verbosity 
       
        @returns A fit_result<Parameter> object with best-fit information 
    */
    result_type operator()(unsigned verbose=0) {
      
      _context.verbose = verbose;
      
      set_jacobian(_user.jacobian);
      set_hessian(_user.hessian);
      

      // Initialize the solver 
      gsl_multifit_nlinear_winit(_guess, _wvector, &_functions, _work);

      // Compute the initial cost (chi^2)
      gsl_vector* fy = gsl_multifit_nlinear_residual(_work);
      double chi20;
      gsl_blas_ddot(fy, fy, &chi20);

      // Solve the system
      int info;
      int status = gsl_multifit_nlinear_driver(_config.max_iterations,
					       _config.parameter_tolerance,
					       _config.gradient_tolerance,
					       _config.residuals_tolerance,
					       fitter::callback,
					       &_context,
					       &info,
					       _work);
      _result.status = result_type::failed;
      if (status == GSL_EMAXITER)
	_result.status = result_type::max_iterations;
      switch (info) {
	//case 0:
	// _result.status = result_type::failed; break;
      case 1:
	_result.status = result_type::small_step; break;
      case 2:
	_result.status = result_type::small_gradient; break;
      case 3:
	_result.status = result_type::small_residuals; break;
      case GSL_ENOPROG:
	_result.status = result_type::no_progress; break;
      }
      _result.iterations     = gsl_multifit_nlinear_niter(_work);
      _result.residual_calls = _functions.nevalf;
      _result.jacobian_calls = _functions.nevaldf;
      _result.hessian_calls  = _functions.nevalfvv;
      
      // Jacobian and covariance 
      _jacobian = gsl_multifit_nlinear_jac(_work);
      _covar    = gsl_matrix_alloc(_npar,_npar);
      gsl_multifit_nlinear_covar(_jacobian, 0., _covar);
      
      // Final chi2
      double chi2;
      gsl_blas_ddot(fy, fy, &chi2);

      // Set fields in return structure
      _result.chi_square = chi2;
      
      from_vector(_work->x, _result.parameters);
      from_matrix(_covar,   _result.covariance);

      return _result;
    }
  protected:
    /** Number of paramters  */
    size_t _npar;
    /** Configuration of the fit */
    const fit_config& _config;
    /** Fitter algorithm  */
    const gsl_multifit_nlinear_type* _type;
    /** Parameters for the fitter algorithm  */
    gsl_multifit_nlinear_parameters _parameters;
    /** The fitter workspace  */
    gsl_multifit_nlinear_workspace* _work;
    /** Vector of weights  */
    gsl_vector* _wvector;
    /** Structure that holds functions needed by fitter  */
    gsl_multifit_nlinear_fdf _functions;
    /** Initial guess at parameter values  */
    gsl_vector* _guess;
    /** The jacobian  */
    gsl_matrix*  _jacobian;
    /** The covariance  */
    gsl_matrix* _covar;
    /** The user supplied data */
    user_type _user;
    /** The callback context  */
    context_type _context;
    /** The result  */
    result_type _result;

    /**
        @{
        @name Wrapper functions
    */
    /**
        Residuals wrapper
       
        Calls the user supplied Residuals _Callable_ like
       
            Residuals(const Data&       data,
                      const Parameters& parameters)
       
        and stores the result in the vector @a o
       
        @param p    Current parameter values
        @param user User data
        @param o    Output vector
       
        @returns `GSL_SUCCESS` 
    */
    static int residuals(const gsl_vector* p, void* user, gsl_vector* o) {
      user_type* u = reinterpret_cast<user_type*>(user);
      
      // Copy GSL vector elements of parameters to std::vector      
      parameters_type pp(u->npar);
      from_vector(p, pp);
			      
      // Evaluate residuals on the data
      to_vector(u->residuals(u->data,pp), o);
	
      return GSL_SUCCESS;
    };

    /**
        Wrap passed jacobian function and data points in lambda
       
        Calls the user supplied JAcobian _Callable_ like
       
            Jacobian(const Data&       data,
                      const Parameters& parameters)
       
        and stores the result in the vector @a o
       
        @param p    Current parameter values
        @param user User data
        @param o    Output matrix
       
        @returns `GSL_SUCCESS` 
    */
    static int jacobian(const gsl_vector* p, void* user, gsl_matrix* o) {
      user_type* u = reinterpret_cast<user_type*>(user);
      
      // Copy GSL vector elements of parameters to std::vector      
      parameters_type pp(u->npar);
      from_vector(p, pp);
			      
      // Evaluate residuals on the data
      to_matrix(u->jacobian(u->data,pp), o);

      return GSL_SUCCESS;
    };

    /**
        Wrap passed hessian function and data points in lambda
       
        Calls the user supplied Hessian _Callable_ like
       
            Hessian(const Data&       data,
                    const Parameters& parameters)
       
        and stores the result in the vector @a o
       
        @param p    Current parameter values
        @param v    Current velocity
        @param user User data
        @param o    Output vector
       
        @returns `GSL_SUCCESS` 
    */
    static int hessian(const gsl_vector* p,
		       const gsl_vector* v,
		       void*             user,
		       gsl_vector*       o) {
      user_type* u = reinterpret_cast<user_type*>(user);
      
      // Copy GSL vector elements of parameters to std::vector      
      parameters_type pp(u->npar);
      parameters_type vv(u->npar);
      from_vector(p, pp);
      from_vector(v, vv);
			      
      // Evaluate residuals on the data
      to_vector(u->hessian(u->data,pp,vv), o);
	
      return GSL_SUCCESS;
    };

    /**
        Callback
       
        If the user supplied a callback _Callable_, then that is called like
       
             Callback(size_t step,
                      const Parameters& parameters,
                      double            condition,
                      double            abs_objective)
        
        @param step    Step (iteration) number
        @param context The context
        @param work    The workspace 
    */
    static void callback(const size_t                          step,
			 void*                                 context,
			 const gsl_multifit_nlinear_workspace* work) {
      
      context_type*    ctx = reinterpret_cast<context_type*>(context);

      if (ctx->verbose <= 0) return;

      std::ostream& o    = ctx->out;
      if (step == 0) 
	o << "Solver: " << gsl_multifit_nlinear_name(work) << ", "
	  << "method: " << gsl_multifit_nlinear_trs_name(work)
	  << std::endl;

      if (ctx->verbose <= 1) return;
	
      gsl_vector* r    = gsl_multifit_nlinear_residual(work);
      gsl_vector* x    = gsl_multifit_nlinear_position(work);
      double      chi2;
      gsl_blas_ddot(r, r, &chi2);
      
      parameters_type pp(ctx->npar);
      from_vector(x,pp);

      o << std::setw(3) << step << ": ";
      std::copy(std::begin(pp), std::end(pp),
		std::ostream_iterator<wdouble<10,4>>(o,", "));
      o << " chi^2=" << wdouble<10,4>(chi2);
      
      if (ctx->verbose > 2) {
	double rcond;
	gsl_multifit_nlinear_rcond(&rcond, work);
	o << " condition=" << wdouble<10,4>(1/rcond);
      }

      if (ctx->verbose > 3) 
	o << " |a/v|=" << wdouble<10,4>(gsl_multifit_nlinear_avratio(work));
	
      o << std::endl;

      if (ctx->verbose > 4) {
	gsl_matrix* jac = gsl_multifit_nlinear_jac(work);
	
	for (size_t i = 0; i < jac->size1; i++) {
	  o << std::setw(12) << gsl_vector_get(r,i) << " | ";

	  for (size_t j = 0; j < jac->size2; j++) 
	    o << std::setw(12) << gsl_matrix_get(jac,i,j);

	  o << std::endl;
	}
      }
      
    }
    /** @} */

    /**
        @{
        @name SFINAE specialisation for different user options
    */
    /**
        Set jacobian wrapper - in case it isn't given
       
        This does nothing, leaving the Jacobian unspecified.  The
        fitter will use numerical evaluation of the Jacobian in this
        case.
    */
    template <typename J,
	      typename std::enable_if<std::is_same<J,void*>::value ||
				      std::is_same<J,nullptr_t>::value,
				      bool>::type = true>
    void set_jacobian(J)
    {
    }

    /**
        Set the jacobian wrapper
       
        This is enabled if the user passed a _Callable_ as
        Jacobian. In this case, the fitter will use the user supplied
        _Callable_ to calculate the Jacobian matrix.
    */
    template <typename J,
	      typename std::enable_if<!std::is_same<J,void*>::value &&
				      !std::is_same<J,nullptr_t>::value,
				      bool>::type = true>
    void set_jacobian(J)
    {
      _functions.df = fitter::jacobian;
    }

    /**
        Set hessian wrapper - in case it isn't given
       
        This does nothing, leaving the Hessian unspecified.  The
        fitter will use numerical evaluation of the Hessian in this
        case.
    */
    template <typename H,
	      typename std::enable_if<std::is_same<H,void*>::value ||
				      std::is_same<H,nullptr_t>::value,
				      bool>::type = true>
    void set_hessian(H)
    {
    }

    /**
        Set the hessian wrapper
       
        This is enabled if the user passed a _Callable_ as
        Hessian. In this case, the fitter will use the user supplied
        _Callable_ to calculate the Hessian matrix.
    */
    template <typename H,
	      typename std::enable_if<!std::is_same<H,void*>::value &&
				      !std::is_same<H,nullptr_t>::value,
				      bool>::type = true>
    void set_hessian(H)
    {
      _functions.fvv = fitter::hessian;
    }
    /** @} */

    /**
        @{
        @name Helper function to copy/to from GSL structures
    */
    /**
        Copy values from an _Iterable_ to a `gsl_vector`
       
        @param from _Iterable_ to copy from 
        @param to   `gsl_vector` to copy to
    */
    template <typename Iterable>
    static void to_vector(const Iterable& from, gsl_vector* to)
    {
      auto start = std::begin(from);
      for (auto i = start; i != std::end(from); ++i)
	gsl_vector_set(to, std::distance(start,i), *i);
    }
    /**
        Copy values from a `gsl_vector` to an _Iterable_
       
        @param from `gsl_vector` to copy from
        @param to   _Iterable_ to copy to 
    */
    template <typename Iterable>
    static void from_vector(const gsl_vector* from, Iterable& to)
    {
      auto start = std::begin(to);
      for (auto i = start; i != std::end(to); ++i)
	*i = gsl_vector_get(from, std::distance(start,i));
    }
    /**
        Copy values from an _Iterable_ to a gsl_matrix
       
        The matrix is assumed to be encoded in standard C style - that
        is in row-major mode (elements of a row is consecutive)
       
        @param from _Iterable_ to copy from 
        @param to   `gsl_matrix` to copy to
    */
    template <typename Iterable>
    static void to_matrix(const Iterable& from, gsl_matrix* to)
    {
      auto   start = std::begin(from);
      size_t n     = to->size2; // # columns
      for (auto i = start; i != std::end(from); ++i) {
	size_t j = std::distance(start,i);
	gsl_matrix_set(to, j / n, j % n, *i);
      }
    }
    /**
        Copy values from a `gsl_matrix` to an _Iterable_
       
        The matrix will be encoded in standard C style - that is in
        row-major mode (elements of a row is consecutive)
       
        @param from `gsl_matrix` to copy from
        @param to   _Iterable_ to copy to 
    */
    template <typename Iterable>
    static void from_matrix(const gsl_matrix* from, Iterable& to)
    {
      auto   start = std::begin(to);
      size_t n     = from->size2; // # columns
      for (auto i = start; i != std::end(to); ++i) {
	size_t j = std::distance(start,i);
	*i = gsl_matrix_get(from, j / n, j % n);
      }
    }
    /** @} */
  };

  //==================================================================
  /**
     Function to fit function to data 
     
     @tparam Parameters type of parameter container (_Iterable_)
     @tparam Data       type of data container
     @tparam Residuals  type of residual function
     @tparam Weights    type of weight function
     @tparam Jacobian   type of Jacobian function
     @tparam Hessian   type of hessian function

     See also @ref gslfit for more on the requirements. 

     @param config    Configuration of fit 
     @param p0        Intial guess at parameter values
     @param data      Data to fit to
     @param n         Number of data points in data
     @param residuals Callable that evaluates the residuals
     @param weights   Callable that evaluates the weights
     @param jacobian  Callable that evaluates the Jacobian at each point
     @param hessian   Callable that evaluates the Hessian at each point
     @param verbose   Verbosity level
     @param output    Output stream for logging 
     
     @returns A fit_result<Parameter> object with best-fit information

     @ingroup gslfit
  */
  template <typename Parameters,
	    typename Data,
	    typename Residuals,
	    typename Weights,
	    typename Jacobian,
	    typename Hessian>
  fit_result<Parameters> fit(const fit_config&   config,
			     const Parameters&  p0,
			     const Data&        data,
			     size_t             n,
			     Residuals          residuals,
			     Weights            weights,
			     Jacobian           jacobian,
			     Hessian            hessian,
			     unsigned           verbose=0,
			     std::ostream&      output=std::cout)
  {
    fitter<Parameters,Data,Residuals,Weights,Jacobian,Hessian> o(config,
								 p0,
								 data,
								 n,
								 residuals,
								 weights,
								 jacobian,
								 hessian,
								 output);
    return o(verbose);
  }
  //------------------------------------------------------------------
  /**
     Function to fit function to data 
     
     @tparam Parameters type of parameter container (_Iterable_)
     @tparam Data       type of data container
     @tparam Residuals  type of residual function
     @tparam Weights    type of weight function
     @tparam Jacobian   type of Jacobian function

     See also @ref gslfit for more on the requirements. 

     @param config    Configuration of fit 
     @param p0        Intial guess at parameter values
     @param data      Data to fit to
     @param n         Number of data points in data
     @param residuals Callable that evaluates the residuals
     @param weights   Callable that evaluates the weights
     @param jacobian  Callable that evaluates the Jacobian at each point
     @param verbose   Verbosity level
     @param output    Output stream for logging 
     
     @returns A fit_result<Parameter> object with best-fit information

     @ingroup gslfit
  */
  template <typename Parameters,
	    typename Data,
	    typename Residuals,
	    typename Weights,
	    typename Jacobian>
  fit_result<Parameters> fit(const fit_config&  config,
			     const Parameters&  p0,
			     const Data&        data,
			     size_t             n,
			     Residuals          residuals,
			     Weights            weights,
			     Jacobian           jacobian,
			     unsigned           verbose=0,
			     std::ostream&      output=std::cout)
  {
    fitter<Parameters,Data,Residuals,Weights,Jacobian,void*> o(config,
							       p0,
							       data,
							       n,
							       residuals,
							       weights,
							       jacobian,
							       nullptr,
							       output);
    return o(verbose);
  }
  //------------------------------------------------------------------
  /**
     Function to fit function to data 
     
     @tparam Parameters type of parameter container (_Iterable_)
     @tparam Data       type of data container
     @tparam Residuals  type of residual function
     @tparam Weights    type of weight function

     See also @ref gslfit for more on the requirements. 

     @param config    Configuration of fit 
     @param p0        Intial guess at parameter values
     @param data      Data to fit to
     @param n         Number of data points in data
     @param residuals Callable that evaluates the residuals
     @param weights   Callable that evaluates the weights
     @param verbose   Verbosity level
     @param output    Output stream for logging 
     
     @returns A fit_result<Parameter> object with best-fit information

     @ingroup gslfit
  */
  template <typename Parameters,
	    typename Data,
	    typename Residuals,
	    typename Weights>
  fit_result<Parameters> fit(const fit_config&   config,
			     const Parameters&  p0,
			     const Data&        data,
			     size_t             n,
			     Residuals          residuals,
			     Weights            weights,
			     unsigned           verbose=0,
			     std::ostream&      output=std::cout)
  {
    fitter<Parameters,Data,Residuals,Weights,void*,void*> o(config,
							    p0,
							    data,
							    n,
							    residuals,
							    weights,
							    nullptr,
							    nullptr,
							    output);
    return o(verbose);
  }

  //==================================================================
  /** 
      @example normal.cc 
    
      Generate normal distributed data (with noise) and fit a normal
      distribution to the that.

      Note, we allow for different variations of the fit, including 

      * Different trust region methods 
      * Use of analytic Jacobian 
      * Use of analytic Hessian
      
      Note that we use std::valarray<double> as our data container,
      since that allows for easy and fast vector operations.

      See also @ref gslfit_example_normal
  */
  //------------------------------------------------------------------
  /** 
      @example unormal.cc 
    
      Generate normal distributed data (with noise) and fit a normal
      distribution to the that.

      Note, we allow for different variations of the fit, including 

      * Different trust region methods 
      * Use of analytic Jacobian 
      * Use of analytic Hessian
      
      Note that we use std::valarray<double> as our data container,
      since that allows for easy and fast vector operations.

      See also @ref gslfit_example_unormal
  */
  //------------------------------------------------------------------
  /** 
      @example expo.cc 
    
      Generate exponentially distributed data (with noise) and fit a
      exponential distribution to the that.

      Note, we allow for different variations of the fit, including 

      * Different trust region methods 
      * Use of analytic Jacobian 
      
      Note that we use std::valarray<double> as our data container,
      since that allows for easy and fast vector operations.

      See also @ref gslfit_example_expo
  */
  //------------------------------------------------------------------
  /** 
      @example simple.cc 
    
      Fit a normal distribution to histogram of normal random variable. 
      
      Note that we use std::valarray<double> as our data container,
      since that allows for easy and fast vector operations.

      See also @ref gslfit_example_simple
  */
  //------------------------------------------------------------------
  /** 
      @example lorentz.cc 
    
      Fit a Lorentz (or Briet-Wigner) distribution (including
      background) to a resonance spectrum. 
      
      Note that we use std::valarray<double> as our data container,
      since that allows for easy and fast vector operations.

      See also @ref gslfit_example_lorentz
  */

}
#endif
// Local Variables:
//  compile-command: "g++ -c gslfit.hh"
// End:
