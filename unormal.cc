/**
   @file      unormal.cc
   @author    Christian Holm Christensen <cholmcc@gmail.com>
   @date      18 Aug 2021
   @copyright (c) 2021 Christian Holm Christensen. 
   @license   LGPL-3
   @brief     Second geodesic example from GSL documentation
*/
#include <random>
#include <valarray>
#include <iostream>
#include <chrono>
#include <gslfit.hh>
#include <utils.hh>
#include <example.hh>

namespace example
{
  /** 
      @defgroup gslfit_example_unormal Fitting unormalized normal distributed data

      @ingroup gslfit_examples

      @note This correponds to the [second geodesic
      example](https://www.gnu.org/software/gsl/doc/html/nls.html#geodesic-acceleration-example-2)
      in the GSL documentation.

      We fit the function 

      @f[ f(x;a,\mu,\sigma) = ae^{-\frac12\left(\frac{x-\mu}{\sigma}\right)^2}
      \quad,
      @f] 
      
      to data.  

      The data is has @f$ N=300@f$ points and have 

      @f{align*}{
      x &= \left\{x_i=i\frac{1}{N}\middle|i=0,\ldots,N-1\right\}\\
      \delta &= 
      \left\{\delta_i 
      \sim\mathcal{N}\left[0,\frac1{10}f(x_i;a_0,\mu_0,\sigma_0)\right]
      \middle|i=1,\ldots,N-1\right\}\\
      y &= \left\{y_i=f(x_i;a_0,\mu_0,\sigma_0)+\delta_i\middle|
                  i=1,\ldots,N-1\right\}\\
      e &= \left\{e_i=\delta_i\middle|i=1,\ldots,N-1\right\}\quad,
      @f}

      where 
      
      @f{align*}{
      a_0      &= 5\\
      \mu_0    &= 0.4\\
      \sigma_0 &= 0.15 
      @f}      

      @image html unormal.png 

      The class example::unormal::fit encodes the fitting function @f$
      f@f$, the gradient @f$J=\nabla f@f$, and the directional second
      derivative @f$ v^THv=v^T(\nabla\nabla f)v@f$.

      The class example::unormal::data contains our data.  
      

      @b Timings

      The test was performed on a 

          Intel(R) Core(TM) i7-4550U CPU @ 1.50GHz

      with 1,000 runs of each fit kind

      | Jacobian | Hessian  | Average time (ms) |
      |----------|----------|-------------------|
      | numeric  | numeric  |  1.570 +/- 0.006  |
      | analytic | numeric  |  2.068 +/- 0.009  |
      | analytic | analytic |  2.696 +/- 0.008  |
  */
  /** 
      Namespace for this example 

      @ingroup gslfit_example_unormal
  */
  namespace unormal
  {
    /** 
	Array of values.  We use std::valarray as this containter (as
	opposed to f.ex. std::vector) allows for optimised computations
	in parallel.

	@ingroup gslfit_example_unormal
    */
    using values=std::valarray<double>;

    /** 
	Normal distribution functions 

	@ingroup gslfit_example_unormal
    */
    struct fit
    {
      /**
	 Calculates
     
	 @f[ f = a e^{-\frac{1}{2}z^2}\quad.@f]
      */ 
      static values f(const values& x, const values& p)
      {
	double a     = p[0];
	double mu    = p[1];
	double sigma = p[2];
	auto   z     = (x - mu) / sigma;
	auto   s     =  std::exp(-std::pow(z,2)/2);
	return a * s;
      }

      /**
	 Calculates
       
	 @f[
         \nabla f=\begin{bmatrix}
	 \frac{\partial f}{\partial a}\\
	 \frac{\partial f}{\partial\mu}\\
	 \frac{\partial f}{\partial\sigma}\\
	 \end{bmatrix} = 
	 \begin{bmatrix}
	 f\frac{1}{a\sigma}\\
	 f\frac{z}{\sigma}\\
	 f\frac{z^2-1}{\sigma}
	 \end{bmatrix}
	 @f]
      */
      static values
      df(const values& x, const values& p)
      {
	using i=std::slice;
	size_t n     = x.size();
	size_t q     = p.size();
	double a     = p[0];
	double mu    = p[1];
	double sigma = p[2];
	auto   z     = (x - mu) / sigma;
	auto   s     = std::exp(-std::pow(z,2)/2);

	values g(n*q);
	g[i(0,n,q)] = s; 
	g[i(1,n,q)] = s * a * z / sigma;
	g[i(2,n,q)] = s * a * std::pow(z,2) / sigma;

	return g;
      }

      /** 
	  Return the second partial derivatives of the function 

	  @f[ 
	  \nabla\nabla f = 
	  \begin{pmatrix} 
	  \frac{\partial^2f}{\partial a\partial a}  
	  & \frac{\partial^2f}{\partial a     \partial \mu}  
	  & \frac{\partial^2f}{\partial a     \partial \sigma} \\
	  & \frac{\partial^2f}{\partial \mu   \partial \mu}  
	  & \frac{\partial^2f}{\partial \mu   \partial \sigma} \\
	  &
	  & \frac{\partial^2f}{\partial \sigma\partial \sigma}  
	  \end{pmatrix} = 
	  \begin{pmatrix} 
	  0 
	  & f\frac{z}{a\sigma}
	  & f\frac{z^2}{a\sigma}\\
	  & f\frac{z^2-1}{\sigma^2}
	  & f\frac{z(z^2-2)}{\sigma^2}\\
	  &
	  & f\frac{z^2(z^2-3)}{\sigma^2}
	  \end{pmatrix}\quad.
	  @f]

	  Note matrix is symmetric, and thus we only show the upper triangle. 
      */
      static values
      d2f(const values& x, const values& p)
      {
	using  i      = std::slice;
	size_t n      = x.size();
	size_t q      = p.size();
	size_t q2     = q * q;
	double a      = p[0];
	double mu     = p[1];
	double sigma  = p[2];
	double sigma2 = sigma * sigma;
	auto   z      = (x - mu) / sigma;
	auto   z2     = std::pow(z, 2);
	auto   s      = std::exp(-std::pow(z,2)/2);

	values g(n * q2); // Tensor!
	g[i(0,n,q2)] = 0;                              // d2f/da2
	g[i(1,n,q2)] =     s / sigma * z;              // d2f/(da dmu)
	g[i(2,n,q2)] =     s / sigma * z2;             // d2f/(da dsigma)
	g[i(3,n,q2)] = g[i(1,n,q2)];                   // d2f/(dmu da)
	g[i(4,n,q2)] = a * s / sigma2 * (z2 - 1);      // d2f/dmu2
	g[i(5,n,q2)] = a * s / sigma2 * (z2 - 2) * z;  // d2f/(dmu dsigma)
	g[i(6,n,q2)] = g[i(2,n,q2)];                   // d2f/(dsigma da)
	g[i(7,n,q2)] = g[i(5,n,q2)];                   // d2f/(dsigma dmu)
	g[i(8,n,q2)] = a * s / sigma2 * (z2 - 3) * z2; // d2f/dsigma2

	return g;
      }
    };

    /**
       Data structure

       Thist contains 

       * @f$x@f$ - the independent variable 
       * @f$y@f$ - the dependent variable 
       * @f$e@f$ - the uncertainty on @f$y@f$ 

       @ingroup gslfit_example_unormal
    */
    struct data
    {
      /** 
	  Constructor.   Sets 

	  * @f$x\in[\mu-3\sigma,\mu+3\sigma]@f$ equidistantly spaced 
	  * @f$y = f(x;a,\mu,\sigma) @f$ 
	  * @f$e = \frac{y/10}@f$ 
	  * Adds noise @f$n\sim\mathcal{N}\left[0,\frac{y}{10}\right]@f$ 
	  to @f$y@f$ distribution 
	
	  @param n     Number of points 
	  @param seed  Random number seed
	  @param a     @f$a@f$ - scale  
	  @param mu    @f$\mu@f$ - mean of normal distribution 
	  @param sigma @f$\sigma@f$ - Standard deviation 
      */
      data(size_t   n     = 300,
	   unsigned seed  = 123456,
	   double   a     = 5,
	   double   mu    = 0.4,
	   double   sigma = 0.15)
	: x(n), y(), e()
      {
	std::random_device         dev;
	std::default_random_engine eng(seed == 0 ? dev() : seed);
	std::normal_distribution<> nrm(0,1);

	values i(n);
	std::iota(std::begin(i),std::end(i), 0);

	values d(n);
	std::generate(std::begin(d),std::end(d),
		      [&eng,&nrm](){ return nrm(eng); });
	
	x =  i / (n-1);
	y =  fit::f(x,{a,mu,sigma});
	e =  y / 10;
	y += e * d;
      }
      size_t size() const { return x.size(); }
      /** Independent variable */
      values x;
      /** Dependent varialbe */
      values y;
      /** Uncertainty on @f$y@f$ */
      values e;
    };
  }
}
/** 
    Program  

    @ingroup gslfit_example_unormal
*/
int main(int argc, char** argv)
{

  example::unormal::values p0{1,0,1};
  example::unormal::data   data(300);

  auto exa = example::make_example("unormal", p0, data,
				   example::unormal::fit::f,
				   example::unormal::fit::df,
				   example::unormal::fit::d2f);

  if (!exa.parse_args(argc, argv)) return 1;
  
  exa.run("p[0]*exp(-((x-p[1])/p[2])**2/2)");

  return 0;
}

// Local Variables:
//   compile-command: "g++ -O2 -I. `gsl-config --cflags` unormal.cc `gsl-config --libs`  -o unormal"
// End:

    
    
  
    
    
    
