/**
   @file      expo.cc
   @author    Christian Holm Christensen <cholmcc@gmail.com>
   @date      18 Aug 2021
   @copyright (c) 2021 Christian Holm Christensen. 
   @license   LGPL-3
   @brief     First fit example from GSL documentation
*/
#include <random>
#include <valarray>
#include <gslfit.hh>
#include <utils.hh>
#include <example.hh>

namespace example
{
  /** 
      @defgroup gslfit_example_expo Exponential fit
      @ingroup gslfit_examples

      @note This correponds to the [first
      example](https://www.gnu.org/software/gsl/doc/html/nls.html#exponential-fitting-example)
      in the GSL documentation.

      We fit the function 

      @f[f(x;a,\lambda,b) = a^{-\lambda x}+b\quad,@f] 
      
      to data.  The data has @f$ N=100@f$ points

      @f{align*}{
      x &= \left\{x_i=i\frac{x_{\mathrm{max}}}{N}
      \middle|i=0,\ldots,N-1\right\}\\
      \delta &= 
      \left\{\delta_i 
      \sim\mathcal{N}\left[0,\frac1{10}f(x_i;a_0,\mu_0,\sigma_0)\right]
      \middle|i=1,\ldots,N-1\right\}\\
      y &= \left\{y_i=f(x_i;a_0,\mu_0,\sigma_0)+\delta_i\middle|
                  i=1,\ldots,N-1\right\}\\
      e &= \left\{e_i=\delta_i\middle|i=1,\ldots,N-1\right\}\quad,
      @f}

      The initial parameters are 
      
      @f{align*}{
        a_0              &= 5\\
	\lambda_0        &= 1.5\\
	b_0              &= 1\\
	x_{\mathrm{max}} &= 3\quad.
      @f} 

      @image html expo.png 

      The class example::expo::fit encodes the fitting function @f$
      f@f$, and the gradient @f$J=\nabla f@f$.

      The class example::expo::data contains our data.  
      
      @b Timings

      The test was performed on a 

          Intel(R) Core(TM) i7-4550U CPU @ 1.50GHz

      with 1,000 runs of each fit kind

      | Jacobian | Hessian  | Average time (ms) |
      |----------|----------|-------------------|
      | numeric  | numeric  | 0.0904 +/- 0.0007 |
      | analytic | numeric  | 0.0884 +/- 0.0006 |
  */
  /** 
      Namespace for this example 
    
      @ingroup gslfit_example_expo
  */
  namespace expo
  {
    /** 
	Array of values.  We use std::valarray as this containter (as
	opposed to f.ex. std::vector) allows for optimised computations
	in parallel.

	@ingroup gslfit_example_expo
    */
    using values = std::valarray<double>;

    /** 
	The function to fit to data 

	@ingroup gslfit_example_expo
    */
    struct fit
    {
      static values f(const values& x, const values& p) {
	return p[0] * std::exp(-p[1] * x) + p[2];
      }
      static values df(const values& x, const values& p) {
	auto   explx     = std::exp(-p[1]*x);
	  
	values ret(x.size()*p.size());
	ret[std::slice(0,x.size(),p.size())] = explx; 
	ret[std::slice(1,x.size(),p.size())] = -p[0] * x * explx;
	ret[std::slice(2,x.size(),p.size())] = 1;

	return ret;
      }
    };
      
    /** 
	Our data structure 

	@ingroup gslfit_example_expo
    */
    struct data
    {
      /**
	 Create exponential data at random.
       
	 We create @f$ n@f$ data points between 0 and 3
	 where
       
	 @f[ y_i = 1+5e^{-3x_i/2}\quad, @f]
       
	 and uncertainty
       
	 @f[ \delta_i = y_i / 10\quad. @f]
       
	 We add normal noise to @f$ y_i@f$ with a standard deviation of
	 @f$\delta_i@f$.
       
      */
      data(size_t   n      = 100,
	   unsigned seed   = 123456,
	   double   a      = 5,
	   double   lambda = 1.5,
	   double   b      = 1,
	   double   xmax   = 3)
	: x(n),
	  y(n),
	  e(n)
      {
	std::random_device         rnd;
	std::default_random_engine gen(seed > 0 ? seed : rnd());
	std::normal_distribution<> norm(0,1);

	values i(n);
	std::iota(std::begin(i),std::end(i),0);

	values d(n);
	std::generate(std::begin(d),std::end(d),
		      [&gen,&norm](){ return norm(gen);});

	x =  i * xmax / (n-1);
	y =  fit::f(x,{a,lambda,b});
	e =  y / 10;
	y += e * d;
      }
      size_t size() const { return x.size(); }
      values x;
      values y;
      values e;
    };
  }
}



// -------------------------------------------------------------------
/** 
    Program  

    @ingroup gslfit_example_expo
*/
int main(int argc, char** argv)
{
  example::expo::values p0{1,0,0};
  example::expo::data   data(100);

  auto exa = example::make_example("expo", p0, data,
				   example::expo::fit::f,
				   example::expo::fit::df);
				   
  if (!exa.parse_args(argc, argv)) return 1;
  
  exa.run("p[0]*exp(-p[1]*x)+p[2]");

  return 0;
}

// Local Variables:
//   compile-command: "g++ -O2 -I. `gsl-config --cflags` expo.cc `gsl-config --libs`  -o expo"
// End:
