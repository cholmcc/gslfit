/**
   @file      yodatest.cc
   @author    Christian Holm Christensen <cholmcc@gmail.com>
   @date      18 Aug 2021
   @copyright (c) 2021 Christian Holm Christensen. 
   @license   LGPL-3
   @brief     Fit function to YODA objects
*/
#include <valarray>
#include <YODA/IO.h>
#include <YODA/Scatter2D.h>
#include <YodaGslFit.hh>
#include <iostream>
#include <iomanip>
#include <random>
#include <chrono>

/** 
    @defgroup gslfit_example_yoda Fitting YODA objects using GSL 

    @ingroup gslfit_examples 

    The code in this example illustrates how to fit functions to YODA
    objects using the GNU Scientific Library routines.  This uses the
    wrapper function YODA::GSL::fit defined in the header
    YodaGslFit.hh.

    In this example, we read in a `YODA::AnalysisObject` from a file
    and fits the function

    @f[ f(x;a,\mu,\sigma) = a\frac{1}{\sqrt{2\pi}\sigma}
    e^{-\frac12\left(\frac{x-\mu}{\sigma}\right)^2}
    \quad,
    @f] 

    to the data in that object. 

    The class example::YODA::Fit encodes the function @f$f@f$, the
    gradient of that @f$J=\nabla f@f$, and the Hessian
    @f$H=\nabla\nabla f@f$ of that too.

    The code in YODA::GSL::Wrappers takes care to calculate the
    residuals, the Jacobian and Hessian of the residuals using these
    functions, so we need not implement that here.

    The Python script `misc/mkyoda.py` generates some test data. 

    @image html fits.png

    @b Timings

    The test was performed on a 

          Intel(R) Core(TM) i7-4550U CPU @ 1.50GHz

    with 1,000 runs of each fit kind on `histo1D` (data size @f$30@f$)

    | Jacobian | Hessian  | Average time (ms) |
    |----------|----------|-------------------|
    | numeric  | numeric  | 0.2063 +/- 0.0009 |
    | analytic | numeric  | 0.1590 +/- 0.0009 |
    | analytic | analytic | 0.2044 +/- 0.001  |

    Similar for `histo2D` (data size is @f$30^2=900@f$)

    | Jacobian | Hessian  | Average time (ms) |
    |----------|----------|-------------------|
    | numeric  | numeric  |  40.36 +/- 0.04   |
    | analytic | numeric  |  23.99 +/- 0.04   |

*/

namespace example {
  namespace YODA {
    /** 
	Timer guard 
       
       @ingroup gslfit_example_yoda
    */
    struct Timer
    {
      /** clock type */
      using ClockType=std::chrono::high_resolution_clock;
      /** time type */
      using TimePoint=typename ClockType::time_point;
      /** Our measuring unit */
      using Duration=std::chrono::duration<double,std::milli>;
      /** Start of timer */
      TimePoint _start;
      /** Last lap */
      TimePoint _prev;
      /** Sum of laps */
      double _sum;
      /** Sum of square laps */
      double _sum2;
      /** Number of laps */
      size_t _cnt;
      
      Timer()
	: _start(ClockType::now()),
	  _prev(_start),
	  _sum(0),
	  _sum2(0),
	  _cnt(0)
      {}

      void lap() {
	auto now  =  ClockType::now();
	auto elap =  Duration(now - _prev).count();
	_prev     =  now;
	_sum      += elap;
	_sum2     += elap * elap;
	_cnt++;
      }
    
      ~Timer() {
	double mean  = _sum / _cnt;
	double mean2 = _sum2 / _cnt;
	double var   = mean2 - mean * mean;
	double sem   = std::sqrt(var / _cnt);

	std::cout << "Elapsed: " << _sum << " ms for " << _cnt << " laps"
		  << " average lap: (" << mean
		  << " +/- " << sem << ") ms" << std::endl;
      }
    };

    /**
       Type of value vector 
       
       @ingroup gslfit_example_yoda
    */
    using Values=std::valarray<double>;

    /** 
	Normal distribution functions 
       
       @ingroup gslfit_example_yoda
    */
    template <typename Type>
    struct Fit {
      using Value=Type;
      
      /** 
	  Calculates 

	  @f[f(z^2;a) = a e^{-z^2/2}\quad.@f]

	  @param a  @f$ a@f$ 
	  @param z2 @f$ z^2@f$ 

	  @returns @f$ f(z^2;a)@f$
      */
      static double f(const double& a, const double& z2) {
	return a * std::exp(-z2/2);
      }
      /** 
	  Calculates the location and scale indepedent variable 

	  @f[z = \frac{x - \mu}{\sigma}\quad. @f]
	  
	  @param x      @f$ x@f$ 
	  @param mu     @f$ \mu@f$
	  @param sigma  @f$ \sigma@f$

	  @returns @f$ z@f$ 
      */
      static double z(const double& x, const double& mu, const double& sigma) {
	return (x - mu) / sigma;
      }
    };

    /** 
	1D Normal distribution functions 
       
       @ingroup gslfit_example_yoda
    */
    struct Fit1D : public Fit<double>
    {
      using Base=Fit<double>;
      using Value=typename Base::Value;

      /** The constant @f$\sqrt{2\pi}@f$ */
      constexpr static double sqrt_2pi = std::sqrt(2*M_PI);

      /** 
	  Calculate 
	  
	  @f[f(x;a,\mu,\sigma) = a\frac{1}{\sqrt{2\pi}\sigma}
	     e^{-\frac12z^2}\quad,@f]

	  where 

	  @f[z =\frac{x-\mu}{\sigma}\quad.@f]

	  @param x   Independent variable 
	  @param p   Parameters @f$\{a,\mu,\sigma\}@f$ 
	  
	  @returns @f$ f@f$
      */
      static double f(const Value& x, const Values& p) {
	double a     = p[0];
	double mu    = p[1];
	double sigma = p[2];
	double norm  = a / (sqrt_2pi * sigma);
	double z2    = std::pow(Base::z(x,mu,sigma),2);
	return Base::f(norm, z2);
      }

      /**
	 Calculates
       
	 @f[
         \nabla f=\begin{bmatrix}
	 \frac{\partial f}{\partial a}\\
	 \frac{\partial f}{\partial\mu}\\
	 \frac{\partial f}{\partial\sigma}\\
	 \end{bmatrix} = 
	 \begin{bmatrix}
	 f\frac1{a}\\
	 f\frac{z}{\sigma}\\
	 f\frac{z^2-1}{\sigma}
	 \end{bmatrix}\quad,
	 @f]

	 where 

	 @f[z =\frac{x-\mu}{\sigma}\quad.@f]

	  @param x   Independent variable 
	  @param p   Parameters @f$\{a,\mu,\sigma\}@f$ 
	  
	  @returns @f$ \nabla f@f$
      */
      static Values df(const Value& x, const Values& p) {
	double a     = p[0];
	double mu    = p[1];
	double sigma = p[2];
	double norm  = 1 / (sqrt_2pi * sigma);
	double z     = Base::z(x,mu,sigma);
	double z2    = z * z;
	double s     = Base::f(norm, z2);
	double f     = a * s;
	Values g(p.size());

	g[0] = s;
	g[1] = f / sigma * z;
	g[2] = f / sigma * (z2 - 1);

	return g;
      }

      /** 
	  Calculates the second partial derivatives of the function 

	  @f[ 
	  \nabla\nabla f = 
	  \begin{pmatrix} 
	  \frac{\partial^2f}{\partial a\partial a}  
	  & \frac{\partial^2f}{\partial a     \partial \mu}  
	  & \frac{\partial^2f}{\partial a     \partial \sigma} \\
	  & \frac{\partial^2f}{\partial \mu   \partial \mu}  
	  & \frac{\partial^2f}{\partial \mu   \partial \sigma} \\
	  &
	  & \frac{\partial^2f}{\partial \sigma\partial \sigma}  
	  \end{pmatrix} = 
	  \begin{pmatrix} 
	  0 
	  & f\frac{z}{a\sigma}
	  & f\frac{z^2-1}{a\sigma}\\
	  & f\frac{z^2-1}{\sigma^2}
	  & f\frac{z(z^2-3)}{\sigma^2}\\
	  &
	  & f\frac{2+z^2(z^2-5)}{\sigma^2}
	  \end{pmatrix}\quad,
	  @f]

	 where 

	 @f[z =\frac{x-\mu}{\sigma}\quad.@f]

	 Note matrix is symmetric, and thus we only show the upper triangle. 

	 @param x   Independent variable 
	 @param p   Parameters @f$\{a,\mu,\sigma\}@f$ 
	  
	 @returns @f$ \nabla\nabla f@f$
      */
      static Values  d2f(const Value& x, const Values& p)
      {
	double a      = p[0];
	double mu     = p[1];
	double sigma  = p[2];
	double sigma2 = sigma * sigma;
	double norm   = 1 / (sqrt_2pi * sigma) / sigma;
	double z      = Base::z(x,mu,sigma);
	double z2     = z * z;
	double s      = Base::f(norm, z2);
	double f      = a * s;
	Values g(p.size()*p.size());

	g[0] = 0;                               // d2f/da2
	g[1] = s * z;                           // d2f/(da dmu) 
	g[2] = s * (z2 - 1);                    // d2f/(da dsigma)
	g[3] = g[1];                            // d2f/(dmu da)
	g[4] = f / sigma * (z2 - 1);            // d2f/dmu2
	g[5] = f / sigma * z * (z2 - 3);        // d2f/(dmu dsigma)
	g[6] = g[2];                            // d2f/(dsigma da)
	g[7] = g[5];                            // d2f/(dsigma dmu)
	g[8] = f / sigma * (2 + z2 * (z2 - 5)); // d2f/dsigma2

	return g;
      }
    };
    
    /** 
	2D Normal distribution functions 
       
       @ingroup gslfit_example_yoda
    */
    struct Fit2D : public Fit<std::pair<double,double>>
    {
      using Base=Fit<std::pair<double,double>>;
      using XY=std::pair<double,double>;
      
      /** 
	  Calculate 
	  
	  @f[f(x,y;a,\mu_x,\mu_y,\sigma_x,\sigma_y) 
	  = a\frac{1}{2\pi\sigma_x\sigma_y}
	     e^{-\frac12\left(z_x^2+z_y^2\right)}\quad,@f]

	  where 

	  @f{align*}{ 
	  z_x &= \frac{x-\mu_x}{\sigma_x}\\
	  z_y &= \frac{y-\mu_y}{\sigma_y}\\
	  @f}

	  @param xy  Independent variable @f$(x,y)@f$
	  @param p   Parameters @f$\{a,\mu,\sigma\}@f$ 
	  
	  @returns @f$ f@f$
      */
      static double f(const std::pair<double,double>& xy, const Values& p) {
	const double& x      = xy.first;
	const double& y      = xy.second;
	double        a      = p[0];
	double        mux    = p[1];
	double        muy    = p[2];
	double        sigmax = p[3];
	double        sigmay = p[4];
	double        norm   = a / (2 * M_PI * sigmax * sigmay);
	double        zx     = Base::z(x,mux,sigmax);
	double        zy     = Base::z(y,muy,sigmay);
	double        z2     = zx * zx + zy * zy;
	  
	return Base::f(norm,z2);
      }

      /**
	 Calculates
       
	 @f[
         \nabla f=\begin{bmatrix}
	 \frac{\partial f}{\partial a}\\
	 \frac{\partial f}{\partial\mu_x}\\
	 \frac{\partial f}{\partial\mu_y}\\
	 \frac{\partial f}{\partial\sigma_x}\\
	 \frac{\partial f}{\partial\sigma_y}\\
	 \end{bmatrix} = 
	 \begin{bmatrix}
	 f\frac1{a}\\
	 f\frac{z_x}{\sigma_x}\\
	 f\frac{z_y}{\sigma_y}\\
	 f\frac{z_x^2-1}{\sigma_x}\\
	 f\frac{z_y^2-1}{\sigma_y}
	 \end{bmatrix}
	 @f]

	 where 
	 
	 @f{align*}{ 
	 z_x &= \frac{x-\mu_x}{\sigma_x}\\
	 z_y &= \frac{y-\mu_y}{\sigma_y}\\
	 @f}
	 
	 @param xy  Independent variable @f$(x,y)@f$
	 @param p   Parameters @f$\{a,\mu,\sigma\}@f$ 
	 
	 @returns @f$ \nabla f@f$
      */
      static Values df(const Value& xy, const Values& p) {
	const double& x      = xy.first;
	const double& y      = xy.second;
	double        a      = p[0];
	double        mux    = p[1];
	double        muy    = p[2];
	double        sigmax = p[3];
	double        sigmay = p[4];
	double        norm   = 1 / (2 * M_PI * sigmax * sigmay);
	double        zx     = Base::z(x,mux,sigmax);
	double        zy     = Base::z(y,muy,sigmay);
	double        zx2    = zx * zx;
	double        zy2    = zy * zy;
	double        z2     = zx2 + zy2;
	double        s      = Base::f(norm, z2);
	double        f      = a * s;
	Values        g(p.size());

	g[0] = s;
	g[1] = f / sigmax * zx;
	g[2] = f / sigmay * zy;
	g[3] = f / sigmax * (zx2 - 1);
	g[4] = f / sigmay * (zy2 - 1);

	return g;
      }
    };
    //================================================================
    /** 
	This is a trampoline to the appropriate fitting function
	depending on what is selected in @a ifit:

	- `ifit=0` Use numerically calculated Jacobian or Hessian 
	- `ifit=1` Use analytic Jacobian, numerical Hessian 
	- `ifit=2` Use analytic Jacobian and Hessian 

	@param config Fit configuration 
	@param data   The data to fit to 
	@param ifit   Choice of functions (see above) 
	@param iverb  Verbosity 

	@return Fit result 
       
	@ingroup gslfit_example_yoda
    */
    template <typename Data>
    ::YODA::GSL::FitResult<Values>
    doFit1D(const ::YODA::GSL::FitConfig& config,
	    const Data&                   data,
	    int                           ifit,
	    int                           iverb)
    {
      Values p0{1.,2.,3.};
      switch (ifit) {
      case 0:
	return ::YODA::GSL::fit(config, p0, data,
				Fit1D::f, nullptr, nullptr, iverb);
      case 1:
	return ::YODA::GSL::fit(config, p0, data,
				Fit1D::f, Fit1D::df, nullptr, iverb);
      case 2:
	return ::YODA::GSL::fit(config, p0, data,
				Fit1D::f, Fit1D::df, Fit1D::d2f, iverb);
      }
      return ::YODA::GSL::FitResult<Values>(0,0);
    }
    //----------------------------------------------------------------
    /** 
	This is a trampoline to the appropriate fitting function
	depending on what is selected in @a ifit:

	- `ifit=0` Use numerically calculated Jacobian or Hessian 
	- `ifit=1` Use analytic Jacobian, numerical Hessian 
	- `ifit=2` Use analytic Jacobian and Hessian 

	@param config Fit configuration 
	@param data   The data to fit to 
	@param ifit   Choice of functions (see above) 
	@param iverb  Verbosity 

	@return Fit result 
       
	@ingroup gslfit_example_yoda
    */
    template <typename Data>
    ::YODA::GSL::FitResult<Values>
    doFit2D(const ::YODA::GSL::FitConfig& config,
	    const Data&                   data,
	    int                           ifit,
	    int                           iverb)
    {
      Values p0{1.,2.,2.,3.,3.};
      switch (ifit) {
      case 0:
	return ::YODA::GSL::fit(config, p0, data,
				Fit2D::f, nullptr, nullptr, iverb);
      case 1:
	return ::YODA::GSL::fit(config, p0, data,
				Fit2D::f, Fit2D::df, nullptr, iverb);
      // case 2:
      // 	return ::YODA::GSL::fit(config, p0, data,
      //                                Fit2D::f, Fit2D::df, Fit2D::d2f, iverb);
      }
      return ::YODA::GSL::FitResult<Values>(0,0);
    }
    //----------------------------------------------------------------
    /** 
	Trampoline that decodes the YODA::AnalysisObject real type and
	delegates to appropriate template instance of doFit.  This
	also sets the fitting method used base on `imeth`.

	@param ao     Object to fit to 
	@param imeth  Trust region method selector 
	@param ifit   Choice of functions (see above) 
	@param iverb  Verbosity 
	@param repeat Repeatition of test
       
	@ingroup gslfit_example_yoda
    */
    void performFit(::YODA::AnalysisObject* ao, int imeth, int ifit, int iverb,
		    int repeat=1) {
      using namespace ::YODA::GSL;
      
      FitConfig  cfg;
      switch (imeth) {
      case 0: cfg.method = FitConfig::method_type::levenberg_marquardt;	break;
      case 1: cfg.method =
	  FitConfig::method_type::accelerated_levenberg_marquardt;	break;
      case 2: cfg.method = FitConfig::method_type::dogleg;  	        break;
      case 3: cfg.method = FitConfig::method_type::double_dogleg;       break;
      case 4: cfg.method = FitConfig::method_type::subspace;            break;
      case 5: cfg.method = FitConfig::method_type::steihaug_toint;      break;
      }

      FitResult<example::YODA::Values> res(0,0);

      ::YODA::Histo1D*   histo1D   = dynamic_cast<::YODA::Histo1D*>  (ao);
      ::YODA::Profile1D* profile1D = dynamic_cast<::YODA::Profile1D*>(ao);
      ::YODA::Scatter2D* scatter2D = dynamic_cast<::YODA::Scatter2D*>(ao);
      ::YODA::Histo2D*   histo2D   = dynamic_cast<::YODA::Histo2D*>  (ao);
      ::YODA::Profile2D* profile2D = dynamic_cast<::YODA::Profile2D*>(ao);
      ::YODA::Scatter3D* scatter3D = dynamic_cast<::YODA::Scatter3D*>(ao);

      int cnt = repeat-1;
      Timer t;
      do {
	if      (scatter2D) res = doFit1D(cfg, *scatter2D, ifit, iverb);
	else if (histo1D)   res = doFit1D(cfg, *histo1D,   ifit, iverb);
	else if (profile1D) res = doFit1D(cfg, *profile1D, ifit, iverb);
	else if (scatter3D) res = doFit2D(cfg, *scatter3D, ifit, iverb);
	else if (histo2D)   res = doFit2D(cfg, *histo2D,   ifit, iverb);
	else if (profile2D) res = doFit2D(cfg, *profile2D, ifit, iverb);
	else 
	  std::cerr << "Don't know how to fit to a " << ao->type() << " object"
		    << std::endl;

	t.lap();

	if (cnt == 0) {
	  std::cout << res << std::endl;

	  size_t p   = res.parameters.size();
	  Values var = Values(res.covariance[std::slice(0,p,p+1)]);
	  Values sd  = std::sqrt(var);
	  Values sd2(sd.size()*sd.size());
	  auto   v2 = std::begin(sd2);
	  for (auto vi = std::begin(sd); vi != std::end(sd); ++vi) 
	    for (auto vj = std::begin(sd); vj != std::end(sd); ++vj, ++v2) 
	      *v2 = *vi * *vj;
	  
	  auto corr = res.covariance / sd2;

	  std::cout << "Correlation matrix:\n"
		    << std::setw(3) << "#" << "  " << std::flush;
	  for (size_t i = 0; i < p; i++)
	    std::cout << std::setw(9) << i << ": ";
	  std::cout << std::endl;
	  
	  for (size_t i = 0; i < p; i++) {
	    std::cout << std::setw(3) << i << ": ";
	    for (size_t j = 0; j <=  i; j++)
	      std::cout << gsl::wdouble<10,6>(corr[i * p + j]) << " ";
	    std::cout << std::endl;
	  }
	}
	else if (cnt % (repeat / 10) == 0)
	  std::cout << "Repetition " << cnt << " " << std::endl;
      } while (--cnt >= 0);
    }
  }
}

/** 
    Show help 
       
    @ingroup gslfit_example_yoda
*/
void usage(std::ostream&      o,
	   const std::string& progname,
	   const std::string& fn,
	   size_t             itab,
	   size_t             ix,
	   size_t             iy,
	   const std::string& name,
	   size_t             imeth,
	   size_t             ifit,
	   size_t             iverb,
	   bool               list)
{
  o << "Usage: " << progname << " [OPTIONS] FILE\n\n"
    << "Options:\n"
    << " -t TABLE  Choice of table               (" << itab << ")\n"
    << " -x X      Choice of X in table          (" << ix   << ")\n"
    << " -y Y      Choice of Y in table          (" << iy   << ")\n"
    << " -n NAME   Choice of object              (" << name << ")\n"
    << " -m METHOD Choice of trust method        (" << imeth<< ")\n"
    << " -f FIT    Choice of fitter functions    (" << ifit << ")\n"
    << " -v LEVEL  Verbosity level               (" << iverb<< ")\n"
    << " -l        List content of file          (" << list << ")\n"
    << " -h        This help\n\n"
    << "FILE is the input data file (" << fn << ")\n\n"
    << "TABLE,X,Y identifes d<TABLE>-x<X>-y<Y> the object to read.\n"
    << "Alternatively, the NAME can be given\n\n"
    << "METHOD is one of\n\n"
    << "- 0 Levenberg-Marquardt\n"
    << "- 1 Levenberg-Marquardt w/acceleration\n"
    << "- 2 Dogleg\n"
    << "- 3 Double dogleg\n"
    << "- 4 2D subspace\n"
    << "- 5 Steihaug-Toint\n"
    << std::endl;
}

/**
   The program
    
   @ingroup gslfit_example_yoda
*/
int main(int argc, char** argv)
{
  int         itab   = 1;
  int         ix     = 1;
  int         iy     = 1;
  int         iverb  = 0;
  int         imeth  = 1;
  int         ifit   = 0;
  int         repeat = 1;
  bool        list   = false;
  std::string fn     = "data.yoda";
  std::string name   = "histo1D";

  for (int i = 1; i < argc; i++) {
    if (argv[i][0] != '-') {
      fn = argv[i];
      continue;
    }

    switch (argv[i][1]) {
    case 'h': usage(std::cout, argv[0], fn, 
		    itab, ix, iy, name,
		    imeth, ifit,
		    iverb, list);
      return 0;
    case 't': itab   = std::stoi(argv[++i]); break;
    case 'x': ix     = std::stoi(argv[++i]); break;
    case 'y': iy     = std::stoi(argv[++i]); break;
    case 'f': ifit   = std::stoi(argv[++i]); break;
    case 'n': name   = argv[++i];            break;
    case 'm': imeth  = std::stoi(argv[++i]); break;
    case 'v': iverb  = std::stoi(argv[++i]); break;
    case 'r': repeat = std::stoi(argv[++i]); break;
    case 'l': list   = !list;                break;
    default: 
      std::cerr << argv[0] << ": Unknown option " << argv[i] << std::endl;
      return 1;
    }
  }

  if (name.empty()) {
    std::stringstream s;
    s << std::setfill('0')
      << "d" << std::setw(2) << itab << "-"
      << "x" << std::setw(2) << ix   << "-"
      << "y" << std::setw(2) << iy;
    name = s.str();
  }
  
  auto                  aos = YODA::read(fn);
  YODA::AnalysisObject* ao  = 0;

  if (list) std::cout << std::left;
  
  for (auto& o : aos) {
    std::string star(" ");
    if (name == o->name()) {
      ao = o;
      star = "*";
    }
    
    if (list) 
      std::cout << std::setw(20) << (o->name()+star) << " "
		<< std::setw(20) << ("<"+o->type()+">") << " "
		<< o->path() << std::endl;
  }
  if (!ao) {
    std::cerr << "Object " << name << " not found" << std::endl;
    return 1;
  }

  example::YODA::performFit(ao, imeth, ifit, iverb, repeat);
  
  return 0;
}
// Local Variables:
//  compile-command: "g++ -O2 yodatest.cc `yoda-config --cflags` `gsl-config --cflags` -I. `yoda-config --libs` `gsl-config --libs` -o yodatest"
// End:
